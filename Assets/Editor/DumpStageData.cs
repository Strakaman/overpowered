﻿using UnityEngine;
using System.Collections;
using UnityEditor.Callbacks;
using UnityEditor;
using System;
using System.IO;

public class DumpStageData : OmonoPehaviour
{
	/*
 * This below method will automatically run whenever the Unity project is built.
 * One because the script is in the editor folder
 * Two because it has the [PostProcessBuild]
 * The specific parameters passed into the method
 */
	[PostProcessBuild]
	public static void OnPostprocessBuild (BuildTarget target, string pathToBuildProject)
	{
		//Debug.Log (Application.persistentDataPath);
		try {
			//Utilize Config subfolder to control all configuration based items.
			if (!Directory.Exists(SessionData.stageConfigFolderPath)) { //check to see if there is a Config folder
				//Debug.Log("Path DNE");
				Directory.CreateDirectory(SessionData.stageConfigFolderPath); //create folder if it doesn't exist
			}	
			//string stageConfigPath = Application.persistentDataPath + "/Config/StageDataConfig.OP";
			if (File.Exists(SessionData.stageConfigFilePath))
			{
				//work around because i don't know how to update an already existing file
				File.Delete(SessionData.stageConfigFilePath);
			}
			//cope file containing actual configuration data to output folder.
			FileUtil.CopyFileOrDirectory ("Assets/Editor/StageDataConfig.OP", SessionData.stageConfigFilePath);
		}
		catch (Exception e) {
			Debug.Log ("Omari: "+e.Message);
		}
	}
}