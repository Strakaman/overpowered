﻿using UnityEngine;
using System.Collections;
using System;

public enum SequenceAction { DoNothing, MoveLeft, MoveRight, Jump, DropThrough, Dash, Shoot, Melee, GoOverPower, Reset };

[Serializable]
public class ActionItem
{
	public SequenceAction Action;
	public float Duration;
}

public class ActionSequence : OmonoPehaviour
{
	public PlayerAction playerAction;
	public ActionItem[] PlayerSequences;
	
	private int actionItemIndex;
	private float moveDurationElapsed;
	private float moveYieldTime = .015f;
	private bool alreadyExecutingAction;
	private bool doneExecutingActions;
	private ActionItem CA; //Current Action Item local variable makes it easier to read since it's referenced often
	
	
	protected override void OnStart () {
			actionItemIndex = 0;
		alreadyExecutingAction = false;
		doneExecutingActions = false;
	}
	
	void Update()
	{
		if (doneExecutingActions) { return; }
		if (actionItemIndex >= PlayerSequences.Length)
		{
			doneExecutingActions = true;
		}
		
		if (!doneExecutingActions)
		{
			if (!alreadyExecutingAction)
			{
				InterpretCurrentAction();
			}
		}
	}
	
	void InterpretCurrentAction()
	{
		alreadyExecutingAction = true;
		CA = PlayerSequences[actionItemIndex];
		if (CA.Action.Equals(SequenceAction.DoNothing))
		{
			Invoke("IncreaseIndex", CA.Duration);
		}
		else if (CA.Action.Equals(SequenceAction.MoveLeft))
		{
			StartCoroutine(MoveLeft());
		}
		else if (CA.Action.Equals(SequenceAction.MoveRight))
		{
			StartCoroutine(MoveRight());
		}
		else if (CA.Action.Equals(SequenceAction.Jump))
		{
			StartCoroutine(Jump());
		}
		else if (CA.Action.Equals(SequenceAction.DropThrough))
		{
			StartCoroutine(DropThrough());
		}
		else if (CA.Action.Equals(SequenceAction.Dash))
		{
			StartCoroutine(Dash());
		}
		else if (CA.Action.Equals(SequenceAction.Shoot))
		{
			StartCoroutine(Shoot());
		}
		else if (CA.Action.Equals(SequenceAction.Melee))
		{
			StartCoroutine(Melee());
		}
		else if (CA.Action.Equals(SequenceAction.GoOverPower))
		{
			StartCoroutine(GoOverPower());
		}
		else if (CA.Action.Equals(SequenceAction.Reset))
		{
			StartCoroutine(Reset());
		}
	}
	
	void IncreaseIndex()
	{
		actionItemIndex++;
		alreadyExecutingAction = false;
	}
	
	IEnumerator MoveLeft()
	{
		moveDurationElapsed = 0;
		while (moveDurationElapsed < CA.Duration)
		{
			playerAction.HorizontalInput(-1f);
			yield return new WaitForSeconds (moveYieldTime);
			moveDurationElapsed += moveYieldTime;
		}
		IncreaseIndex();
	}
	
	IEnumerator MoveRight()
	{
		moveDurationElapsed = 0;
		while (moveDurationElapsed < CA.Duration)
		{
			playerAction.HorizontalInput(1f);
			yield return new WaitForSeconds (moveYieldTime);
			moveDurationElapsed += moveYieldTime;
		}
		IncreaseIndex();
	}
	
	IEnumerator Jump()
	{
		playerAction.JumpInput();
		yield return new WaitForSeconds (CA.Duration);
		IncreaseIndex();
	}
	
	IEnumerator DropThrough()
	{
		playerAction.DropThrough();
		yield return new WaitForSeconds (CA.Duration);
		IncreaseIndex();
	}
	
	IEnumerator Dash()
	{
		playerAction.DashInput();
		yield return new WaitForSeconds (CA.Duration);
		IncreaseIndex();
	}
	
	IEnumerator Shoot()
	{
		playerAction.RangeInput();
		yield return new WaitForSeconds (CA.Duration);
		IncreaseIndex();
	}
	
	IEnumerator Melee()
	{
		playerAction.MeleeInput();
		yield return new WaitForSeconds (CA.Duration);
		IncreaseIndex();
	}
	
	IEnumerator GoOverPower()
	{
		//pAction script doesn't have a command yet for going OverPower
		//may have to grab reference to OverPower script to skip actual check for OPP points
		yield return new WaitForSeconds (CA.Duration);
		IncreaseIndex();
	}
	
	IEnumerator Reset()
	{
		actionItemIndex = 0;
		yield return new WaitForSeconds (CA.Duration);
		IncreaseIndex();
	}
	
}
