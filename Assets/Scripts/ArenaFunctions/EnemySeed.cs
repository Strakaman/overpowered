using UnityEngine;
using System.Collections;
using System;

[System.Serializable]
public class EnemySeed:IComparable {

	public GameObject EnemyPrefab;
	public int SeedLevel;


	/**
	 * Implements IComaparable so needs a method that allows it to 
	 * compare Seeds and determine which seed is higher, uses seed level
	 * to determine this
	 */ 
	public int CompareTo(object o)
	{
		EnemySeed e = (EnemySeed) o;
		if (this.SeedLevel > e.SeedLevel)
		{
			return 1;
		}
		else if (this.SeedLevel < e.SeedLevel)
		{
			return -1;
		}
		else
		{
			return 0;
		}
	}
}
