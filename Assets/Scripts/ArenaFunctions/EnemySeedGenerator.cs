﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System;


public class EnemySeedGenerator : OmonoPehaviour {
	public EnemySeed[] SeedList;
	// Use this for initialization
	protected override void OnStart () {
		SortSeedArray(); //just in case it's not sorted by seed level, sort it
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public EnemySeed GetSeed(int index)
	{
		if ((index < 0) || (index >= SeedList.Length)) //out of bounds check
		{
			return null;
		}
		else
		{
			return SeedList[index];
		}
	}

	/**
	 * Use the level of the current room to 
	 * Get the max and min index used for random seed generation
	 * 
	 */ 
	public void getLevelBasedRange(short stageLevelMin, short stageLevelMax, out short indexMin, out short indexMax)
	{
		int i,j;
		indexMin = 0;
		indexMax = 0;
		for (i = 0; i < SeedList.Length; i++)
		{
			if (stageLevelMax >= SeedList[i].SeedLevel)
			{
				indexMax = (short)i;
			}
			else
			{
				break;
			}
		}
		for (j = SeedList.Length-1; j >= 0; j--)
		{
			if (stageLevelMin <= SeedList[j].SeedLevel)
			{
				indexMin = (short)j;
			}
			else
			{
				break;
			}
		}
	}

	/***
	 * Get the index location of the currently maximum level
	 * spawnable seed
	 */ 
	public void getRemainderBasedMax(short stageLevelMax, out short indexMax, int currentRemainder)
	{
		int i;
		indexMax = 0;
		for (i = 0; i < SeedList.Length; i++)
		{
			if (SeedList[i].SeedLevel > currentRemainder) 
			{
				//because that means we already found the highest seed index
				//that our room capacity remainder will allow us to spawn
				break;
			}
			if (stageLevelMax >= SeedList[i].SeedLevel)
			{
				indexMax = (short)i;
			}
			else
			{
				break;
			}
		}
	}
	
	void SortSeedArray()
	{
		Array.Sort(SeedList);
	}

	/***
	 * Called by the level/room, generates a random list of enemies
	 * based on the parameters of that room. Returns that list so that enemies
	 * can be spawned over time.
	 */ 
	public Queue<EnemySeed> generateEnemyList(int roomLevel, short stageLevelMin, short stageLevelMax)
	{
		short indexMin,indexMax;
		getLevelBasedRange(stageLevelMin, stageLevelMax, out indexMin, out indexMax);
		Queue<EnemySeed> roomEnemyList = new Queue<EnemySeed>();
		int remainder = roomLevel;
		int ranNum;
		/**
		 * While this room's weakest summonable enemy is within the remaining capacity of the room
		 * randomnly add an enemy within the range to the list
		 * subtract that enemy's level from the remaining room capacity
		 * get the new max index since it's not fair to summon a high level enemy when the room is already supposed to be near full capacity 
		 */ 
		while (SeedList[indexMin].SeedLevel <= remainder)  
		{
			ranNum = UnityEngine.Random.Range(indexMin,(indexMax+1));
			//Debug.Log("Index Min: " + indexMin + " Index Max: " + indexMax + " Ran: "+ranNum);
			roomEnemyList.Enqueue(SeedList[ranNum]);
			remainder -= SeedList[ranNum].SeedLevel;
			getRemainderBasedMax(stageLevelMax,out indexMax,remainder);
		}
		return roomEnemyList;
	}

	/**
	 * Method overload in case just the room is being passed in
	 */ 
	public Queue<EnemySeed> generateEnemyList(Room daRoom)
	{
		return this.generateEnemyList(daRoom.RoomLevel, (short)daRoom.minESeedLevel, (short)daRoom.maxESeedLevel);
	}
}
