﻿using UnityEngine;
using System.Collections;

public class JumpThruPlat : OmonoPehaviour {
	//bool fromBottom = false;
	public BoxCollider2D actualPlatform;
	// Use this for initialization
	
	protected override void OnStart () {
	
		}
	
	// Update is called once per frame
	void Update () {
		/*if (fromBottom) //if a player has collided from the bottom platform collider should be disabled so you can jump through it
		{
			actualPlatform.enabled = false;
		}
		if (!fromBottom)
		{
			actualPlatform.enabled = true;
		}*/
	}

	/*
	 * If trigger portion of the collider 
	 * 
	 */
	void OnTriggerStay2D(Collider2D coll) {
		//fromBottom = true;
		//Debug.Log("yup");
		if (Utilities.hasMatchingTag("JumpThrough",coll.gameObject))
		{
			Physics2D.IgnoreCollision(coll.GetComponent<Collider2D>(),actualPlatform,true);
		}
	}
	
	void OnTriggerExit2D(Collider2D coll) {
		//fromBottom = false;
		if (Utilities.hasMatchingTag("JumpThrough",coll.gameObject))
		{
			Physics2D.IgnoreCollision(coll.GetComponent<Collider2D>(),actualPlatform,false);
		}
}
}
