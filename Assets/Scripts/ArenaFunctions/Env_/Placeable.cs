﻿using UnityEngine;
using System.Collections;
using System;

public class Placeable {
	public GameObject objectToBePlaced;
	public int x;
	public int y;
	
	public Placeable(GameObject objectToBePlaced, int x, int y)
	{
		this.objectToBePlaced = objectToBePlaced;
		this.x = x;
		this.y = y;
	}
}