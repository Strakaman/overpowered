﻿using UnityEngine;
using System.Collections;

public class Platform : OmonoPehaviour {

	private Vector3 s; //box collider size to help with collision checks
	// Use this for initialization
	protected override void OnStart (){
		BoxCollider2D zollider = GetComponent<BoxCollider2D> ();
		s = zollider.size;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnCollisionEnter2D(Collision2D collInfo)
	{
		if (Utilities.hasMatchingTag("WalksOnGround",collInfo.gameObject))
		{
			if (collInfo.gameObject.transform.position.y > (transform.position.y + s.y/2 )) 
			{//player has to be higher than the top of the platform, which should 
				//equal the middle of the platform plus its top half in distance
				collInfo.gameObject.BroadcastMessage("GroundSelf");
			}
		}
	}
}
