﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections.ObjectModel;

public enum PlatformConfigurations {All, XCross, O, P, Plus, LowercaseN, LowercaseV, MidOnly, NoMid, None };

public class PlatformGenerator : OmonoPehaviour {
	public const int configOptionsLength = 10;

	int curXLoc;
	int curYLoc;
	int numOfPlatforms;
	int curPlaceableIndex;
	public GameObject[] PlaceableObjects; //set in inspector
	public PlatformConfigurations RoomPlatConfig;

	private bool[] All = 	new bool[]{ false,true,true,true,true,true,true,true,true,true};
	private bool[] XCross = 	new bool[]{ false,true,false,true,false,true,false,true,false,true};
	private bool[] O =		new bool[]{ false,false,true,false,true,false,true,false,true,false};
	private bool[] P =		new bool[]{ false,true,false,false,true,true,true,true,true,true};
	private bool[] Plus = 	new bool[]{ false,false,true,false,true,true,true,false,true,false};
	private bool[] LoweN =	new bool[]{ false,true,false,true,false,true,false,false,false,false};
	private bool[] LoweV = new bool[]{ false,false,true,false,true,false,true,false,false,false};
	private bool[] MidOnly =	new bool[]{ false,false,false,false,false,true,false,false,false,false};
	private bool[] NoMid =	new bool[]{ true,true,true,true,true,false,true,true,true,true};
	private bool[] None =	new bool[]{ false,false,false,false,false,false,false,false,false,false};

	[System.Serializable]
	public class jplats
	{
		public GameObject[] list = new GameObject[10];
	}
	public jplats JPlats;
	//public GameObject[] jPlats;

	public Placeable[] generatePlatformList()
	{
		curXLoc = 0; //initialize with some numbers guaranteed to not be in use
		curYLoc = 0;
		numOfPlatforms = Random.Range(2,5);
		Placeable[] roomPlatforms = new Placeable[numOfPlatforms];
		for (int i = 0; i < numOfPlatforms; i++)
		{
			while (numsUsed(curXLoc, curYLoc,i,roomPlatforms)) {
				curXLoc = Random.Range(1,5);
				curYLoc = Random.Range(1,4);
			}
			curPlaceableIndex = Random.Range(0,PlaceableObjects.Length); //randomly place platforms or other things
			roomPlatforms [i] = new Placeable(PlaceableObjects[curPlaceableIndex],curXLoc,curYLoc);
		}		
		return roomPlatforms;
	}
	
	bool numsUsed(int x, int y, int highestIndex,Placeable[] placeArray) {
		for (int j = 0; j < highestIndex; j++)
		{
			if (placeArray[j].x == x && placeArray[j].y == y) {
				return true;
			}
		}
		return false;	
	}

	void FixedUpdate()
	{
		//TODO: comment out when no longer want to control during live gameplay and Unity Player
		GenerateBasedOnConfig (RoomPlatConfig);
	}

    public void GenerateBasedOnConfig()
    {
        GenerateBasedOnConfig(RoomPlatConfig);
    }

	void GenerateBasedOnConfig(PlatformConfigurations pC)
	{
		if (pC.Equals(PlatformConfigurations.All))
		{
			GenerateBasedOnConfig(All);
		}
		else if (pC.Equals(PlatformConfigurations.XCross))
		{
			GenerateBasedOnConfig(XCross);
		}
		else if (pC.Equals(PlatformConfigurations.O))
		{
			GenerateBasedOnConfig(O);
		}
		else if (pC.Equals(PlatformConfigurations.P))
		{
			GenerateBasedOnConfig(P);
		}
		else if (pC.Equals(PlatformConfigurations.Plus))
		{
			GenerateBasedOnConfig(Plus);
		}
		else if (pC.Equals(PlatformConfigurations.LowercaseN))
		{
			GenerateBasedOnConfig(LoweN);
		}
		else if (pC.Equals(PlatformConfigurations.LowercaseV))
		{
			GenerateBasedOnConfig(LoweV);
		}
		else if (pC.Equals(PlatformConfigurations.MidOnly))
		{
			GenerateBasedOnConfig(MidOnly);
		}
		else if (pC.Equals(PlatformConfigurations.NoMid))
		{
			GenerateBasedOnConfig(NoMid);
		}
		else if (pC.Equals(PlatformConfigurations.None))
		{
			GenerateBasedOnConfig(None);
		}
	}

	void GenerateBasedOnConfig(bool[] configToUse)
	{
		for (int i=1; i < configToUse.Length; i++) //start at 1 just so we could line up platforms with numpad
		{
			JPlats.list[i].SetActive(configToUse[i]);
		}
	}

	public int getConfigCount()
	{
		return configOptionsLength;
	}
}