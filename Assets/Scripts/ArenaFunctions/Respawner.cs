﻿using UnityEngine;
using System.Collections;

public class Respawner : OmonoPehaviour
{
	public bool InfiniteRespawnOn; //for real stages, set to false
	public GameObject OPrefab; //to be instantiated when respawn triggered
	public GameObject PPrefab; //to be instantiated when respawn triggered
	private bool stopRespawning; //need boolean for bug where infinite respawn can cause an object to be respawn when scene is destroying objects
	public GameObject spawnAnimation;

	//create listener for player death
	
	//create message for when game is over
	
	protected override void OnStart()
	{
		//register listener for player death
		Utilities.RegisterListener(new Listener(OmonoPehaviour.ms_CHARDIED,gameObject,"CharacterDied"));
		Utilities.RegisterListener(new Listener(OmonoPehaviour.ms_CHANGINGSCENE,gameObject,"SceneChanging"));

	}
	
	/** Triggered by listener, if a character has died,
	* figure out who they are and attempt to respawn them.
	*/
	void CharacterDied(CharacterDeathMessage m)  
	{
		if (stopRespawning)
		{
			return;
		}

		if (m.myCharacter.Equals(WhichCharacter.O))
		{
			RespawnCharacter(OPrefab, m.myCharacter);
		}
		else if (m.myCharacter.Equals(WhichCharacter.P))
		{
			RespawnCharacter(PPrefab, m.myCharacter);
		}
	}
	
	void RespawnCharacter(GameObject objectToSpawn, WhichCharacter wCee)
	{
		PlayerData playaToRespawn; //get player data to see if they have enough lives
		int numOfOtherPlayerLives; //see how many lives the other player has just in case both of them are out of lives
		if (InfiniteRespawnOn) //usually set to true in "for fun" areas, like training or w/e
		{
			GameObject instance = (GameObject) Instantiate(spawnAnimation,new Vector3(Random.Range (-15, 15), 0, 0), Quaternion.Euler(0,0,0));
			EntitySpawnSequence spawnAnimationInstanceScript = instance.GetComponent<EntitySpawnSequence> ();
			spawnAnimationInstanceScript.SetTheEntityToSpawn (objectToSpawn);
			//Instantiate (objectToSpawn, new Vector3 (Random.Range (-15, 15), 0, 0), Quaternion.Euler (0, 0, 0));
		}
		else
		{
			if (PlayerData.amIPlayer1(wCee)) //need to find out which player the character belonged to check how much lives they have left
			{
				playaToRespawn = PlayerData.GetPlayer1();
				numOfOtherPlayerLives = PlayerData.GetPlayer2().GetNumOfLives();
			}
			else
			{
				playaToRespawn = PlayerData.GetPlayer2();
				numOfOtherPlayerLives = PlayerData.GetPlayer1().GetNumOfLives();
			}
			
			if (playaToRespawn.GetNumOfLives() > 0) //cool, still have lives left. bring em back!
			{
				playaToRespawn.SetNumOfLives(playaToRespawn.GetNumOfLives()-1);
				GameObject instance = (GameObject) Instantiate(spawnAnimation, new Vector3 (2,2,0), Quaternion.Euler(0,0,0));
				EntitySpawnSequence spawnAnimationInstanceScript = instance.GetComponent<EntitySpawnSequence> ();
				spawnAnimationInstanceScript.SetTheEntityToSpawn (objectToSpawn);

				//Instantiate(objectToSpawn, new Vector3(2, 2, 0), Quaternion.Euler(0, 0, 0));
			}
			else //that player is out of lives, do not respawn them. can have some sort of indication that this player is not coming back until new level
			{
				if (numOfOtherPlayerLives <= 0)
				{
					//both players are out of lives
					//check to see if other player is Dead too
					//if they are, then it's game over
					//if they aren't then do nothing and just keep going
					//Broadcast message: "GAME OVER"	
				}
			}
		}
	}

	void SceneChanging(Message m)
	{
		stopRespawning = true;
	}

		void OnApplicationQuit()
		{
			stopRespawning = true;
		}
	
	
}