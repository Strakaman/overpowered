﻿using System;
using UnityEngine;
using System.Collections;

[System.Serializable]	
public class Room
{
	public int RoomLevel;
	public int minESeedLevel;
	public int maxESeedLevel;
	public float SimulSpawnChance;

	public Room(int rL, int minL, int maxL, float simSpawnChance)
	{
		RoomLevel = rL;
		minESeedLevel = minL;
		maxESeedLevel = maxL;
		SimulSpawnChance = simSpawnChance;

	}

	//Only should be used if something went horribly wrong with data load
	public Room(): this(5,1,5,0f)
	{

	}
}