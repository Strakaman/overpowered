﻿using UnityEngine;
using System.Collections;

[System.Serializable]	
public class Stage
{
	public int stageNum;
	public string stageName;
	public bool isUnlocked;
	public Room[] listOfRooms;

	public Stage(int sNu, string sNa, bool unlockDesu, Room[] roomList)
	{
		stageNum = sNu;
		stageName = sNa;
		isUnlocked = unlockDesu;
		listOfRooms = roomList;
	}

	
	//Only should be used if something went horribly wrong with data load
	public Stage():this(1,"Non-Level",true,new Room[]{new Room()})
	{

	}

	public override string ToString ()
	{
		string returnString = stageNum + " - " + stageName + " - " + isUnlocked + " - ";
		foreach (Room r in listOfRooms)
		{
			returnString += "Room: " + r.RoomLevel + ";" + r.minESeedLevel+";"+r.maxESeedLevel+";"+r.SimulSpawnChance+" ";
		}
		return  returnString;
	}

    public int GetAverageRoomRating()
    {
        int roomAverage = 0;
        foreach (Room currRoom in listOfRooms)
        {
            roomAverage += ((currRoom.maxESeedLevel + currRoom.minESeedLevel) / 2);
        }
        return (roomAverage/listOfRooms.Length);
    }
}
