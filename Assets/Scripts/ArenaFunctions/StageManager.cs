﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections.ObjectModel;

public class StageManager : OmonoPehaviour {
	private Room[] RoomList;
	public EnemySeedGenerator eGenerator; //call this scriopt to generate the list of enemies
	public PlatformGenerator pGenerator; //will use eventually to generate platforms
	public Texture2D background;
	public GUIStyle messageDisplay;
	public GameObject spawnAnimationPrefab;
	SState stageState;
	bool finalRoom; //when clearing the final room, we go from room complete to stage complete
	int currentRoomIndex; //keep track of what room we are in for that stage
	int textDrawIndicator; //draws different text string on screen depending on current situation
	int messageDuration; //how long to keep text on screen
	Queue<EnemySeed> enemiesToMake; //produced by eGenerator
	Placeable[] platformsToMake;
	GameObject[] platformsMade;
	Rect drawLocation; //location to draw variable text (probably need to move this to new UGUI)
	// Use this for initialization
	
	/*
	NOTES : UPDATE CURRENT ROOM INDEX ONCE ROOM IS EVALUATED AS COMPLETE
	EVERY UPDATE I GUESS CHECK TO SEE if enemygroup has no children enemies?
	only do it once the last enemy is generated
	and if so, check to see if it is the last room
	if it is the last room, level complete
	if it's not the last room, update room index and put game in room starting state
	
	*/
	protected override void OnStart () {
        SessionData.VersusMode = false;
        stageState = SState.StageStarting;
		finalRoom = false;
		textDrawIndicator = 0;
		drawLocation = new Rect(Screen.width/4,Screen.height/4,Screen.width/2,80);
		messageDuration	= 2;
		currentRoomIndex = 0;
		if (Utilities.inDebugMode())
		{
			if (SessionData.listOfStages == null)
			{
				SessionData.LoadStageConfigData();
				//Debug.Log(SessionData.currentStageIndex);
			}
		}
		RoomList = SessionData.listOfStages[SessionData.currentStageIndex].listOfRooms;
		Invoke ("StartStage",1);
	}
	
	// Update is called once per frame
	void Update () {
		//only bother checking to see if the room is over if the last enemy has been generated, 
		//otherwise it's a waste of resources that could lead to false positives
		if (stageState.Equals(SState.LastEnemyGenerated))
		{
			int numOfChildEnemies = 0;
			foreach (Transform child in transform) //all enemies spawned are made children of this game object so we can loop through it's children to check
			{
				if (Utilities.hasMatchingTag("Enemy",child.gameObject))
				{
					numOfChildEnemies++;
				}
			}
			if (numOfChildEnemies == 0) //based on previous check, how many enemies are actually still alive
			{
				if (finalRoom) //if this was the last room the stage is over, otherwise it just means we need to transition to the next room
				{
					stageState = SState.StageComplete;
					StartCoroutine (ShowEndMessage());
					Invoke ("EndLevel",5);
				}
				else
				{
					textDrawIndicator = 3;
					stageState = SState.RoomComplete;
					Debug.Log ("Broadcasting message that room has ended.");
					Utilities.SendToListeners (new Message (gameObject, ms_ROOMENDED));
					currentRoomIndex++;
					if (currentRoomIndex == RoomList.Length - 1)
					{
						finalRoom = true;
					}
					StartCoroutine (DelayMakeNewRoom ());
				}
				
			}
		}
		
	}
	
	void OnGUI()
	{
		//GUI.DrawTexture(new Rect (0,0,Screen.width, Screen.height),background);
		if (textDrawIndicator == 1)
		{
			GUI.Label(drawLocation,"READY...",messageDisplay);
		}
		else if (textDrawIndicator == 2)
		{
			GUI.Label(drawLocation,"ENGAGE!",messageDisplay);
		}
		else if (textDrawIndicator == 3)
		{
			GUI.Label(drawLocation,"ROOM COMPLETE!",messageDisplay);
		}
		else if (textDrawIndicator == 4)
		{
			GUI.Label(drawLocation,"STAGE COMPLETE!",messageDisplay);
		}
	}
	
	public void LoadOPMetaData()
	{
		//Not sure how we will be storing data for characters but may need to load it here
		
	}

	void StartStage()
	{
		Debug.Log ("Broadcasting message that stage has started.");
		Utilities.SendToListeners(new Message(gameObject, OmonoPehaviour.ms_STAGESTARTED));
		if (RoomList.Length == 1) {
			finalRoom = true;
		}
		MakeNewRoom();
	}

	IEnumerator DelayMakeNewRoom()
	{
		yield return new WaitForSeconds (3f);
		MakeNewRoom ();
		/*if (PlayerData.GetPlayer1().getStageInstance() != null)
		{
			PlayerData.GetPlayer1 ().getStageInstance ().transform.position = Utilities.Player1StartPosition;
		}
		if (PlayerData.GetPlayer2().getStageInstance() != null)
		{
			PlayerData.GetPlayer2 ().getStageInstance ().transform.position = Utilities.Player2StartPosition;
		}*/
	}

	public void MakeNewRoom()
	{
		stageState = SState.RoomStarting;
		//drawthat compiling screen on top of everything so players can't see everything changing?
		//reset player positions
		//should player ammo rollover to next room?
		//should they get a refill on active ammo?
		Utilities.SendToListeners(new Message(gameObject, OmonoPehaviour.ms_ROOMSTARTED));
		Debug.Log ("Broadcasting message that room has started.");
		DeleteOldPlatforms();
		enemiesToMake = eGenerator.generateEnemyList(RoomList[currentRoomIndex]);
		if (pGenerator != null)
        {
            pGenerator.RoomPlatConfig = (PlatformConfigurations)(Random.Range(0, 9)); //exclude none cuz it's no fun
            pGenerator.GenerateBasedOnConfig();
        }

        //platformsToMake = pGenerator.generatePlatformList();
		//platformsMade = MakePlatforms();//make platforms
		//start showing room 
		stageState = SState.RoomInitialized;
		StartCoroutine(ShowStartMessage());
		StartCoroutine(TriggerEnemyWave());
	}
	
	public void DeleteOldPlatforms()
	{
		if (platformsMade == null) {return;}
		foreach (GameObject g in platformsMade)
		{
			Destroy(g);
		}
	}
	
	public GameObject[] MakePlatforms()
	{
		Vector3 placeHere; 
		GameObject[] plats = new GameObject[platformsToMake.Length];
		int r=0;
		foreach (Placeable p in platformsToMake)
		{
			placeHere = new Vector3(p.x*(Screen.width/6),p.y*(Screen.height/4),0);
			plats[r++] = (GameObject)Instantiate(p.objectToBePlaced,placeHere,Quaternion.Euler(0,0,0));
		}
		return plats;
	}
	
	IEnumerator ShowStartMessage()
	{	
		textDrawIndicator = 0;
		yield return new WaitForSeconds (messageDuration);
		textDrawIndicator++;
		yield return new WaitForSeconds (messageDuration);
		textDrawIndicator++;
		yield return new WaitForSeconds (messageDuration/2f);
		textDrawIndicator = 0;
	}

	IEnumerator ShowEndMessage()
	{	
		textDrawIndicator = 4;
		yield return new WaitForSeconds (messageDuration *2);
		textDrawIndicator = 0;
	}
	
	IEnumerator TriggerEnemyWave()
	{
        SessionData.listOfRoomEnemies = new List<GameObject>();
		yield return new WaitForSeconds (messageDuration*1.5f);
		int i = 0;
		while (enemiesToMake.Count > 0)
		{
			EnemySeed kurr = enemiesToMake.Dequeue();
			GameObject instance = (GameObject)Instantiate(spawnAnimationPrefab,new Vector3(Random.Range(-17.5f,17.5f),Random.Range(-9.5f,9.5f),0),Quaternion.Euler(0,0,0)); //test numbers
			instance.transform.parent = transform;
			EntitySpawnSequence spawnAnimationInstanceScript = instance.GetComponent<EntitySpawnSequence> ();
			spawnAnimationInstanceScript.SetTheEntityToSpawn (kurr.EnemyPrefab);
			//kurr.EnemyPrefab.transform.parent = transform;
			yield return new WaitForSeconds (2 + kurr.SeedLevel/3);
		}
		stageState = SState.LastEnemyGenerated;
	}
	
	void EndLevel()
	{
		Utilities.SendToListeners(new Message(gameObject, OmonoPehaviour.ms_STAGEENDED));
		Debug.Log ("Broadcasting message that stage has ended.");
        if (SessionData.currentStageIndex < SessionData.listOfStages.Count - 1) //meaning there is another stage to unlock
        {
            SessionData.listOfStages[SessionData.currentStageIndex + 1].isUnlocked = true;
        }
	}
}