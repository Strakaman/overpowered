﻿using UnityEngine;
using System.Collections;

public class AIArtilleryStrike : AIAttack {
	public OPDirection attackOPDirection = OPDirection.right;
	public GameObject ShotToFire;

	//true, warmup 0, cooldown 4, no attack 3, no move 0, Entity target for now
	//put layer mask variable that calculates itself in OnStart method based on target type, make it easier in case layer changes
	protected override bool AttackInRange(OPDirection facingOPDirection)
	{
		return true;
	}
	
	protected override void DoTheThing (OPDirection facingOPDirection)
	{
		GameObject instanceOfRangePrefab = (GameObject)GameObject.Instantiate (ShotToFire, new Vector3 (10000, 10000), Quaternion.identity);
		EntityStateManager eManager =  GetComponent<EntityStateManager> ();
		eManager.SetAttackState(AState.shooting);
		eManager.SetOPDirectionState (attackOPDirection);
		BulletDesu bulletScript = instanceOfRangePrefab.GetComponent<BulletDesu> ();
		if (bulletScript) {
			bulletScript.setState (eManager, eManager.EntityIManage.myEntityType, eManager.EntityIManage.whoAmI);
			bulletScript.execute (gameObject);
		}
		//instanceOfRangePrefab.SendMessage ("setState", temp); //too lazy to make struct, send player direction to prefab
		//instanceOfRangePrefab.SendMessage ("execute", gameObject); //sends gameobject of entity so projectile knows where to spawn from 
	}
}
