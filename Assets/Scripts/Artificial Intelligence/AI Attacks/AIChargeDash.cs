﻿using UnityEngine;
using System.Collections;
[RequireComponent (typeof (Dash))]

public class AIChargeDash : AIAttack {
	public StrikeCollision hitBox;
	public DamageStruct ChargeAttack;

	public float attackRange = 30;

	public Dash dashScript;

	protected override bool AttackInRange (OPDirection facingOPDirection) {
		int xDir;
		if (facingOPDirection.Equals(OPDirection.left))
		{
			xDir = -1;
		}
		else
		{
			xDir = 1;
		}
		//draw a raycast with origin = curr position, directon = xDir,0
		RaycastHit2D hit = Physics2D.Raycast(new Vector2(transform.position.x,transform.position.y-1f),new Vector2(xDir,0),attackRange,targetLayerMask);
		if (hit.collider)
		{
			return true;
			
		}
		else
		{
			return false;
			
		}
	}
	
	protected override void DoTheThing (OPDirection dir) {
		hitBox.setDamageInfo(ChargeAttack, targetType);
		if (dashScript.CanDash()) {
			dashScript.DashInput ();
		}
	}
	/*
	public void Update() {
		if (!dashScript.CanDash()) {
			hitBox.collider2D.enabled = true;
		} else {
			hitBox.collider2D.enabled = false;
		}
	}

	protected void OnCollisionEnter2D(Collision2D collInfo) {
		if (Utilities.hasMatchingTag(targetType, collInfo.gameObject))
		{
			if(!dashScript.CanDash()) {
				collInfo.gameObject.SendMessage("callDamage",SendMessageOptions.DontRequireReceiver);
				collInfo.gameObject.GetComponent<Entity>().damageProperties(gameObject, damageValue,hKnockBack, vKnockBack, hitDelay);
			}
		}
	}*/
}
