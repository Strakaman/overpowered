﻿using UnityEngine;
using System.Collections;
using System;

public class AIStraightShot: AIAttack {

	public GameObject ShotToFire;
	public float RaycastRange;
	public double shotAngle;
	GroundChecker myGroundChecker;
	EntityStateManager eManager;
	//true, warmup 0, cooldown 4, no attack 3, no move 0, Entity target for now
	//put layer mask variable that calculates itself in OnStart method based on target type, make it easier in case layer changes

	protected override void OnStart()
	{
		eManager = GetComponent<EntityStateManager> ();
		myGroundChecker = GetComponent<GroundChecker> ();
		base.OnStart ();
		shotAngle = ((Math.PI / 180) * shotAngle); //allows you to set in inspector as degrees
        if (eManager.EntityIManage.myEntityType.Equals(EntityType.Player))
        {
            myCharIfCpuAlly = GetComponent<PlayerAction>().whoAmI;
        }
    }

	protected override bool AttackInRange(OPDirection facingOPDirection)
	{
		int xDir;
		if (!myGroundChecker.isGrounded ()) {
			return false;
		}
		if (facingOPDirection.Equals(OPDirection.left))
		{
			xDir = -1;
		}
		else
		{
			xDir = 1;
		}
		//draw a raycast with origin = curr position, directon = xDir,0
		RaycastHit2D hit = Physics2D.Raycast(new Vector2(transform.position.x,transform.position.y),new Vector2(xDir*(float)Math.Round(Math.Cos(shotAngle),2),(float)Math.Round(Math.Sin(shotAngle),2)),RaycastRange,targetLayerMask);
		if (hit.collider)
		{
			return true;
		}
		else
		{
			return false;

		}
	}

	protected override void DoTheThing (OPDirection facingOPDirection)
	{
		/*Vector3 clonePosition;
		Vector3 cloneVelocity;
		Quaternion cloneOrientation;
		if (facingOPDirection == OPDirection.left) {
			clonePosition = transform.position + new Vector3(-1f, 0, 0);
			cloneVelocity = new Vector3 (-15, 0, 0);
			cloneOrientation = Quaternion.Euler(0, 0, 180);
		}
		else if (direction == OPDirection.right) {
			clonePosition = transform.position + new Vector3(1,0,0);
			cloneVelocity = new Vector3 (15, 0, 0);
			cloneOrientation = Quaternion.Euler(0, 0, 180);
		}
		GameObject instance = (GameObject)Instantiate (ShotToFire, new Vector3 (10000, 10000), Quaternion.identity);
		Utilities.cloneObject(instance, clonePosition, cloneVelocity, cloneOrientation);
		Physics2D.IgnoreCollision (instance.collider2D, collider2D);
		Destroy (bulletToClone,projectileDuration);*/
		GameObject instanceOfRangePrefab = (GameObject)GameObject.Instantiate (ShotToFire, new Vector3 (10000, 10000), Quaternion.identity);
		eManager.SetAttackState(AState.shooting);
		BulletDesu bulletScript = instanceOfRangePrefab.GetComponent<BulletDesu> ();
		if (bulletScript) {
			bulletScript.setState (eManager, eManager.EntityIManage.myEntityType, eManager.EntityIManage.whoAmI);
			bulletScript.execute (gameObject);
		}
        //instanceOfRangePrefab.SendMessage ("setState", eManager); //too lazy to make struct, send player direction to prefab
        //instanceOfRangePrefab.SendMessage ("execute", gameObject); //sends gameobject of entity so projectile knows where to spawn from 
        if (eManager.EntityIManage.myEntityType.Equals(EntityType.Player))
        {
            Utilities.SendToListeners(new BonusLostMessage(gameObject, OmonoPehaviour.ms_BONUSLOST, myCharIfCpuAlly, WhichBonus.NoRange));
        }
    }



}