﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public abstract class AIAttack: OmonoPehaviour
{

		public bool isActive;
		//can have the attack be inactive as a way of making it no longer usuable, or inactivate it until activation condition is met
		public float warmUpTime;
		// time for when object is first instantiated before move can actually be used
		public float coolDownTime;
		//time before AI can use this specific attack again
		public float noAttackPenaltyTime;
		//time before AI can attack again with any attacks at all, 99% of the time should be less than cool down Time
		public float noMovementPenaltyTime;
		//time before AI can move after executing attack, bootleg way of turning off AI movement :) while attacking
		private bool inCoolDown;
		//used to keep track of if move is in cooldown
		public string targetType;
		//Entity, Player, Enemy ....hmm should we have made an Enum for this?
		protected int targetLayerMask;
		//store layer mask based on target type
		protected abstract bool AttackInRange (OPDirection facingOPDirection);

    //TODO: find a better way to figure out who character is from this script
    protected WhichCharacter myCharIfCpuAlly;

    protected abstract void DoTheThing (OPDirection dir);

		protected override void OnStart ()
		{
				if (warmUpTime > 0) {
						inCoolDown = true;
						Invoke ("CoolDownOver", warmUpTime); //reusing cooldown variable for both warmup and cool down since it's only needed once for warmup
				}
				if (targetType.Equals ("Player")) {
						targetLayerMask = Utilities.PlyrOnlyLMask;
				} else if (targetType.Equals ("Enemy")) {
						targetLayerMask = Utilities.EnemOnlyLMask;
				} else {
						targetLayerMask = Utilities.EntiOnlyLMask;
				}
		}

		//called if can attack is true by AI script
		//AI to grab attack penalty time before calling this
		public void DoAttack (OPDirection dir)
		{
				inCoolDown = true;
				Invoke ("CoolDownOver", coolDownTime);
				DoTheThing (dir);
		}

		/* returns to AI if this attack is able to be used right now
		 * If the attack is active and not in cool down and in range
		 * Then return true. Whether attack is in range or not is determined by specific attack
		 */
		public bool CanAttack (OPDirection dir)
		{
				if ((!isActive) || (inCoolDown)) {
						return false;
				} else {
						return AttackInRange (dir);
				}
		}

		//called by invoke after cooldown time has elapsed
		void CoolDownOver ()
		{
				inCoolDown = false;
		}

		public float GetAttackPenaltyTime ()
		{
				return noAttackPenaltyTime;
		}
}