﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Rigidbody2D))]
public class AIHumanoidMovement : AIMovement
{
	EntityStateManager eManager;
	Animator animator;
	GroundChecker myGroundChecker;
	Dash dashScript;
	WallJump eWallJumper;
	int howManyJumps = 3;
	
	public bool HasJumpThruIntel;
	public bool HasGroundDashIntel;
	public bool HasAirDashIntel;
	public bool HasDropThruIntel;
	public bool HasJumpOverIntel;
	public bool HasWallJumpIntel;

	//for raycasting
	private Vector3 s;
	//box collider size to help with raycasting
	private Vector3 c;
	//box collider center to help with raycasting
	
	private float myLocalScale;
	//private float xjumpOverVec = Mathf.Cos(Mathf.Deg2Rad * 80);
	//private float yJumpOverVec = -Mathf.Sin(Mathf.Deg2Rad * 80);
	private float xDistance; //displacement from target x
	private float yDistance; //displacement from target y
	private float absYDistance;
	private Vector2 p; //curr position
	private bool xOptionsExhausted = false;
	private bool yOptionsExhausted = false;
	//private bool testonce = true;
	private	WState WallJumpMode;

    //TODO: find a better way to figure out who character is from this script
    WhichCharacter myCharIfCpuAlly;

	protected override void OnStart ()
	{
		myLocalScale = Mathf.Abs (transform.localScale.x);
		eManager = GetComponent<EntityStateManager> ();
		animator = GetComponent<Animator> ();
		dashScript = GetComponent<Dash> ();
		myGroundChecker = GetComponent<GroundChecker> ();
		eWallJumper = GetComponent<WallJump> ();
		BoxCollider2D zollider = GetComponent<BoxCollider2D> ();
		s = zollider.size;
		c = zollider.offset;
		WallJumpMode = WState.normal;
        if (eManager.EntityIManage.myEntityType.Equals(EntityType.Player))
        {
            myCharIfCpuAlly = GetComponent<PlayerAction>().whoAmI;
        }
    }
	
	void UpdateDistancesAndLocations()
	{
		xDistance = target.x - transform.position.x;
		yDistance = target.y - transform.position.y;
		absYDistance = Mathf.Abs (yDistance);
		p = transform.position;
	}
	
	public override void SetTarget(Vector3 newTargetPos)
	{
		Vector3 modTargetPos = newTargetPos;
		if (transform.position.x > newTargetPos.x)
		{
			modTargetPos += new Vector3(1,0,0);
		}
		else
		{
			modTargetPos -= new Vector3(1,0,0);
		}
		base.SetTarget (modTargetPos);
	}	

		public override void BreakPatrol()
		{
				xOptionsExhausted = false;
				yOptionsExhausted = false;
		}
	
	void Update()
	{
		UpdateAnimation ();
	}
	void UpdateAnimation ()
	{
		if (GetComponent<Rigidbody2D>().velocity.x != 0f) {
			//transform.localScale = new Vector3 (myLocalScale, transform.localScale.y, transform.localScale.z);
			//eManager.SetOPDirectionState (OPDirection.right);
			animator.SetBool ("walkRight", true);
		}
	}
	public override void MovementChecks()
	{
//		if (testonce)
//					{
//						testonce = false;
//						Invoke("WallJump",2);
//
//					}
		//increaseHTransform ();
		//by putting patrol check above other code, allows AI to go into patrol mode which it will need another script to tell it when to break
		if (xOptionsExhausted && yOptionsExhausted) 
		{
			patrol();
			Debug.Log ("patrolling");
			return;
		}
		UpdateDistancesAndLocations ();
		if (!WallJumpMode.Equals (WState.normal)) {
			Debug.Log ("evaluating wall jump mode");
			EvaluateWallJumpMode ();
			return;
		}
		xOptionsExhausted = false;
		yOptionsExhausted = false; //assume you have options and change if you don't
		if (Mathf.Abs(xDistance) > .75f)
		{	
			if (ShouldDash ()) {
				//	pAction.DashInput ();
				if (dashScript.DashInput ()) { 
					//let dash script return a boolean to tell you whether or not you are actually going to dash, useful for restricting multiple air dashes
					animator.SetBool ("Dash", true);
					if (!myGroundChecker.isGrounded())
					{
						dashScript.SetDashRestricted(true);
					}
                    if (eManager.EntityIManage.myEntityType.Equals(EntityType.Player))
                    {
                        Utilities.SendToListeners(new BonusLostMessage(gameObject, OmonoPehaviour.ms_BONUSLOST, myCharIfCpuAlly, WhichBonus.NoDash));
                    }
                }
			}
			else if (eManager.Equals (EState.normal)) {
				if (xDistance > 0) {
					increaseHTransform ();
				} 	else if (xDistance < 0) {
					decreaseHTransform ();
				}
			}
		}
		else
		{
			xOptionsExhausted = true;
		}

		
		if (ShouldJumpThru ()) {
			Jump ();
        }
		else if (ShouldDropThru ()) {
			myGroundChecker.DropThrough ();
		}
		else if (HasJumpOverIntel && yDistance > -.5f && FacingEndOFLedge ()) {
			Jump();
		}
		else if ((myGroundChecker.isGrounded()) && (absYDistance > 4)) //if no vertical option was executed and you aren't horizontally aligned with grounded player, you should patrol
		{
			if (HasWallJumpIntel &&xOptionsExhausted) { 
				//but if you have wall jump enabled and your opponent is above you, and you have already tried to get to them via x-axis, maybe you should wall jump
				if (yDistance > -.5f) {
					WallJumpMode = WState.wallLooking;
				} else {
					yOptionsExhausted = true;
				}
			}
			else {
				yOptionsExhausted = true;
			}

		}
	}
	
	bool ShouldJumpThru ()
	{
		/* -If AIHasJmpIntel == true
				* -if O/P is above target and O/P is more than 1/5 of vertical space above target
				* -if upward raycast of jump length hits JPlat
				* -return true
				*/
		if (HasJumpThruIntel) {
			//float distance = target.transform.position.y - transform.position.y; //get distance between you and target
			//Debug.DrawRay(new Vector3(x,y,transform.position.z),new Vector3(0,1,0));
			if ((yDistance > 0) && (absYDistance > 4)) {
				//Vector2 p = transform.position; //current position
				float x = p.x + c.x; //middle
				float y = (p.y + c.y + s.y / 2); // top
				//draw ray cast at head of object going upwards to see if there is a jump through platform above them
				RaycastHit2D hit = Physics2D.Raycast (new Vector2 (x, y), new Vector2 (0, 1), 4f, Utilities.ObstOnlyLMask);
				if ((hit.collider) && (hit.collider.gameObject) && Utilities.hasMatchingTag ("DropThru", hit.collider.gameObject)) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	bool ShouldDash ()
	{
		if (HasGroundDashIntel || HasAirDashIntel) {
			//float distance = target.transform.position.x - transform.position.x;
			if ((xDistance < 0) && (eManager.Equals (OPDirection.right))) {
				return false; //return false if facing right and enemy is left of you
			}
			if ((xDistance > 0) && (eManager.Equals (OPDirection.left))) {
				return false;
			}
			if ((Mathf.Abs (xDistance) > 12)) {
				if (dashScript.CanDash ()) {
					if (myGroundChecker.isGrounded ()) {
						if (HasGroundDashIntel) {
							return true;
						}
					} else {
						if (HasAirDashIntel && GetComponent<Rigidbody2D>().velocity.y < -.5) {
							//should only air dash if you are falling not jumping, otherwise you could cut your jump short and miss the platform you were aiming for
							return true; 
						}
					}
				}
			}
		}
		return false;
	}
	
	bool ShouldDropThru ()
	{
		if (HasDropThruIntel) {
			if (myGroundChecker.isGrounded ()) {
				//float distance = target.transform.position.y - transform.position.y;
				if ((yDistance < 0) && (absYDistance > 4)) {
					if (myGroundChecker.CanDropThrough ()) {
						return true;
					}
				}
			}
		}
		
		return false;
	}
	
	bool FacingEndOFLedge ()
	{
		if (!myGroundChecker.isGrounded ()) {
			return false;
		}
		int xDir;
		if (eManager.Equals (OPDirection.right)) {
			xDir = 1;
		} else {
			xDir = -1;
		}
		//Vector2 p = transform.position; //current position
		float x = p.x + c.x + (myLocalScale * s.x / 2); //right
		float y = (p.y + c.y + (myLocalScale * s.y / 2)); // top
		//Debug.DrawRay(new Vector2(x,y),new Vector2(xDir * Utilities.xjumpOverVec,Utilities.yJumpOverVec));
		RaycastHit2D hit = Physics2D.Raycast (new Vector2 (x, y), new Vector2 (xDir * Utilities.xjumpOverVec, Utilities.yJumpOverVec), 2f, Utilities.ObstWallMask); //raycast against walls and floors
		if ((hit.collider) && (hit.collider.gameObject)) {
			return false;
		}
		return true;
	}
	
	void Jump()
	{
		if (myGroundChecker.isGrounded ()) { 
			GetComponent<Rigidbody2D>().velocity = new Vector2 (GetComponent<Rigidbody2D>().velocity.x, Utilities.JumpSpeed);
			dashScript.InterruptDash ();
            if (eManager.EntityIManage.myEntityType.Equals(EntityType.Player))
            {
                Utilities.SendToListeners(new BonusLostMessage(gameObject, OmonoPehaviour.ms_BONUSLOST, myCharIfCpuAlly, WhichBonus.NoJump));
            }
        }
	}
	
	void decreaseHTransform ()
	{
		GetComponent<Rigidbody2D>().velocity = new Vector3 (-Utilities.HorMaxSpeed, GetComponent<Rigidbody2D>().velocity.y);
		transform.localScale = new Vector3 (-myLocalScale, transform.localScale.y, transform.localScale.z);
		eManager.SetOPDirectionState (OPDirection.left);
	}
	
	void increaseHTransform ()
	{
		GetComponent<Rigidbody2D>().velocity = new Vector3 (Utilities.HorMaxSpeed, GetComponent<Rigidbody2D>().velocity.y);
		transform.localScale = new Vector3 (myLocalScale, transform.localScale.y, transform.localScale.z);
		eManager.SetOPDirectionState (OPDirection.right);
	}

	void patrol() {
		int xDir;
		if (eManager.Equals (OPDirection.right)) {
			xDir = 1;
		} else {
			xDir = -1;
		}
		RaycastHit2D hit = Physics2D.Raycast (transform.position, new Vector2 (xDir, 0), 2f, Utilities.WallOnlyLMask);
		if ((hit.collider) && (hit.collider.gameObject)) {
			if (xDir == 1) { //facing right, turn around
				decreaseHTransform();
			} else { //facing left, turn around
				increaseHTransform();
			} 
		} else {
			if (xDir == 1) { //facing right, keep going right
				increaseHTransform();
			} else { //facing left, keep going left
				decreaseHTransform();
			} 
		}
	}

	void WallJump ()
	{
		WallJumpMode = WState.wallJumping;
		if (absYDistance > 8) { //use y distance from target to determine how many wall jumps will be needed to reach them
			howManyJumps = 3;
		} else if (absYDistance > 4) {
			howManyJumps = 2;
		} else {
			howManyJumps = 1;
		}
		if (myGroundChecker.isGrounded ()) {
			Jump ();
			Invoke ("WallJump", .5f);
		} else {
			StartCoroutine ("WallJumpCycle", howManyJumps);
		}
	}

	void EvaluateWallJumpMode()
	{
		if (WallJumpMode.Equals (WState.wallLooking)) {
			int dir;
			if (eWallJumper.CanWallJump (out dir)) {
				WallJumpMode = WState.atWall;
			} else {
				if (p.x > 0) {
					increaseHTransform ();
				} else {
					decreaseHTransform ();
				}
			}				
		} else if (WallJumpMode.Equals (WState.atWall)) { //don't wall jump if target is below you
						if ((absYDistance > 4) && (yDistance > -.5f)) {
								WallJump ();
						} else {
								WallJumpMode = WState.normal;
						}
		} else if (WallJumpMode.Equals (WState.wallJumping)) {
			//break wall jump coroutine if target is now below you
			if (yDistance < -.5f) {
				WallJumpMode = WState.normal;
			}
		}
	}
	IEnumerator WallJumpCycle (int numOfTimes)
	{
		while (numOfTimes > 0) { //basically how many wall jumps to perform in series
			numOfTimes--;
			int direction; //pAction wall jump code from here
			if (eWallJumper.CanWallJump (out direction)) {
				dashScript.InterruptDash ();
				dashScript.SetDashRestricted (false);
				eWallJumper.PerformWallJump (direction); //to here
				if (numOfTimes > 0) { //every jump other than the last jump, AI should go towards the wall
					float translationDuration = .3f; //delay in between wall jump attempts
					while (translationDuration > 0) { //loop to make sure AI keeps heading towards the wall
						if (direction < 0) { //less than 0 means u just wall jumped to the left so for multiple jumps you should head back right
							increaseHTransform ();
						} else { //should be heading left for next jump
							decreaseHTransform ();
						}
						yield return new WaitForSeconds (.1f); //wait a tenth of a second between calls
						translationDuration -= .1f;
					}
				}
			}
		}
		WallJumpMode = WState.normal;
	}
	
}