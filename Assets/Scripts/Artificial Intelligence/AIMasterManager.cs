﻿using UnityEngine;
using System.Collections;

public class AIMasterManager : OmonoPehaviour
{
		public AITargeting AITargetingProtocol;
		public AIMovement AIMovementProtocol;
		public AIAttack[] AIAttackOptions;
		private bool inNoAttackCooldown;
		private float movementCooldownDuration;
		private float targetUpdateCooldown = 4;
		private float targetLocationUpdateCooldown = 2;
		protected Vector3 target;
		EntityStateManager eManager;

		protected override void OnStart ()
		{
				eManager = GetComponent<EntityStateManager> ();
				Invoke ("UpdateTarget", .1f);
				Invoke ("UpdateTargetLocation", .5f);
//

				movementCooldownDuration = 0f;
		}

		void Update ()
		{
				if (movementCooldownDuration > 0) {
						movementCooldownDuration -= Time.deltaTime;
				}
		}


		void FixedUpdate ()
		{
				if (!inNoAttackCooldown) {
						AttackChecks ();
				}

				if (movementCooldownDuration <= 0) {
						AIMovementProtocol.MovementChecks ();
				}
		}

		void AttackChecks()
		{
				foreach (AIAttack currAttack in AIAttackOptions)
				{
						if (currAttack.CanAttack(eManager.GetDirState()))
						{
								if (currAttack.noAttackPenaltyTime > 0)
								{
										inNoAttackCooldown = true;
										Invoke("NoAttackCooldownOver", currAttack.noAttackPenaltyTime);
								}

								if (currAttack.noMovementPenaltyTime > 0)
								{
										movementCooldownDuration = currAttack.noMovementPenaltyTime;
								}
								AIMovementProtocol.BreakPatrol (); //having an attack option means unit doesn't need to patrol. 		
								currAttack.DoAttack(eManager.GetDirState());
								break; //found the attack we wanna use so lets use it!
						}
				}
		}

		protected void NoAttackCooldownOver()
		{
				inNoAttackCooldown = false;
		}


		protected void UpdateTarget()
		{
				target = AITargetingProtocol.UpdateTarget();
				AIMovementProtocol.SetTarget(target);
				Invoke ("UpdateTarget", targetUpdateCooldown);
		}

		protected void UpdateTargetLocation()
		{
				target = AITargetingProtocol.UpdateTargetLocation();
				AIMovementProtocol.SetTarget(target);
				Invoke ("UpdateTargetLocation",.5f);
		}

}