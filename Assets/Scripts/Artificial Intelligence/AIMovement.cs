﻿using UnityEngine;
using System.Collections;


public abstract class AIMovement : OmonoPehaviour
{
		public abstract void MovementChecks();
		protected Vector3 target;

		public virtual void SetTarget(Vector3 newTargetPos)
		{
				target = newTargetPos;
		}

		public abstract void BreakPatrol ();
				//used so AI Master manager can tell movement script to break patrol when it's doing it.
}