﻿using UnityEngine;
using System.Collections;

public class AINoJumpMovement : AIMovement {
	public bool HasPatrolIntel;
	public float speed;
	private float startTimer = 0;
	private float maxTimer = 3;
	private bool moving;

	EntityStateManager eManager;
	Animator animator;

	private float xDistance; //displacement from target x
	private float yDistance; //displacement from target y
	private Vector2 p; //curr position
	private float myLocalScale;

	protected override void OnStart ()
	{
		myLocalScale = Mathf.Abs (transform.localScale.x);
		eManager = GetComponent<EntityStateManager> ();
		animator = GetComponent<Animator> ();
		BoxCollider2D zollider = GetComponent<BoxCollider2D> ();
	}

	protected void Update() {
		startTimer += Time.deltaTime;
		UpdateAnimation ();
	}

	void UpdateAnimation ()
	{
		if (GetComponent<Rigidbody2D>().velocity.x != 0) {
			//transform.localScale = new Vector3 (myLocalScale, transform.localScale.y, transform.localScale.z);
			//eManager.SetOPDirectionState (OPDirection.right);
			animator.SetBool ("walkRight", true);
		}
	}

	public override void MovementChecks() {
		if (target == null) {
			Debug.Log ("is this null check necessary?");
			return;
		}
		UpdateDistancesAndLocations ();
		if (yDistance > -4 && yDistance < 4) {
			if (Mathf.Abs(xDistance) > .75f)
			{			
				if (eManager.Equals (EState.normal)) {					
					if (xDistance > 0) {
						increaseHTransform ();
					} 	else if (xDistance < 0) {
						decreaseHTransform ();
					}
				}
			}
		} else {
			if (HasPatrolIntel) {
				patrol();
			}
		}
	}

	void UpdateDistancesAndLocations()
	{
		xDistance = target.x - transform.position.x;
		yDistance = target.y - transform.position.y;
		p = transform.position;
	}

	void patrol() {
		int xDir;
		if (eManager.Equals (OPDirection.right)) {
			xDir = 1;
		} else {
			xDir = -1;
		}
		RaycastHit2D hit = Physics2D.Raycast (transform.position, new Vector2 (xDir, 0), 2f, Utilities.WallOnlyLMask);
		if ((hit.collider) && (hit.collider.gameObject)) {
			if (xDir == 1) { //facing right, turn around
				decreaseHTransform();
			} else { //facing left, turn around
				increaseHTransform();
			} 
		} else {
			if (xDir == 1) { //facing right, keep going right
				increaseHTransform();
			} else { //facing left, keep going left
				decreaseHTransform();
			} 
		}
	}

	void decreaseHTransform ()
	{
		GetComponent<Rigidbody2D>().velocity = new Vector3 (-Utilities.HorMaxSpeed, GetComponent<Rigidbody2D>().velocity.y);
		transform.localScale = new Vector3 (-myLocalScale, transform.localScale.y, transform.localScale.z);
		eManager.SetOPDirectionState (OPDirection.left);
	}
	
	void increaseHTransform ()
	{
		GetComponent<Rigidbody2D>().velocity = new Vector3 (Utilities.HorMaxSpeed, GetComponent<Rigidbody2D>().velocity.y);
		transform.localScale = new Vector3 (myLocalScale, transform.localScale.y, transform.localScale.z);
		eManager.SetOPDirectionState (OPDirection.right);
	}

		public override void BreakPatrol()
		{

		}
}
