﻿using UnityEngine;
using System.Collections;

public class AIOneOPDirectionMovement : AIMovement {
	public float speed;
	Animator animator;
	private float myLocalScale;
	private float xDistance; //displacement from target x
	private float yDistance; //displacement from target y
	private Vector2 p; //curr position
	public OPDirection direction = OPDirection.right;

	protected override void OnStart ()
	{
		myLocalScale = Mathf.Abs (transform.localScale.x);
		if (direction.Equals(OPDirection.left))
			transform.localScale = new Vector3 (-myLocalScale, transform.localScale.y, transform.localScale.z);
		if (direction.Equals(OPDirection.right))
		    transform.localScale = new Vector3 (myLocalScale, transform.localScale.y, transform.localScale.z);
	}

	public override void MovementChecks() {
		if (target == null) {
			Debug.Log ("is this null check necessary?");
			return;
		}
		UpdateDistancesAndLocations ();
		if (Mathf.Abs(xDistance) > 10f)
		{			
			if (xDistance > 0) {
				increaseHTransform ();
			} 	else if (xDistance < 0) {
				decreaseHTransform ();
			}
		}
	}

	void UpdateDistancesAndLocations()
	{
		xDistance = target.x - transform.position.x;
		yDistance = target.y - transform.position.y;
		p = transform.position;
	}

	public override void BreakPatrol ()
	{

	}

	void decreaseHTransform ()
	{
		GetComponent<Rigidbody2D>().velocity = new Vector3 (-speed, GetComponent<Rigidbody2D>().velocity.y);
	}
	
	void increaseHTransform ()
	{
		GetComponent<Rigidbody2D>().velocity = new Vector3 (speed, GetComponent<Rigidbody2D>().velocity.y);
	}
}
