﻿using UnityEngine;
using System.Collections;

public abstract class AITargeting : OmonoPehaviour {

	protected GameObject currTarget;

	public Vector3 UpdateTargetLocation()
	{
				if (currTarget == null) {
						return UpdateTarget();
				}
				else
				{
						return currTarget.transform.position;
				}
	}

		/*
	calculates the new target based on specific algorithm
*	defined by subclass
* 	after new target is selected, 
*/
		public abstract Vector3 UpdateTarget();

		public GameObject GetTargetGameObject()
		{
				return currTarget;
		}
}
