﻿using UnityEngine;
using System.Collections;

public class AITargetingForAllies : AITargeting
{
    protected override void OnStart()
    {
        Utilities.RegisterListener(new Listener(OmonoPehaviour.ms_ENTITYWASDESTROYED, gameObject, "CheckIfTargetDied"));
        Utilities.RegisterListener(new Listener(OmonoPehaviour.ms_ENTITYWASBORN, gameObject, "CheckIfINeedTarget"));
    }
    /*
     * NPC allies will target the oldest enemy in terms of the earliest to spawn
     * they will listen for the spawning of a new enemy so that if they don't have one, they will immediately get one
     * they will listen for the death of an enemy so that if their enemy died, they will instantly look for a new one
     */
    public override Vector3 UpdateTarget()
    {
        if (currTarget == null) //try to find a new opponent if you don't already have one
        {
            currTarget = FindNewOpponent();
        }
        if (currTarget == null)
        {
            return Utilities.DefaultTargetLocation;
        }
        else
        {
            return currTarget.transform.position;
        }
    }

    public GameObject FindNewOpponent()
    {
        if (SessionData.listOfRoomEnemies!= null)
        {
            foreach(GameObject enemy in SessionData.listOfRoomEnemies) //should return the first non null enemy on the list
            {
                if (enemy != null)
                {
                    return enemy;
                }
            }
        }
        return null;
    }

    public void CheckIfTargetDied(EntityDiedMessage m)
    {
        if (currTarget == null)
        {
            UpdateTarget();
        }
    }

    public void CheckIfINeedTarget(EntityCreatedMessage m)
    {
        if (currTarget = null)
        {
            currTarget = m.createdEntityObj;
        }
    }
}
