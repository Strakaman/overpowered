﻿using UnityEngine;
using System.Collections;

public class AITargetingForGenEnemies : AITargeting
{

	GameObject targetO;
	GameObject targetP;

	public override Vector3 UpdateTarget ()
	{
		targetO = Utilities.GetOInstance ();
		targetP = Utilities.GetPInstance ();
		if ((targetO == null) && (targetP == null)) { //they are both dead
			return Utilities.DefaultTargetLocation; //TODO: add a random element here so not every AI goes to the same spot when bored
		} else if (targetO == null) { //only P is alive
			currTarget = targetP;
		} else if (targetP == null) { //only O is alive	
			currTarget = targetO;
		} else { //both alive
			currTarget = FindClosestTarget (); //#Hanabi
		}
		return UpdateTargetLocation ();
	}

	public GameObject FindClosestTarget ()
	{

		float oXDist;
		float oYDist;
		float pXDist;
		float pYDist;

		oXDist = Mathf.Abs (targetO.transform.position.x - transform.position.x);
		pXDist = Mathf.Abs (targetP.transform.position.x - transform.position.x);
		oYDist = Mathf.Abs (targetO.transform.position.y - transform.position.y);
		pYDist = Mathf.Abs (targetP.transform.position.y - transform.position.y);

		float oDistance = (oXDist * oXDist) + (oYDist * oYDist);
		float pDistance = (pXDist * pXDist) + (pYDist * pYDist);

		if (oDistance < pDistance) {
			return targetO;
		} else { //changed to else since all code paths must return a value
			return targetP;
		}
	}
}

