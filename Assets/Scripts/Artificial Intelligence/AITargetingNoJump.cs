﻿using UnityEngine;
using System.Collections;

public class AITargetingNoJump : AITargeting {
	
	GameObject targetO;
	GameObject targetP;
	public override Vector3 UpdateTarget()
	{
		targetO = Utilities.GetOInstance();
		targetP = Utilities.GetPInstance();
		if ((targetO) && (targetP)) //both alive
		{
			currTarget = FindClosestTarget(); //#Hanabi
		}
		else if (targetO == null) //only P is alive
		{
			currTarget = targetP;
		}
		else if (targetP == null) //only O is alive
		{
			currTarget = targetO;
		}
		else //they are both dead
		{
			return Utilities.DefaultTargetLocation; //idfk where they should go, probably want to put a random element here
		}
		return UpdateTargetLocation ();
	}
	
	public GameObject FindClosestTarget() {
		
		float oXDist;
		float oYDist;
		float pXDist;
		float pYDist;
		
		oXDist = Mathf.Abs(targetO.transform.position.x - transform.position.x);
		pXDist = Mathf.Abs(targetP.transform.position.x - transform.position.x);
		oYDist = Mathf.Abs(targetO.transform.position.y - transform.position.y);
		pYDist = Mathf.Abs(targetP.transform.position.y - transform.position.y);
		
		float oDistance = (oXDist*oXDist) + (oYDist*oYDist);
		float pDistance = (pXDist*pXDist) + (pYDist*pYDist);

		if (((oYDist < transform.position.y + 2f) && (oYDist > transform.position.y - 2f)) 
		    && ((pYDist < transform.position.y + 2f) && (pYDist > transform.position.y - 2f))) {
			if (oXDist < pXDist) {
				return targetO;
			} else { //changed to else since all code paths must return a value
				return targetP;
			}
		} else if ((oYDist < transform.position.y + 2f) && (oYDist > transform.position.y - 2f)) {
				return targetO;
		} else if ((pYDist < transform.position.y + 2f) && (pYDist > transform.position.y - 2f)) {
				return targetP;
		} else {
			if (oDistance < pDistance) {
				return targetO;
			} else { //changed to else since all code paths must return a value
				return targetP;
			}
		}
	}
}

