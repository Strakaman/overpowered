﻿using UnityEngine;
using System.Collections;

public class AITargetingPrioritizeCharacter : AITargeting {

		public WhichCharacter CharacterToPrioritize;

		public override Vector3 UpdateTarget()
		{
				if (CharacterToPrioritize.Equals(WhichCharacter.O))
				{
						currTarget = PrioritizeO();
				}
				else 
				{
						currTarget = PrioritizeP ();
				}
				if (currTarget == null) //they are both dead
				{
			return Utilities.DefaultTargetLocation; //idfk where they should go, probably want to put a random element here
				}
				return UpdateTargetLocation ();
		}

		private GameObject PrioritizeO()
		{
				if (Utilities.GetOInstance() != null)
				{
						return Utilities.GetOInstance();
				}
				else if (Utilities.GetPInstance() != null)
				{
						return Utilities.GetPInstance(); 
				}
				else
				{
						return null;
				}
		}

		private GameObject PrioritizeP()
		{
				if (Utilities.GetPInstance() != null)
				{
						return Utilities.GetPInstance();
				}
				else if (Utilities.GetOInstance() != null)
				{
						return Utilities.GetOInstance(); 
				}
				else
				{
						return null;
				}
		}

}

