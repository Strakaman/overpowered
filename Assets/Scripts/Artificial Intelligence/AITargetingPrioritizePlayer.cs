﻿using UnityEngine;
using System.Collections;

public class AITargetingPrioritizePlayer : AITargeting {

		PlayerData playerPriority;
		PlayerData otherPlayer;
		public WhichPlayer PlayerToPrioritize;

		protected override void OnStart()
		{
				if (PlayerToPrioritize.Equals(WhichPlayer.Player1))
				{
						playerPriority = PlayerData.GetPlayer1();
						otherPlayer = PlayerData.GetPlayer2();
				}
				else
				{
						playerPriority = PlayerData.GetPlayer2();
						otherPlayer = PlayerData.GetPlayer1();
				}
		}
		public override Vector3 UpdateTarget()
		{
				if (playerPriority.getStageInstance())
				{
						currTarget = playerPriority.getStageInstance();
				}
				else if (otherPlayer.getStageInstance())
				{
						currTarget = otherPlayer.getStageInstance();
				}
				else //they are both dead
				{
			return Utilities.DefaultTargetLocation; //idfk where they should go, probably want to put a random element here
				}
				return UpdateTargetLocation ();
		}

}
