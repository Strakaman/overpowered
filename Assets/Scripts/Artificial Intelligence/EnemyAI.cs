﻿using UnityEngine;
using System.Collections;
using Pathfinding;

[RequireComponent (typeof (Rigidbody2D))]
public class EnemyAI : OmonoPehaviour {

	// What to chase?
	public Transform targetO;
	public Transform targetP;
	private Transform closestTarget;

	//Movement type
	//private EnemyMovement eMov;
	
	// How many times each second we will update our path
	public float updateRate = 2f;
	
	// Caching
	private Rigidbody2D rb;

	
	//The AI's speed per second
	public float speed = 300f;
	public ForceMode2D fMode;



	protected override void OnStart () {
		targetO = GameObject.FindGameObjectWithTag ("Player O").gameObject.transform;
		targetP = GameObject.FindGameObjectWithTag ("Player P").gameObject.transform;
		FindClosestTarget ();

		//eMov = GetComponent<EnemyMovement>();

		rb = GetComponent<Rigidbody2D>();

		if (closestTarget == null) {
			//Debug.LogError ("No Player found? PANIC!");
			return;
		}
	}
	

	public void FindClosestTarget() {

		float oXDist;
		float oYDist;
		float pXDist;
		float pYDist;
		if (targetO == null) {
			if (targetP == null) {
				return;
			} else {
				closestTarget = targetP;
				return;
			}
		}
		else if (targetP == null)
		{
			closestTarget = targetO;
			return;
		}
		oXDist = Mathf.Abs(targetO.position.x - transform.position.x);
		pXDist = Mathf.Abs(targetP.position.x - transform.position.x);
		oYDist = Mathf.Abs(targetO.position.y - transform.position.y);
		pYDist = Mathf.Abs(targetP.position.y - transform.position.y);

		float oDistance = (oXDist*oXDist) + (oYDist*oYDist);
		float pDistance = (pXDist*pXDist) + (pYDist*pYDist);

		//Debug.Log ("ODistance: " + oDistance + " PDistance: " + pDistance);
		if (oDistance < pDistance) {
			closestTarget = targetO;
		} else if (pDistance < oDistance) {
			closestTarget = targetP;
		}
	}
	
	void FixedUpdate () {
		FindClosestTarget ();
		if (closestTarget == null) {
			//TODO: Insert a player search here.
			return;
		}

		//Move the AI
		//rb.AddForce (dir, fMode);
		if (closestTarget.transform.position.x > transform.position.x) {
			//eMov.increaseHTransform();
		}
		if (closestTarget.transform.position.x < transform.position.x) {
			//eMov.decreaseHTransform();
		}
		if (closestTarget.transform.position.y > transform.position.y) {
			//eMov.increaseYTransform ();
		}
		if (closestTarget.transform.position.y < transform.position.y) {
			//eMov.decreaseYTransform ();
		}
		if (closestTarget.transform.position == transform.position) {
			//eMov.idle();
		}
	}
	
}
