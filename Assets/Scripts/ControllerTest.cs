using UnityEngine;
using System.Collections;

public class ControllerTest : OmonoPehaviour
{


	protected override void OnStart()
	{
		//register listener for player death
	}

	string[] inputArray1 = new string[] { "Left1", "Right1", "Up1", "Down1", "Range1", "Melee1", "Jump1", "Dash1", "Start1" };
	string[] inputArray2 = new string[] { "Left2", "Right2", "Up2", "Down2", "Range2", "Melee2", "Jump2", "Dash2", "Start2" };

	void Update()
	{
		foreach (string str1 in inputArray1)
		{
			if (Input.GetButtonDown(str1))
			{
				Debug.Log(str1 + " was pressed for Player 1.");
			}
		}
		foreach (string str2 in inputArray2)
		{
			if (Input.GetButtonDown(str2))
			{
					Debug.Log(str2 + " was pressed for Player 2.");
			}
		}
		/*if (Input.GetButtonDown("LeftO"))
		{
			Debug.Log("Left Button Pressed: LeftO");
		}

		if (Input.GetButtonDown("RightO"))
		{
			Debug.Log("Left Button Pressed: RightO");
		}

		if (Input.GetButtonDown("Up1"))
		{
			Debug.Log("Left Button Pressed: Up1");
		}

		if (Input.GetButtonDown("Down1"))
		{
			Debug.Log("Left Button Pressed: Down1");
		}

		if (Input.GetButtonDown("Range1"))
		{
			Debug.Log("Left Button Pressed: Range1");
		}

		if (Input.GetButtonDown("Melee1"))
		{
			Debug.Log("Left Button Pressed: Melee1");
		}

		if (Input.GetButtonDown("Jump1"))
		{
			Debug.Log("Left Button Pressed: Jump1");
		}

		if (Input.GetButtonDown("Dash1"))
		{
			Debug.Log("Left Button Pressed: Dash1");
		}
		if (Input.GetButtonDown("Pause1"))
		{
			Debug.Log("Left Button Pressed: Pause1");
		}*/
	}
}

/*
A = joystick button 0

B = joystick button 1

X = joystick button 2

Y = joystick button 3

LB = joystick button 4

RB = joystick button 5

Back = joystick joystick button 6

Start = joystick button 7

Left Analogue Press = joystick button 8

Right Analogue Press = joystick button 9


AXIS

Analog Axis sensitivity should be 1, digital should be 1000.

Left Analog Horizontal = X Axis

Left Analog Vertical = Y Axis

Triggers = 3rd Axis (Left: -1 - 0, Right: 0 - 1) _ (Left Trigger Axis: 9th, Right Trigger Axis: 10th (Both axis are 0-1))

Right Analog Horizontal = 4th axis

Right Analog Vertical = 5th axis

D-Pad Horizontal = 6th Axis

D-Pad Vertical = 7th Axis 
*/