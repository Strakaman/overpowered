﻿using UnityEngine;
using System.Collections;

enum DeathTurnDir {left,right};
public class DeathLiner : OmonoPehaviour {

	private float r;
	private Vector2 c;
	DeathTurnDir alwaysTurn = DeathTurnDir.right;
	OPDirection currOPDirection = OPDirection.right;
	bool isActive;
	public int speed;
	// Use this for initialization
	protected override void OnStart () {
		isActive = true; //have to be inactive by default because ball doesn't know which direction to go when it's created
		TurnToOPDirection(currOPDirection);
		CircleCollider2D zollider = GetComponent<CircleCollider2D> ();
		r = zollider.radius;
		c = zollider.offset;
		Debug.Log("" + r);
		/* possible solution to fix: when going left, check that the entire collider is passed the corner so
		you can turn right properly, this should theoretically fix the problem
		so draw a ray upwards from the point of (position + center x + radius, position + center.y)

*/
}

	void SetOPDirection(OPDirection dir)
	{
		currOPDirection = dir;
	}

	void SetActive()
	{
		isActive = true;
	}

	public int GetSpeed()
	{
		return speed;
	}
	bool CanGoInOPDirection(OPDirection theD)
	{
		//Vector2 origin = new Vector2(transform.position.x, transform.position.y);
		Vector2 origin1;
		Vector2 origin2;
		Vector2 direction; 
		if (theD.Equals(OPDirection.right))
		{
			origin1 = new Vector2(transform.position.x + c.x, transform.position.y + c.y + r/2+.01f);
			origin2 = new Vector2(transform.position.x + c.x, transform.position.y + c.y - r/2-.01f);
			direction = new Vector2(1,0);
		}
		else if (theD.Equals(OPDirection.left))
		{
			origin1 = new Vector2(transform.position.x + c.x, transform.position.y + c.y + r/2+.01f);
			origin2 = new Vector2(transform.position.x + c.x, transform.position.y + c.y - r/2-.01f);
			direction = new Vector2(-1,0); 
		}
		else if (theD.Equals(OPDirection.up))
		{
			origin1 = new Vector2(transform.position.x + c.x + r/2+.01f, transform.position.y + c.y);
			origin2 = new Vector2(transform.position.x + c.x - r/2-.01f, transform.position.y + c.y);
			direction = new Vector2(0,1); 
		}
		else //this means the passed in direction was right
		{
			origin1 = new Vector2(transform.position.x + c.x + r/2+.01f, transform.position.y + c.y);
			origin2 = new Vector2(transform.position.x + c.x - r/2-.01f, transform.position.y + c.y);
			direction = new Vector2(0,-1); //down
		}
		return (!HitObstacle(origin1,direction,origin2));
	}

	bool HitObstacle(Vector2 vectorOrigin, Vector2 vectorOPDirection,Vector2 v2Origin)
	{
		RaycastHit2D hit = Physics2D.Raycast (vectorOrigin,vectorOPDirection, .2f, 65537);
		Debug.DrawRay(vectorOrigin,vectorOPDirection);
		RaycastHit2D hit2 = Physics2D.Raycast (v2Origin,vectorOPDirection, .2f, 65537);
		Debug.DrawRay(v2Origin,vectorOPDirection);
		RaycastHit2D hitmid = Physics2D.Raycast (transform.position,vectorOPDirection, .2f, 65537);
		Debug.DrawRay(transform.position,vectorOPDirection);
		//RaycastHit2D hit = Physics2D.Raycast (new Vector2 (transform.position.x, transform.position.y), new Vector2 (1, 0), .1f, 65537); 
		if ((hit.collider) ||(hit2.collider)||(hitmid.collider)) {
			//Debug.Log("turn because of " + hit.collider.gameObject.name);
			return true;
		}
		return false;
	}
	// Update is called once per frame
	void Update () {
		if (isActive)
		{
			if (alwaysTurn.Equals(DeathTurnDir.right))
			{
				ATRLogic();
			}
			else if (alwaysTurn.Equals(DeathTurnDir.left))
			{
				ATLLogic();
			}
		}

	
	}

	void ATRLogic()
	{
		OPDirection rightTurnDir = WhatIsRight();
		if (CanGoInOPDirection(rightTurnDir))
		{
			/*if (currOPDirection.Equals(OPDirection.left))
			{
			Vector2 bug = new Vector2(transform.position.x + c.x + r,transform.position.y + c.y);
			RaycastHit2D hit = Physics2D.Raycast (bug,new Vector2(0,1), .2f, 65537); 
			//RaycastHit2D hit = Physics2D.Raycast (new Vector2 (transform.position.x, transform.position.y), new Vector2 (1, 0), .1f, 65537); 
			if ((!hit.collider)) {
				//transform.position = new Vector3(transform.position.x-.0025f,transform.position.y,transform.position.z);
				Debug.Log("turn");
					TurnToOPDirection(rightTurnDir);
			}
			
			}*/
			TurnToOPDirection(rightTurnDir);
		}
		else if (CanGoInOPDirection(currOPDirection))
		{
			//do nothing because this means you are moving in this
			//direction already because of previous TurnToOPDirection call
		}
		else 
		{
			TurnToOPDirection(WhatIsLeft());
		}
	}

	void ATLLogic()
	{
		OPDirection leftTurnDir = WhatIsLeft();
		if (CanGoInOPDirection(leftTurnDir))
		{
			TurnToOPDirection(leftTurnDir);
		}
		else if (CanGoInOPDirection(currOPDirection))
		{
			//do nothing because this means you are moving in this
			//direction already because of previous TurnToOPDirection call
		}
		else 
		{
			TurnToOPDirection(WhatIsRight());
		}
	}

	void TurnToOPDirection(OPDirection dir)
	{
		//Debug.Log ("Turning to: " + dir.ToString());
		currOPDirection = dir;
		if (dir.Equals(OPDirection.right))
		{
			GetComponent<Rigidbody2D>().velocity = new Vector2(speed,0);
		}
		if (dir.Equals(OPDirection.left))
		{
			GetComponent<Rigidbody2D>().velocity = new Vector2(-speed,0);
		}
		if (dir.Equals(OPDirection.up))
		{
			GetComponent<Rigidbody2D>().velocity = new Vector2(0,speed);
		}
		if (dir.Equals(OPDirection.down))
		{
			GetComponent<Rigidbody2D>().velocity = new Vector2(0,-speed);
		}

	}


OPDirection WhatIsLeft()
{
	if (currOPDirection.Equals(OPDirection.down))
	{
		return OPDirection.right;
	}
	else if (currOPDirection.Equals(OPDirection.left))
	{
		return OPDirection.down;
	}
	else if (currOPDirection.Equals(OPDirection.up))
	{
		return OPDirection.left;
	}
	else 
	{
		return OPDirection.up; //this means current direction was right
	}
}
	OPDirection WhatIsRight()
	{
		if (currOPDirection.Equals(OPDirection.down))
		{
			return OPDirection.left;
		}
		else if (currOPDirection.Equals(OPDirection.left))
		{
			return OPDirection.up;
		}
		else if (currOPDirection.Equals(OPDirection.up))
		{
			return OPDirection.right;
		}
		else 
		{
			return OPDirection.down; //this means current direction was right
		}
	}
}
