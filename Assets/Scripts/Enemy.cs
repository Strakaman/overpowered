﻿using UnityEngine;
using System.Collections;

public class Enemy : OmonoPehaviour {

	public GameObject orp; //increases OverPower
	// Use this for initialization
	private int safeSpawnLayer = 15; //"EnemySpawn" layer so that when enemy spawns it doesn't hit player and can't get hit
	private int activeLayer = 10; //actual layer for "Enemies"

	protected override void OnStart ()
	{
		//gameObject.layer = safeSpawnLayer;
		//Invoke("ActivateEnemy",2);
	}
	// Update is called once per frame
	void Update () {
	
	}
	void SuperUpdate()
	{

	}
	void OnCollisionEnter2D(Collision2D collInfo)
	{
		//if it's a player, damage the player with a lot of knockback
	}

	protected override void OnDestroyOverride()
	{
		//instantiate the orp at this location
		//increase combo counter
	}

	void ActivateEnemy()
	{
		gameObject.layer = activeLayer;
	}
}

