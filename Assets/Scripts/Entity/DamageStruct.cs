﻿using UnityEngine;

[System.Serializable]
public struct DamageStruct
{
	public int damage;
	public float hKnockback;
	public float vKnockback;
	public float hitDelay;
	
	public DamageStruct (int d, float hkb, float vkb, float hd, string t)
	{
		damage = d;
		hKnockback = hkb;
		vKnockback = vkb;
		hitDelay = hd;
	}
}


