﻿using UnityEngine;
using System.Collections;

public class Dash : OmonoPehaviour {
	public float boostSpeedMagnitude = 10;
	private Vector2 boostSpeed = new Vector2(0,0);
	private bool dashCoolDownOver = true;
	private bool dashRestricted = false; //set by certain moves/actions that would restrict a dash, such as dive bomb attack
	public float boostCooldown = 2f;
	private OPDirection direction = 0;
	public EntityStateManager eManager;
	//if we store the user's gravity in a variable, we can turn it off when air dashing and turn it back on later
	private float rigidBodyGravity;
	//public GameObject DashTrail;
	private Animator animator;
	
	protected override void OnStart () {
		//gameObject.GetComponent<TrailRenderer>().enabled=false; 
				rigidBodyGravity = Utilities.gravityScale;//rigidbody2D.gravityScale;
		//animator = (Animator)GetComponent ("Animator");
	}
	
	void Update()
	{

	}

	public bool DashInput()
	{
		if ((dashCoolDownOver == true) && (!dashRestricted) && eManager.Equals(EState.normal))
		{
			StartCoroutine( Boost(.0001f) ); //Start the Coroutine called "Boost", and feed it the time we want it to boost us
			eManager.SetEntityState(EState.dashing);
			if (eManager.Equals(AState.shooting)) //so that if you dash while in the shooting animation it cuts attack animation
			{
				eManager.SetAttackState(AState.normal);
			}
			return true;
		}
		return false;
	}
	IEnumerator Boost(float boostDur) //Coroutine with a single input of a float called boostDur, which we can feed a number when calling
	{
		//gameObject.GetComponent<TrailRenderer>().enabled=true; 
		GetComponent<Rigidbody2D>().gravityScale = 0;
		float time = 0; //create float to store the time this coroutine is operating
		dashCoolDownOver = false; //set dashCoolDownOver to false so that we can't keep boosting while boosting
		//Instantiate(DashTrail, transform.position, Quaternion.identity );
		while(boostDur > time) //we call this loop every frame while our custom boostDuration is a higher value than the "time" variable in this coroutine
		{
			time += Time.deltaTime; //Increase our "time" variable by the amount of time that it has been since the last update
			//SetOPDirection ();
			direction = eManager.GetDirState();
			calcBoostSpeed();
			GetComponent<Rigidbody2D>().velocity = boostSpeed; //set our rigidbody velocity to a custom velocity every frame, so that we get a steady boost direction like in Megaman
			yield return 0; //go to next frame
		}
		//rigidbody2D.gravityScale = rigidBodyGravity;
		yield return new WaitForSeconds(.4f*1.5f);
		GetComponent<Rigidbody2D>().gravityScale = rigidBodyGravity;
		//animator.SetBool ("Dash", false);
		//gameObject.GetComponent<TrailRenderer>().enabled=false; 
		if (eManager.Equals(EState.dashing)) //only set state back to normal if they are dashing, just in case they died during a dash or press start or something
		{
			eManager.SetEntityState(EState.normal);
		}
		yield return new WaitForSeconds(boostCooldown); //Cooldown time for being able to boost again, if you'd like.
		dashCoolDownOver = true; //set back to true so that we can boost again.
	}

	public void InterruptDash()
	{
		if (eManager.Equals(EState.dashing)) {
			eManager.SetEntityState(EState.normal);
			//rigidbody2D.velocity = new Vector2(0,rigidbody2D.velocity.y); //if you interrupted your own dash, cut horizontal speed completely
		}
		GetComponent<Rigidbody2D>().gravityScale = rigidBodyGravity;
		dashCoolDownOver = true;
		//animator.SetBool ("Dash", false);
	}
	void calcBoostSpeed() {
		if (OPDirection.down == direction) {
			boostSpeed = new Vector2(0,-boostSpeedMagnitude);
		} else if (OPDirection.up == direction) {
			boostSpeed = new Vector2(0,boostSpeedMagnitude);
		} else if (OPDirection.left == direction) {
			boostSpeed = new Vector2(-boostSpeedMagnitude,0);
		} else if (OPDirection.right == direction) {
			boostSpeed = new Vector2(boostSpeedMagnitude,0);
		}
	}

	public void SetDashRestricted(bool isDashRestricted)
	{
		dashRestricted = isDashRestricted;
	}

	public bool CanDash()
	{
		return ((dashCoolDownOver == true) && (!dashRestricted) && eManager.Equals(EState.normal));
	}
	/*	void SetOPDirection ()
	{
		if (rigidbody2D.velocity.x < 0) {
			direction = OPDirection.left;
		} else if (rigidbody2D.velocity.x > 0) {
			direction = OPDirection.right;
		}
	}*/
}