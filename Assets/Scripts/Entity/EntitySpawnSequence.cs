﻿using UnityEngine;
using System.Collections;

public class EntitySpawnSequence : OmonoPehaviour {

	public GameObject realEntitytoSpawn;
	// Use this for initialization
	protected override void OnStart (){
		Invoke ("SpawnTheEntity", 2);
	}

	void SpawnTheEntity()
	{
		if (realEntitytoSpawn) {
			GameObject theEntity = (GameObject)Instantiate (realEntitytoSpawn, transform.position, Quaternion.Euler (0, 0, 0));
			theEntity.transform.parent = transform.parent;
            if (!SessionData.VersusMode && Utilities.hasMatchingTag("Enemy",theEntity)) //if just in main menu or if non Enemy spawns, don't add to list
            {
                SessionData.listOfRoomEnemies.Add(theEntity);
            }
            Utilities.SendToListeners(new EntityCreatedMessage(gameObject, OmonoPehaviour.ms_ENTITYWASBORN, theEntity));
            //Debug.Log ("My parent should be: " + theEntity.transform.parent);
            Destroy(this.gameObject);
		}
	}

	public void SetTheEntityToSpawn(GameObject spawnThis)
	{
		realEntitytoSpawn = spawnThis;
	}
			
	// Update is called once per frame
	void Update () {
	
	}
}
