﻿using UnityEngine;
using System.Collections;

public class EntityStateManager : OmonoPehaviour {
	EState entityState;
	AState attackState;
	OPDirection entityOPDirection;
	public Entity EntityIManage;

	protected override void OnStart()
	{
		if (EntityIManage == null) {
			EntityIManage = GetComponent<Entity> ();
			Debug.LogWarning (gameObject.name + " needs there entity manager associated with their entity script");
		}
	}

	public void SetEntityState(EState stateToSet)
	{
		entityState = stateToSet;
	}

	public void SetAttackState(AState stateToSet)
	{
		attackState = stateToSet;
	}

	public void SetOPDirectionState(OPDirection stateToSet)
	{
		entityOPDirection = stateToSet;
	}

	public EState GetEntityState()
	{
		return entityState;
	}

	public AState GetAttackState()
	{
		return attackState;
	}

	public OPDirection GetDirState()
	{
		return entityOPDirection;
	}

	public bool Equals(EState stateToCheck)
	{
		return (entityState.Equals(stateToCheck));
	}

	public bool Equals(AState stateToCheck)
	{
		return (attackState.Equals(stateToCheck));
	}

	public bool Equals(OPDirection stateToCheck)
	{
		return (entityOPDirection.Equals(stateToCheck));
	}
}
