﻿using UnityEngine;
using System.Collections;

public class GroundChecker : OmonoPehaviour {

	private Vector3 s; //box collider size to help with raycasting
	private Vector3 c; //collider center to help with raycasting
	private float r; //collider radius to help with raycasting;
	private float rcos60;
	private float rsin60;
	private float raycastDistance = 1f;
	bool grounded;
	bool BoxTrueCircleFalse;
	Collider2D colliderUnderEntity; //theoretically store the platform under player/enemy. used to check if platform can be dropped through
	private float myLocalScale;

	// Use this for initialization
	protected override void OnStart () {
		myLocalScale = Mathf.Abs (transform.localScale.x);
		if (GetComponent<Collider2D>() is BoxCollider2D)
		{
		BoxTrueCircleFalse = true;
		BoxCollider2D zollider = GetComponent<BoxCollider2D> ();
		s = zollider.size;
		c = zollider.offset;
		}
		else
		{
			BoxTrueCircleFalse = false;
			CircleCollider2D zollider = GetComponent<CircleCollider2D> ();
			c = zollider.offset;
			r = zollider.radius;
			rcos60 = r * Mathf.Cos(1.0471975512f);
			rsin60 = r * Mathf.Sin(1.0471975512f);
		}
	}
	
	// Update is called once per frame
	void Update () {
		Vector2 p = transform.position;
		float x;
		float x2;
		float y;
		if (BoxTrueCircleFalse) 		{
			x = p.x + c.x - (myLocalScale * s.x / 2)-.125f; //left bottom (add an extra .125 so the ray cast extends the actual collider size)
			x2 = p.x + c.x + (myLocalScale * s.x / 2)+ .125f; //right bottom
			y = (p.y + c.y - s.y / 2); // bottom
		} else 	{
			 x =  p.x + c.x + rcos60;
			 x2 = p.x + c.x - rcos60;
			 y = p.y + c.y - rsin60;
		}
		//Debug.DrawRay(new Vector2 (x,y), new Vector2(0,-1));
		//Debug.DrawRay(new Vector2 (x2,y), new Vector2(0,-1));
		RaycastHit2D hit2 = Physics2D.Raycast(new Vector2(x2,y),new Vector2(0,-1),raycastDistance,65536);
		RaycastHit2D hit = Physics2D.Raycast(new Vector2(x,y),new Vector2(0,-1),raycastDistance,65536); //check under left and right side of player
		if ((!hit.collider) && (!hit2.collider))
		{	
			UnGroundSelf(); //if neither side of player bottom hits anything, they are airborne 
		}
		else //basically one of the colliders hit something
		{
			if (hit.collider) //if the left collider hit something, store that collider for later use 
			{
				colliderUnderEntity = hit.collider;
			}
			else //if the left collider didn't hit something, the right one did
			{
				colliderUnderEntity = hit2.collider;
			}
		}
	}

	public void GroundSelf()
	{
		grounded = true;
	}
	
	public void UnGroundSelf()
	{
		grounded = false;
	}

	public bool isGrounded()
	{
		return grounded;
	}

	public Collider2D getColliderUnderEntity()
	{
		return colliderUnderEntity;
	}

	public bool CanDropThrough()
	{
		if (colliderUnderEntity)
		{
			if (Utilities.hasMatchingTag("DropThru",colliderUnderEntity.gameObject))
			{
				return true;
			}
		}
		return false;
	}

	public void DropThrough()
	{
		Physics2D.IgnoreCollision(GetComponent<Collider2D>(),colliderUnderEntity,true); //remove the collision so that it can drop thru
		UnGroundSelf (); //Automatically unground them to fix being able to jump immediately after dropping thru a platform
	}
}
