/**
*Ladies and Gentlemen, the greatest class known to man
* OverPower mode will grant the player special abilities and passive skills, maybe even some active ones
*
*/
using UnityEngine;
using System.Collections;

public class OverPower : OmonoPehaviour
{
	private bool isActive; //use a boolean to keep track of if overpower is active
	private const float INITMAXDUR = 600f; //i think this is 10 seconds of OP but who knows
	private const int MAXOPP = 1000; //have to fill OP gauge to 1000 before you can go overpower
	private int currOPP; //this is the amount of OverPower the player has on their way to fill the gauge
	private float timeLeftInOP; //keeps track of how long you have been in OP because once the time is done it's done

	protected override void OnStart () {
		//default values on level start
		isActive = false;
		currOPP = 10;
	}

	void Update()
	{
		/* basically if OP is active, drain from the time left before OP turns off */
		if (isActive)
		{
			timeLeftInOP -= Time.deltaTime;
			if (timeLeftInOP < 0)
			{
				deactivateOP();
			}
		}
	}
	public void addOPP(int amount)
	{
		/* called by events that increase player OP such as hitting an enemy or picking up an Orp */
		currOPP += amount;
		if (currOPP > MAXOPP)
		{
			currOPP = MAXOPP;
		}
		Utilities.SendToListeners(new Message(gameObject, OmonoPehaviour.ms_OPPCHANGE));
	}

	//probably called by HUD
	public int GetOPP() 
	{
		return currOPP;
	}

	//probably called by HUD
	public int GetMaxOPP()
	{
		return MAXOPP;
	}

	/* called by input manager when user presses buttons to go into OP
	 * if user has enough OPP, activate over power
	 */ 
	public void activateOP()
	{
		if ((currOPP == MAXOPP) && (!isActive))
		{
			isActive = true;
			//play sound
			//do animation
			timeLeftInOP = INITMAXDUR;
			currOPP = 0;
		}
		Utilities.SendToListeners(new Message(gameObject, OmonoPehaviour.ms_OPPCHANGE));
	}

	/* probably called when player dies or level is over */
	public void deactivateOP()
	{
		isActive = false;
		//play sound
		//update animator
	}
	
}