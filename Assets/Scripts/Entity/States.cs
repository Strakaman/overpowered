﻿using UnityEngine;
using System.Collections;

public enum EState { 
	
	normal = 0, //player actually playing the game and can move/shoot/etc. 
	dashing = 1, //player is in the middle of a dash
	inmenus = 2, //player maybe at the main or pause menu
	dead = 9 //player just died, use to maybe pop up death menu option
	
};

public enum AState
{
	normal = 0,
	attack1 = 1,
	attack2 = 2,
	attack3 = 3,
	attack4 = 4,
	dashattack = 5,
	followupdashattack = 6,
	debug = 7,
	manup = 8,
	spintowin = 9,
	airattack = 10,
	shooting = 11,
};

public enum GState {

	normal = 0,
	menus = 1,
	loading = 2,
	gameover = 3
};

public enum SState {
	
	StageStarting = 0,
	RoomStarting = 1,
	RoomInitialized = 2,
	LastEnemyGenerated = 3,
	RoomComplete = 4,
	StageComplete = 5
};

public enum WState {
	//used to keep track of wall jump cycle for AI
	normal = 0, //not looking to wall jump
	wallLooking = 1, //headed towards a wall
	atWall = 2, //self explanatory
	wallJumping = 3, //in wall jumping mode
};
