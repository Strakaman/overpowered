﻿using UnityEngine;
using System.Collections;

public class WallJump : OmonoPehaviour {

	private Vector3 s; //box collider size to help with raycasting
	private Vector3 c; //box collider center to help with raycasting
	// Use this for initialization
	int WALLJUMPHSPEED=6;
	int WALLJUMPVSPEED=24;
	float raycastDistance = .85f;
	int raycastMask = Utilities.WallOnlyLMask;
	
	protected override void OnStart () {
		BoxCollider2D zollider = GetComponent<BoxCollider2D> ();
		s = zollider.size;
		c = zollider.offset;
	}
	
	// Update is called once per frame
	void Update () {
		//int num;
		//Debug.Log (CanWallJump(out num));
	}

	public bool CanWallJump(out int dir)
	{
		Vector2 p = transform.position;
		// Check collisions left and right to see if user is next to a wall
		for (int i = 0; i<3; i ++) { //use loop to create three rays equally distributed of player
			float x = p.x + c.x + s.x / 2 * -1; //check the left of the player for a wall first
			float y = (p.y + c.y - s.y / 2) + s.y /2 * i; // shoot a ray from the top, middle, and bottom
			float x2 = x + s.x; //gets other side of player 
			//Ray ray = new Ray (new Vector3(x,y,0), new Vector3 (-1,0,0)); //create ray and cast it left
			//RaycastHit2D hit = Physics2D.Raycast(ray.origin,ray.direction,.1f,9); //.1f to be more or less touching wall
			//Debug.DrawRay(new Vector2(x,y),new Vector2(-1,0));
			RaycastHit2D hit = Physics2D.Raycast(new Vector2(x,y),new Vector2(-1,0),raycastDistance,raycastMask); //check left side for wall jump, then right side
			RaycastHit2D hit2 = Physics2D.Raycast(new Vector2(x2,y),new Vector2(1,0),raycastDistance,raycastMask);
			
			if (hit && hit.collider && Utilities.hasMatchingTag("Wall",hit.collider.gameObject))  {
				dir = 1; //because there is a wall on the left, the wall jump should be to the right
				return true;
			}
			else if(hit2 && hit2.collider && Utilities.hasMatchingTag("Wall",hit2.collider.gameObject)){
				dir = -1; //because there is a wall on the right, wall jump should be to the left
				return true;
			}
		}
		dir = 0;
		return false;
	}

	public void PerformWallJump(int dir)
	{
		GetComponent<Rigidbody2D>().velocity = new Vector2(WALLJUMPHSPEED * dir,WALLJUMPVSPEED);
	}
}
