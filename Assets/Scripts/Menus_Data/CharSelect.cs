﻿using UnityEngine;
using System.Collections;

public class CharSelect : OmonoPehaviour
{
    public DPadButtons dPadScript1;
    public DPadButtons dPadScript2;
    int p1Index;
    int p2Index;
    public Texture2D oTexture;
    public Texture2D pTexture;
    public Texture2D background;
    public Texture2D p1ChoiceIcon;
    public Texture2D p2ChoiceIcon;
    private bool p1confirmed;
    private bool p2confirmed;

    protected override void OnStart()
    {
        p1Index = 2;
        p2Index = 2;
        p1confirmed = false;
        p2confirmed = false;
        dPadScript1.SetAxisControls(MenuControls.LR1, MenuControls.UD1);
        dPadScript2.SetAxisControls(MenuControls.LR2, MenuControls.UD2);
    }

    void Update()
    {
        if (p1confirmed && p2confirmed)
        {
            GoToNextArea();
        }
        if (p1confirmed)
        {
            if (Input.GetButtonDown(MenuControls.Back1))
            {
                p1confirmed = false;
            }
          if (Input.GetButtonDown(MenuControls.Confirm1))
            {
                SessionData.SinglePlayerMode = true;
                GoToNextArea();
            }  
        }
        else
        {
            if (dPadScript1.LeftPressed())
            {
                if (p1Index > 1 && (p1Index > 2 || p2Index > 1))
                {
                    p1Index--;
                }
            }
            if (dPadScript1.RightPressed())
            {
                if (p1Index < 3 && (p1Index < 2 || p2Index < 3))
                {
                    p1Index++;
                }
            }
            if (Input.GetButtonDown(MenuControls.Confirm1))
            {
                if (p1Index != 2)
                {
                        p1confirmed = true;
                }
            }
            if (Input.GetButtonDown(MenuControls.Back1))
            {
                Debug.Log("Player 1 wants to go back to the main menu");
            }
        }
        if (p2confirmed)
        {
            if (Input.GetButtonDown(MenuControls.Back2))
            {
                p2confirmed = false;
            }
            if (Input.GetButtonDown(MenuControls.Confirm2))
            {
                SessionData.SinglePlayerMode = true;
                GoToNextArea();
            }
        }
        else
        {
            if (dPadScript2.LeftPressed())
            {
                if (p2Index > 1 && (p2Index > 2 || p1Index > 1))
                {
                    p2Index--;
                }
            }
            if (dPadScript2.RightPressed())
            {
                if (p2Index < 3 && (p2Index < 2 || p1Index < 3))
                {
                    p2Index++;
                }
            }
            if (Input.GetButtonDown(MenuControls.Confirm2))
            {
                if (p2Index != 2)
                {
                        p2confirmed = true;
                }
            }
            if (Input.GetButtonDown(MenuControls.Back2))
            {
                Debug.Log("Player 2 wants to go back to the main menu");
            }
        }
    }

    void OnGUI()
    {
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), background);
        GUI.DrawTexture(new Rect(p1Index * (Screen.width / 4), (3 * Screen.height / 7), 128, 76), p1ChoiceIcon);
        GUI.DrawTexture(new Rect(p2Index * (Screen.width / 4), (4 * Screen.height / 7), 128, 76), p2ChoiceIcon);
        GUI.DrawTexture(new Rect((2 * Screen.width / 8) - 30, (Screen.height / 20), 200, 200), oTexture);
        GUI.DrawTexture(new Rect(6 * (Screen.width / 8) - 30, (Screen.height / 20), 200, 200), pTexture);
        if (p1confirmed)
        {
            GUI.Label(new Rect(Screen.width / 2 - 100, 8 * Screen.height / 10, 200, 50), "Player 1 Confirmed");
            if (!SessionData.SinglePlayerMode)
            {
                GUI.Label(new Rect(Screen.width / 2 - 200, 17 * Screen.height / 20, 400, 50), "Press Confirm Again for Single Player Mode.");
            }
        }
        if (p2confirmed)
        {
            GUI.Label(new Rect(Screen.width / 2 - 100, 8 * Screen.height / 10, 200, 50), "Player 2 Confirmed");
            if (!SessionData.SinglePlayerMode)
            {
                GUI.Label(new Rect(Screen.width / 2 - 200, 17 * Screen.height / 20, 400, 50), "Press Confirm Again for Single Player Mode.");
            }
        }
        GUI.Label(new Rect(20, Screen.height - 60, Screen.width - 50, 50), "(Player1) Move: W/A/S/D   Back/Shoot: F   Confirm/Melee: G   Jump: H                                                     " +
            "(Player2) Move: Arrow keys   Back/Shoot: Num1   Confirm/Melee: Num2   Jump: Num3");
    }

    void GoToNextArea()
    {
        PlayerData P1;
        PlayerData P2;
        if (p1Index == 1) //Player1 picked O
        {
            P1 = new PlayerData(WhichCharacter.O, WhichPlayer.Player1, false);
            P2 = new PlayerData(WhichCharacter.P, WhichPlayer.Player2, p2Index == 2); //player 2 is either CPU or human that picked P
        }
        else if (p2Index == 1)//player 2 picked O
        {
            P1 = new PlayerData(WhichCharacter.P, WhichPlayer.Player1, p1Index == 2); //player 1 is either CPU or human that picked P
            P2 = new PlayerData(WhichCharacter.O, WhichPlayer.Player2, false);
        }
        else if (p1Index == 3) //one of them is cpu and the other picked P
        {
            P1 = new PlayerData(WhichCharacter.P, WhichPlayer.Player1, false); 
            P2 = new PlayerData(WhichCharacter.O, WhichPlayer.Player2, true);
        }
        else //p2 is P and p1 is cpuO
        {
            P1 = new PlayerData(WhichCharacter.O, WhichPlayer.Player1, true);
            P2 = new PlayerData(WhichCharacter.P, WhichPlayer.Player2, false);
        }
        PlayerData.setTheSessionPlayers(P1, P2);
        Utilities.LoadLevel("StageSelect");
    }

    /*
     * 12
     * 13
     * 21
     * 23
     * 31
     * 32
     * 
     * 
     */


}
