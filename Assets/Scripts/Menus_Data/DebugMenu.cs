﻿using UnityEngine;
using System.Collections;

public class DebugMenu : OmonoPehaviour
{
	public DPadButtons dPadScript1;
	public DPadButtons dPadScript2;
	public Texture2D arrowIndicator;
	private int selectionIndex;
	public GUIStyle menuDisplayFont;
	public Texture2D background;
	
	protected override void OnStart () {
		selectionIndex = 0;
		dPadScript1.SetAxisControls (MenuControls.LR1, MenuControls.UD1);
		dPadScript2.SetAxisControls (MenuControls.LR2, MenuControls.UD2);
	}
	
	void OnGUI ()
	{
		//Draw menu options
		GUI.DrawTexture(new Rect(0,0,Screen.width,Screen.height),background);
		GUI.DrawTexture (new Rect ((2 * Screen.width / 5) - Screen.width / 30-50, (selectionIndex * 40) + (Screen.height / 12.5f)+10, 25, 25), arrowIndicator);
		GUI.Label (new Rect ((2 * Screen.width / 5), (0 * 40) + (Screen.height / 12.5f), Screen.width / 5, 40), "Reload Current Scene", menuDisplayFont);
		GUI.Label (new Rect ((2 * Screen.width / 5), (1 * 40) + (Screen.height / 12.5f), Screen.width / 5, 40), "Main Menu", menuDisplayFont);
		GUI.Label (new Rect ((2 * Screen.width / 5), (2 * 40) + (Screen.height / 12.5f), Screen.width / 5, 40), "Omari Test Scene", menuDisplayFont);
		GUI.Label (new Rect ((2 * Screen.width / 5), (3 * 40) + (Screen.height / 12.5f), Screen.width / 5, 40), "Peter Test Scene", menuDisplayFont);
		GUI.Label (new Rect ((2 * Screen.width / 5), (4 * 40) + (Screen.height / 12.5f), Screen.width / 5, 40), "Game Arena Scene", menuDisplayFont);
	}
	
	void Update()
	{
		if ((dPadScript1.DownPressed ()) || (dPadScript2.DownPressed ())) {
			if (selectionIndex < 4) {
				selectionIndex++;
			}
		} else if ((dPadScript1.UpPressed ()) || (dPadScript2.UpPressed ())) {
			if (selectionIndex > 0) {
				selectionIndex--;
			}
		} else if ((Input.GetButtonDown (MenuControls.Confirm1)) || (Input.GetButtonDown (MenuControls.Confirm2))) {
			ActOnChoice ();
		}
	}
		
		void ActOnChoice ()
		{
			if (selectionIndex == 0) {
			Utilities.LoadLevel (Utilities.LastLoadedLevelB4DebugMenu);
			} else if (selectionIndex == 1) {
			Utilities.LoadLevel ("MainMenu");
			} else if (selectionIndex == 2) {
			Utilities.LoadLevel ("Omari Test Scene");	
			} else if (selectionIndex == 3) {
			Utilities.LoadLevel ("Peter Test Scene");
			} else if (selectionIndex == 4) {
			Utilities.LoadLevel ("Arena");
			}
		}
	}