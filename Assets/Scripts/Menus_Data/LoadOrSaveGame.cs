﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Collections.Generic;

public enum GameDataAction {Load,Save};
public class LoadOrSaveGame : OmonoPehaviour {

//for debugging purposes
	public GameDataAction LoadOrSave = GameDataAction.Save;
	public Texture2D background; //background to draw behind everything
	public GUIStyle numbersDisplay; //for font,size, and color
	private const int numOfAllowedSaveFiles = 11;
	private int currentSelectionIndex;
	private int lowestSelectionIndex=0;
	private int highestSelectionIndex;
	private SaveDisplayData[] displayDatas;
	string headerString;
	string displayString;
	string displayString2;
	string displayString3;
	// Use this for initialization
	protected override void OnStart () {
	//if we are in save mode, we should not show option to create new save data
		if (!Directory.Exists(SessionData.saveDataFolderPath)) { //check to see if there is a Config folder
			//Debug.Log("Path DNE");
			Directory.CreateDirectory(SessionData.saveDataFolderPath); //create folder if it doesn't exist
		}
		if (Utilities.inDebugMode ()) { //create test save data
			#region
			SaveDisplayData lol = null;
			for (int j = 0; j < numOfAllowedSaveFiles; j++)
			{
				if ((j+1)%5==0) {continue;}
				try{
					lol = new SaveDisplayData(SessionData.saveDataFilePathPrefix+j.ToString("000")+".opsav");
					if (File.Exists(lol.fileName))
					{
						//work around because i don't know how to update an already existing file
						File.Delete(lol.fileName);
					}
					//File currFile = new File(lol.fileName);
					StreamWriter currFile = new StreamWriter(lol.fileName);
					currFile.WriteLine(lol.fileName);
					currFile.WriteLine(lol.id);
					currFile.WriteLine(lol.timeSpentPlaying);
					currFile.WriteLine("FileEnd");
					currFile.Close();
				}
				catch(Exception e)
				{
					Debug.Log(e.Message);
				}
			}
			#endregion
		}

		if (LoadOrSave.Equals(GameDataAction.Save))
		{
			headerString = "Save Game";
			//lowestSelectionIndex = 0;
		}
		else
		{
			headerString = "Load Game";
			//lowestSelectionIndex = 1;
		}
		currentSelectionIndex = lowestSelectionIndex;
		#if !UNITY_WEBPLAYER
		string[] saveFileNames = Directory.GetFiles (SessionData.saveDataFolderPath, "SAVE*.opsav",SearchOption.TopDirectoryOnly);
		//Debug.Log (saveFiles.Length);
		displayDatas = new SaveDisplayData[numOfAllowedSaveFiles];
		foreach (string filepathname in saveFileNames) {
			SaveDisplayData currDisplayData = new SaveDisplayData(filepathname);
			//Debug.Log(filepathname.Substring(filepathname.IndexOf('.')-3,3));
			try {
			int num = Int32.Parse(filepathname.Substring(filepathname.IndexOf('.')-3,3));
			if (num >= 0 && num < numOfAllowedSaveFiles)
				{
					displayDatas[num] = currDisplayData;
					Debug.Log ("Winning");
				}
			}
			catch (Exception e)
			{
				Debug.Log("Issue with save file parsing: " + e.Message);
			}
		}

		PopulateCurrentDisplayData ();
#endif
		/*
		 * Loading
		 * count how many save data file in folder
		 * set highest index equal to number of files
		 * it doesn't make sense to read every save data into file, but
		 * but we might have to read a bit of every save data into file as needed
		 * this means the relevant save data because we have to display something to the user so they know which save data is which
		 * so if the only thing i wanted to display was the players level and (lets pretend we are actually keeping track of play time)
		 *  the play time, i should just grab that data once per file and load it into memory.
		 * then when i get the actual file i want, I get all the data corresponding to that file
		 *
		 * create display data class
		 * file name that data is grabbed from
		 * data read from the file
		 *
		 * loop through each file and grab the small data from the first few lines
		 *
		 * use index to track which savedata is being highlited.
		 * when user selects that data to load, reopen the file and grab all the data for the real load data
		 * when user selects save over that save file, create a temp file, fill temp file with data then move save file data to actual file.
		 * when user selects new save file, we have to count the number of files, then add 1 to that number, and use that number to append to file name of new file or something
		 *
		 *
		 * or should we just do save slots?
		 *
		 *
		 *
		 * */
	}

	void PopulateCurrentDisplayData()
	{
		SaveDisplayData hmm = displayDatas [currentSelectionIndex];
		if (hmm == null) {
			Debug.Log("is null yo");
			displayString = "Empty Slot";
			displayString2 = "";
			displayString3 = "";
			return;
		}
		if (hmm.dataLoaded == false)
		{
			Debug.Log("should happen once per so 8 total");
			LoadDisplayDataFromFile(hmm);
		}
		if (hmm.dataLoaded == true) { //should be true because of method call but in case there was an exception, display strings could be null
			displayString = hmm.fileName;
			displayString2 = "Divisible by 7: " + hmm.id;
			displayString3 = "Time Spent Playing: " + hmm.timeSpentPlaying;
		} else {
			displayString = "Data Corrupt";
			displayString2 = "";
			displayString3 = "";
		}
	}

	void LoadDisplayDataFromFile(SaveDisplayData SDD)
	{
		try
		{
			StreamReader currFile = new StreamReader(SDD.fileName);
			currFile.ReadLine();
			SDD.id = Int32.Parse(currFile.ReadLine());
			SDD.timeSpentPlaying = Int32.Parse(currFile.ReadLine());
			SDD.dataLoaded = true;
			currFile.Close();
		}
		catch (Exception e)
		{
			Debug.Log(e.Message);

		}
	}


	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown ("u")) { //moves left if not at beginning of array
			if (currentSelectionIndex > lowestSelectionIndex) {
				currentSelectionIndex--;
				PopulateCurrentDisplayData();
			}
		}
		if (Input.GetKeyDown ("i")) { //moves right if not at end of array
			if (currentSelectionIndex < numOfAllowedSaveFiles - 1) {
				currentSelectionIndex++;
				PopulateCurrentDisplayData();
			}
		}
	}

	void BackToPreviousMenu()
	{
		Debug.Log("GOING BACK TO PREVIOUS MENU");
		if (LoadOrSave.Equals (GameDataAction.Save)) {
			//goback to whatever scene allowed us to save data
		} else {
			Utilities.LoadLevel("MainMenu");
		}
	}

	void ConfirmSaveSelection()
	{


	}


	void OnGUI(	)
	{
		GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), background); //draw BG
		GUI.Label (new Rect (Screen.width / 2, Screen.height / 40, Screen.width / 4, 50), headerString); //header text directing player
		for (int i = 0; i < currentSelectionIndex; i++) {
			//for loop condition makes selectionIndex always greater than counter, so all textures here will be drawn left of the center
			GUI.Label (new Rect (Screen.width / 2 - ((currentSelectionIndex - i) * 150) - 50, Screen.height / 3 - 37.5f, 100, 100), "" + (i+1), numbersDisplay);
		}
		
		
		for (int i = currentSelectionIndex; i < numOfAllowedSaveFiles; i++) {
			//selectionIndex = counter, so texture is drawn exactly at the center
			//rest of for loop condition makes selectionIndex always less than counter, so rest of textures will be drawn right of the center
			GUI.Label (new Rect (Screen.width / 2 + ((i - currentSelectionIndex) * 150) - 50, Screen.height / 3 - 37.5f, 100, 100), "" + (i+1), numbersDisplay);
		}
		GUI.Label(new Rect (Screen.width / 4,Screen.height - (10* Screen.height / 40),Screen.width / 3,50),displayString);
		GUI.Label(new Rect (Screen.width / 4,Screen.height - (7* Screen.height / 40),Screen.width / 3,50),displayString2);
		GUI.Label(new Rect (Screen.width / 4,Screen.height - (4*Screen.height / 40),Screen.width / 3,50),displayString3);
	}

}
