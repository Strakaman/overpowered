﻿using UnityEngine;
using System.Collections;

public class MainMenu : OmonoPehaviour
{
	public DPadButtons dPadScript1;
	public DPadButtons dPadScript2;
	bool actualMenuActive;
	public Texture2D arrowIndicator;
	private int selectionIndex;
	public GUIStyle menuDisplayFont;
	public Texture2D background;
	public MonoBehaviour[] disableTheseWhileMenuIsOpen;

	protected override void OnStart () {
			selectionIndex = 0;
			actualMenuActive = false;
        SessionData.VersusMode = true;
		SessionData.LoadStageConfigData();
				dPadScript1.SetAxisControls (MenuControls.LR1, MenuControls.UD1);
				dPadScript2.SetAxisControls (MenuControls.LR2, MenuControls.UD2);
	}

	void Update ()
	{
			if (!actualMenuActive) {
						if ((Input.GetButtonDown (MenuControls.Start1)) || (Input.GetButtonDown (MenuControls.Start2))) {
							actualMenuActive = true;
				foreach (MonoBehaviour mb in disableTheseWhileMenuIsOpen) {
					mb.enabled = false;
				}
							//Disable O and P input script
					}
			}
			else {
						if ((Input.GetButtonDown (MenuControls.Back1)) || (Input.GetButtonDown (MenuControls.Back2)) || (Input.GetButtonDown (MenuControls.Start1)) || (Input.GetButtonDown (MenuControls.Start2))) {
							BackToFighting ();
						} else if ((dPadScript1.DownPressed()) || (dPadScript2.DownPressed())) {
							if (selectionIndex < 4) {
									selectionIndex++;
							}
						} else if ((dPadScript1.UpPressed()) || (dPadScript2.UpPressed())) {
							if (selectionIndex > 0) {
									selectionIndex--;
							}
						} else if ((Input.GetButtonDown (MenuControls.Confirm1)) || (Input.GetButtonDown (MenuControls.Confirm2))) {
							ActOnChoice ();
					}
			}
	}

	void OnGUI ()
	{
		//GUI.DrawTexture(new Rect(0,0,Screen.width,Screen.height),background);

			if (actualMenuActive) {
					//Draw menu options
					//GUI.DrawTexture(new Rect(0,0,Screen.width,Screen.height),background);
					GUI.DrawTexture (new Rect ((2 * Screen.width / 5) - Screen.width / 30-15, (selectionIndex * 40) + (Screen.height / 2.5f), 25, 25), arrowIndicator);
					GUI.Label (new Rect ((2 * Screen.width / 5), (0 * 40) + (Screen.height / 2.5f), Screen.width / 5, 40), "Back To Fighting", menuDisplayFont);
					GUI.Label (new Rect ((2 * Screen.width / 5), (1 * 40) + (Screen.height / 2.5f), Screen.width / 5, 40), "Start Game", menuDisplayFont);
					GUI.Label (new Rect ((2 * Screen.width / 5), (2 * 40) + (Screen.height / 2.5f), Screen.width / 5, 40), "LoadGame", menuDisplayFont);
			GUI.Label (new Rect ((2 * Screen.width / 5), (3 * 40) + (Screen.height / 2.5f), Screen.width / 5, 40), "Credits", menuDisplayFont);
			GUI.Label (new Rect ((2 * Screen.width / 5), (4 * 40) + (Screen.height / 2.5f), Screen.width / 5, 40), "Quit Game", menuDisplayFont);
			}
        GUI.Label(new Rect(20, Screen.height - 60, Screen.width - 50, 50), "(Player1) Move: W/A/S/D   Back/Shoot: F   Confirm/Melee: G   Jump: H                                                     " +
        "(Player2) Move: Arrow keys   Back/Shoot: Num1   Confirm/Melee: Num2   Jump: Num3");

    }
		          
	void ActOnChoice ()
	{
			if (selectionIndex == 0) {
					BackToFighting ();
			} else if (selectionIndex == 1) {
					StartGame ();
		} else if (selectionIndex == 2) {
			LoadGame ();		
		} else if (selectionIndex == 3) {
			Credits ();
			} else if (selectionIndex == 4) {
					QuitGame ();
			}
	}
		
	void StartGame ()
	{
		Utilities.LoadLevel ("Character Select");
	}
		
	void BackToFighting ()
	{
			actualMenuActive = false;
			//Enable O and P input script
		foreach (MonoBehaviour mb in disableTheseWhileMenuIsOpen) {
			mb.enabled = true;
		}
	}

	void LoadGame()
	{	
		Utilities.LoadLevel("LoadOrSaveGame");
	}

	void Credits ()
	{		
		Utilities.LoadLevel ("Credits");
	}
		
	void QuitGame ()
	{
		Utilities.LoadLevel ("");
			Application.Quit ();
	}
}
