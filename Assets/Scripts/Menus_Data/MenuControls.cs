﻿using UnityEngine;
using System.Collections;

public static class MenuControls {

		public static string LR1 = "DPadX1";
		public static string UD1 = "DPadY1";
		public static string LR2 = "DPadX2";
		public static string UD2 = "DPadY2";
		public static string Confirm1 = "Melee1";
		public static string Confirm2 = "Melee2";
		public static string Back1 = "Range1";
		public static string Back2 = "Range2";
		public static string Start1 = "Start1";
		public static string Start2 = "Start2";
}
