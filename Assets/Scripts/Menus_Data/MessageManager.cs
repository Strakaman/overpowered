﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class MessageManager : MonoBehaviour {
	/** Every scene should really only have one message manager
	 * it contains a list of all the registered listeners so it can update them when necessary
	 * and every OmonoPehaviour has a reference to the messenger so it can call it's methods
	 */
	private List<Listener> listeners = new List<Listener>();

	void OnStart()
	{
		//Register self with Utilities (All this for good null checking)
		Utilities.SetMessenger(this);
	}
	void Update()
	{
		/*if (Input.GetKeyDown("space"))
		{
			foreach (Listener l in listeners)
			{
				Debug.Log(l.ListenFor);
			}
		}*/
		/*
		if (Input.GetKeyDown (KeyCode.L)) {
			Debug.Log ("Listeners Count: " + listeners.Count);
				}*/

		if (Input.GetKeyDown("0"))
		{
			if (Utilities.inDebugMode())
			{
				Utilities.LastLoadedLevelB4DebugMenu = Application.loadedLevel;
				Utilities.LoadLevel("DebugMenu");
			}
		}

	}


	/**
	 * Called by subscribers when they want to listen to a specific event
	 */
	public void RegisterListener(Listener l)
	{
		listeners.Add(l);
	}
	
	/**
	 * Uses LINQ code to update all subscribed listeners when a event/message of that nature
	 * has been triggered
	 */
	public void SendToListeners (Message m)
	{	
		foreach (var f in listeners.FindAll(l => l.ListenFor == m.MessageName)) {
			if (f.ForwardToObject != null) //I was TOLD I DON'T HAVE TO DO NULL CHECKING HERE!!!!!!!	
			{	
				f.ForwardToObject.BroadcastMessage (f.ForwardToMethod, m, SendMessageOptions.DontRequireReceiver);
			}
		}
	}
}

public class OmonoPehaviour : MonoBehaviour {
	//Observer Pattern BaseScript
	//Listeners/Observers will extend this class and subscribe to specific events
	//Messengers/Triggers/Observables will extend this class and send out messages for specific events
	//all objects will have reference to Messenger which functions as broadcaster
	protected MessageManager Messenger;
	
	public void Start()
	{
		Messenger = GameObject.Find("World").GetComponent<MessageManager>();
		if(!Messenger) Debug.LogError("World.MessageManager could not be found.  Insure there is a World object with a MessageManager script attached.");
		OnStart();
	}

	void OnDestroy()
	{
		//TODO: Remove all listeners registered to Messenger listener list?
		//mylisteners  = new listener list
		OnDestroyOverride();
	}
	
	protected virtual void OnStart()
	{
	}

	protected virtual void OnDestroyOverride()
	{

	}

	public static string ms_CHARSPAWNED = "CharacterSpawned"; //hud gets instance data 4 ammo and stuff
	public static string ms_OPPCHANGE = "OppChanged"; //hud gets new OPP value for characters
	//public static string ms_HEALTHCHANGE = "HealthChanged"; //hud gets new health value for characters
	public static string ms_EQUIPCHANGE = "EquipChanged"; //hud gets new equip value for characters
	public static string ms_AMMOCHANGE = "AmmoChanged"; //hud gets new ammo value for characters
	public static string ms_CHARDIED = "CharacterDied"; //respawner knows who to respawn, HUD knows which HUD to draw dead indicator on (we don't have a dead indicator)
	public static string ms_CHANGINGSCENE = "ChangingScene"; //need to broadcast this to turn respawner off before scene transition s no body gets respawned mid scene!
	public static string ms_ROOMSTARTED = "RoomStarted";
	public static string ms_ROOMENDED = "RoomEnded";
	public static string ms_STAGESTARTED = "StageStarted";
	public static string ms_STAGEENDED = "StageEnded";
	public static string ms_BONUSLOST = "BonusLost";
    public static string ms_ENTITYWASBORN = "EntityWasBorn"; //easy way for AI to be notified about potential new enemy
	public static string ms_ENTITYTOOKDAMAGE = "EntityTookDamage"; //used to total amount of damage player has taken and dealt
	public static string ms_ENTITYWASDESTROYED = "EntityWasDestroyed";  //used to keep track of player kills and what killed players
}

//LISTENER CLASS
public class Listener {
	public string ListenFor;
	public GameObject ForwardToObject;
	public string ForwardToMethod;
	
	public Listener(string lf, GameObject fo, string fm)
	{
		ListenFor = lf; //message name that you want to be subscribed to
		ForwardToObject = fo; //suscribed object
		ForwardToMethod = fm; //method to invoke once message received
	}
}

//MESSAGE CLASS
public class Message {
	public GameObject MessageSource 	{ get; set; }
	public string MessageName 		{ get; set; }
	
	public Message(GameObject s, string n)
	{
		MessageSource = s; //allows direct reference to game object that sent message
		MessageName = n; //lookup by message name so only those listening for a specific message type are alerted
	}
}

//ALL MESSAGE SUBCLASSES DEFINED BELOW FOR EASY REFERENCE
public class CharacterDeathMessage : Message {
	
	public WhichCharacter myCharacter;
	public CharacterDeathMessage(GameObject s, string n, WhichCharacter wChar) : base (s,n)
	{
		myCharacter = wChar;
	}
}

public class CharacterSpawnMessage : Message {
	
	public WhichCharacter myCharacter;
	public CharacterSpawnMessage(GameObject s, string n, WhichCharacter wChar) : base (s,n)
	{
		myCharacter = wChar;
	}
}

public class BonusLostMessage : Message 
{
	public WhichBonus theBonusThatWasLost;
	public WhichCharacter whichCharacter;
	public BonusLostMessage(GameObject s, string n, WhichCharacter wC, WhichBonus wBonus) : base (s,n)
	{
		whichCharacter = wC;
		theBonusThatWasLost = wBonus;
	}
}

public class EntityCreatedMessage : Message
{

    public GameObject createdEntityObj;

    public EntityCreatedMessage(GameObject s, string n, GameObject createdEntity) : base(s, n) {
        createdEntityObj = createdEntity;
    }
}

public class EntityDiedMessage : Message
{
	public EntityType AttackingEntityType;
	public EntityType ReceivingEntityType;
	public WhichCharacter AttackingCharacter;
	public WhichCharacter ReceivingCharacter;
	public GameObject theKiller;

	public EntityDiedMessage(GameObject s, string n, EntityType theAttackingEntityType, EntityType theReceivingEntityType, WhichCharacter theAttackingCharacter, WhichCharacter theReceivingCharacter, GameObject whatKilledMe) : base (s,n)
	{
		AttackingEntityType = theAttackingEntityType;
		ReceivingEntityType = theReceivingEntityType;
		AttackingCharacter = theAttackingCharacter;
		ReceivingCharacter = theReceivingCharacter;
		theKiller = whatKilledMe;
	}
}

public class EntityTookDamageMessage : Message
{
	public EntityType AttackingEntityType;
	public EntityType ReceivingEntityType;
	public WhichCharacter AttackingCharacter;
	public WhichCharacter ReceivingCharacter;
	public float DamageTaken;

	public EntityTookDamageMessage(GameObject s, string n, EntityType theAttackingEntityType, EntityType theReceivingEntityType, WhichCharacter theAttackingCharacter, WhichCharacter theReceivingCharacter, float realDamage) : base (s,n)
	{
		AttackingEntityType = theAttackingEntityType;
		ReceivingEntityType = theReceivingEntityType;
		AttackingCharacter = theAttackingCharacter;
		ReceivingCharacter = theReceivingCharacter;
		DamageTaken = realDamage;
	}
}