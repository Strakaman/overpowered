﻿using UnityEngine;
using System.Collections;

public class MultiTagScript : OmonoPehaviour
{

	public string[] objectTags;

	public bool objectHasTag (string tagToCheckFor)
	{
			if ((tagToCheckFor == null) || (objectTags.Length == 0)) {
					return false;
			}
			foreach (string curTag in objectTags) {
					if (curTag.Equals (tagToCheckFor)) {
							return true;
					}
			}
			return false;
	}
}
