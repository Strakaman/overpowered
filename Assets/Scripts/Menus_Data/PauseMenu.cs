﻿using UnityEngine;
using System.Collections;

public class PauseMenu : OmonoPehaviour {

		bool gamePaused;
		int selectionIndex;
		string pauseOwner;
		public Texture2D arrowIndicator;
		public GUIStyle menuDisplayFont;
		
		protected override void OnStart ()
		{
			gamePaused = false;
			selectionIndex = 0;
			pauseOwner = "";
		}
		
		void Update()
		{
			//check if game state is in a pauseable state (like normal)
			if (Input.GetButtonDown("Start1"))
			{
				PauseTheGame(true);
			}
			if (Input.GetButtonDown("Start2"))
			{
			PauseTheGame(false);
			}

		}
		
		void PauseTheGame(bool player1)
		{
			if (player1)
			{
				pauseOwner = "Player1";
			}
			else
			{
				pauseOwner = "Player2";
			}
			gamePaused = true;
		}
		
		void OnGUI()
		{
			if (gamePaused)
			{
				//Draw menu options
				GUI.Label(new Rect ((2*Screen.width/5), (Screen.height/2)-(40), Screen.width/5, 40), pauseOwner + " paused", menuDisplayFont);
				          GUI.DrawTexture(new Rect ((2*Screen.width/5)-Screen.width/30-15, (selectionIndex*40)+(Screen.height/2), 25, 25), arrowIndicator);
				                GUI.Label(new Rect ((2*Screen.width/5), (0*40)+(Screen.height/2), Screen.width/5, 40), "Back", menuDisplayFont);
				          GUI.Label(new Rect ((2*Screen.width/5), (1*40)+(Screen.height/2), Screen.width/5, 40), "Restart Level", menuDisplayFont);
				          GUI.Label(new Rect ((2*Screen.width/5), (2*40)+(Screen.height/2), Screen.width/5, 40), "Level Select", menuDisplayFont);
				          GUI.Label(new Rect ((2*Screen.width/5), (3*40)+(Screen.height/2), Screen.width/5, 40), "Main Menu", menuDisplayFont);
				          }
				          } void ActOnChoice()
				          {
					if (selectionIndex == 0)
					{
						BackToFighting();
					}
					else if (selectionIndex == 1)
					{
						RestartLevel();
					}
					else if (selectionIndex == 2)
					{
						LevelSelect();
					}
					else if (selectionIndex == 3)
					{
						QuitGame();
					}
				}
				
				void RestartLevel()
				{
		Utilities.LoadLevel(Application.loadedLevel);
				}
				
				void BackToFighting()
				{
					gamePaused = false;
					//Enable O and P input script
				}
				
				void LevelSelect()
				{
		Utilities.LoadLevel("LevelSelect");
				}
				
				void QuitGame()
				{
		Utilities.LoadLevel("Main Menu");
				}

				}
