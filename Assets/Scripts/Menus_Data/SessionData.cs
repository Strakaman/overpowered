﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Collections.Generic;


public class SessionData {

	//all path names are variables to make it easier to update if necessary
	public static string OPDataPath = Application.persistentDataPath;
	public static string stageConfigFolderPath = OPDataPath + "/Config";
	public static string stageConfigFilePath = stageConfigFolderPath + "/StageDataConfig.OP";
	public static string saveDataFolderPath = OPDataPath + "/Saves";
	public static string saveDataFilePathPrefix = saveDataFolderPath + "/SAVE";

	public static string testPath = "TestBaby";

	public static List<Stage> listOfStages;
	public static int currentStageIndex = 0;
	public static GState GameState = GState.normal;
	public static SaveGameData CurrGameData;
    public static bool SinglePlayerMode = false;
    public static bool VersusMode = false;
    public static List<GameObject> listOfRoomEnemies; //used for AI of NPC allies (TODO: move to a better location and devise better method of keeping list of enemies)

	public static void LoadStageConfigData()
	{
		//Debug.Log("Starting File Read");
		if (File.Exists(stageConfigFilePath))
		{
			LoadStageConfigData(stageConfigFilePath);
		}
		else if (File.Exists("Assets/Editor/StageDataConfig.OP"))
		{
			LoadStageConfigData("Assets/Editor/StageDataConfig.OP");
		}
		else
		{
			listOfStages = new List<Stage>(); //if we can't find either files we are just gonna have a game with one stage lol
			CreateAddDummyStage();
		}
		TextAsset text = Resources.Load<TextAsset> (testPath);
		Debug.Log (text.text);
	}

	public static void LoadStageConfigData(string filePath)
	{
		Debug.Log("Loading from file path: " + filePath);
	try 
		{
				listOfStages = new List<Stage>();
				//FileStream currFile = File.Open(stageConfigPath);
				string[] currStageSplit;
				string[] roomsSplit;
				string[] currRoomSplt;
				int stageNum;
				string stageName;
				bool unlockedStatus;
				int roomLvl;
				int roomMin;
				int roomMax;
				float roomChance;
				StreamReader currFile = new StreamReader(filePath); //load the stages metadata into memory
				currFile.ReadLine();
				currFile.ReadLine();
				currFile.ReadLine();
				string linePlease = "";
				linePlease = currFile.ReadLine();
				while (linePlease != null)
				{
					//Debug.Log("Current Line: "+linePlease);
					currStageSplit = linePlease.Split('|');
					stageNum = Int32.Parse(currStageSplit[0]);
					stageName = currStageSplit[1];
					unlockedStatus = Boolean.Parse(currStageSplit[2]);
					roomsSplit = currStageSplit[3].Split('^');
					Room[] currRoomArray= new Room[roomsSplit.Length];	
					for (int i = 0; i < currRoomArray.Length;i++)
				{
					currRoomSplt = roomsSplit[i].Split(';');
					roomLvl = Int32.Parse(currRoomSplt[0]);
					roomMin = Int32.Parse(currRoomSplt[1]);
					roomMax = Int32.Parse(currRoomSplt[2]);
					roomChance = float.Parse(currRoomSplt[3]);
					currRoomArray[i] = new Room(roomLvl,roomMin,roomMax,roomChance);
				}
				Stage currStage = new Stage(stageNum,stageName,unlockedStatus,currRoomArray);
					listOfStages.Add(currStage);
					//Debug.Log(currStage.ToString());
					linePlease = currFile.ReadLine();
				}
				currFile.Close();
			}
		catch (Exception e)
		{
			Debug.Log("IDK MAN: " +e.Message);
			CreateAddDummyStage();
		}
	}

	public static void CreateAddDummyStage()
	{
		listOfStages.Add(new Stage());
	}
}

//all data that should be saved when a player saves should be stored in this class.
//Some overlap with other classes will exist here
public class SaveGameData
{
	public int id;
	
}
//info that should display to the player which im not sure is necessary but prevents having to read every save file into memory
public class SaveDisplayData
{
	public string fileName;
	static int nextID = 7;
	public int id;
	public int timeSpentPlaying;
	public bool dataLoaded;
	
	public SaveDisplayData(string filename)
	{
		fileName = filename;
		id = nextID;
		timeSpentPlaying = id*10;
		nextID +=7;
		dataLoaded = false;

	}
}
