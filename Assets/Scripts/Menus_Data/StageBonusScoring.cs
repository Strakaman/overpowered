﻿using UnityEngine;
using System.Collections;

public enum WhichBonus { NoDamage, NoJump, NoDash, NoRange, NoMelee, NoDeath, NoOP };
public class StageBonusScoring : OmonoPehaviour {

	public static bool[] HasNoDamageBonus = {true, true};
	public static bool[] HasNoJumpBonus = {true, true};
	public static bool[] HasNoDashBonus = {true, true};
	public static bool[] HasNoRangeBonus = {true, true};
	public static bool[] HasNoMeleeBonus = {true, true};
	public static bool[] HasNoDeathBonus = {true, true};
	public static bool[] HasNoOPBonus = {true, true};

	public static bool [,] HasBonusArray;

	public static WhichPlayer MostKills;
	public static WhichPlayer MostBonuses;
	public static WhichPlayer LeastDamageTaken;
	public static WhichPlayer MostAssists;
	public static WhichPlayer MostDamageDealt;
	public static WhichPlayer MostOPPlayer;

	public static float P1DamageDealt;
	public static float P2DamageDealt;
	public static float P1DamageTaken;
	public static float P2DamageTaken;

	public static int P1Kills;
	public static int P2Kills;
	public static int P1Deaths;
	public static int P2Deaths;

	public static int P1MOPVotes;
	public static int P2MOPVotes;

	// Use this for initialization
	protected override void OnStart () {
		HasBonusArray = new bool [6,2];
		//Debug.Log ("Registering Listeners for Static Data Store");
		Utilities.RegisterListener(new Listener(OmonoPehaviour.ms_STAGESTARTED,gameObject,"ResetScoring"));
		Utilities.RegisterListener(new Listener(OmonoPehaviour.ms_BONUSLOST,gameObject,"PlayerLosesBonus"));
		Utilities.RegisterListener(new Listener(OmonoPehaviour.ms_ENTITYTOOKDAMAGE,gameObject,"UpdateDamageMetaData"));
		Utilities.RegisterListener(new Listener(OmonoPehaviour.ms_ENTITYWASDESTROYED,gameObject,"UpdateKillsMetaData"));
		Utilities.RegisterListener(new Listener(OmonoPehaviour.ms_STAGESTARTED,gameObject,"ResetScoring"));
	}
	
	public void ResetScoring(Message m)
	{
		//Debug.Log ("Array length: " + HasBonusArray.GetLength(0));
		Debug.Log("Call to Reset Static Data Store of Stage Stats");
		for (int i=0; i < HasBonusArray.GetLength(0); i++)
		{
			for (int j=0; j < 2; j++)
			{
				//Debug.Log ("i : " + i + " j : " + j);
				HasBonusArray[i,j] = true;
			}
		}

		MostKills = WhichPlayer.Neither;
		MostBonuses = WhichPlayer.Neither;
		LeastDamageTaken = WhichPlayer.Neither;
		MostAssists = WhichPlayer.Neither;
		MostDamageDealt = WhichPlayer.Neither;
		MostOPPlayer = WhichPlayer.Neither;

		P1DamageDealt = 0f;
		P2DamageDealt = 0f;
		P1DamageTaken = 0f;
		P2DamageTaken = 0f;
		P1Kills = 0;
		P2Kills = 0;
		P1Deaths = 0;
		P2Deaths = 0;
		P1MOPVotes = 0;
		P2MOPVotes = 0;
	}

	//called by reference
	public void CalcStatsAndAwards()
	{
		EvaluateMostKills();
		//EvaluateMostAssists();
		EvaluateMostBonuses();
		EvaluateMostDamageDealt();
		EvaluateLeastDamageTaken();
		EvaluateMostOP();
	}

	public void PlayerLosesBonus(BonusLostMessage m)
	{
		int playerIndicatorIndex; //0 = player1, 1 = player2
		Debug.Log ("Bonus " + m.theBonusThatWasLost + " will be lost for player " + m.whichCharacter);
		if (PlayerData.amIPlayer1(m.whichCharacter))
		{
			playerIndicatorIndex = 0;
		}
		else
		{
			playerIndicatorIndex = 1;
		}
		if (m.theBonusThatWasLost == WhichBonus.NoJump)
		{
			//HasNoJumpBonus [playerIndicatorIndex] = false;
			HasBonusArray[0,playerIndicatorIndex] = false;
		}
		else if (m.theBonusThatWasLost == WhichBonus.NoDash)
		{
			//HasNoDashBonus [playerIndicatorIndex] = false;
			HasBonusArray[1,playerIndicatorIndex] = false;
		}
		else if (m.theBonusThatWasLost == WhichBonus.NoRange)
		{
			//HasNoRangeBonus [playerIndicatorIndex] = false;			
			HasBonusArray[2,playerIndicatorIndex] = false;

		}
		else if (m.theBonusThatWasLost == WhichBonus.NoMelee)
		{
			//HasNoMeleeBonus [playerIndicatorIndex] = false;
			HasBonusArray[3,playerIndicatorIndex] = false;
		}
		else if (m.theBonusThatWasLost == WhichBonus.NoDeath)
		{
			//HasNoDeathBonus [playerIndicatorIndex] = false;
			HasBonusArray[4,playerIndicatorIndex] = false;

		}
		else if (m.theBonusThatWasLost == WhichBonus.NoOP)
		{
			//HasNoOPBonus [playerIndicatorIndex] = false;
			HasBonusArray[5,playerIndicatorIndex] = false;
		}
	}

	public void UpdateDamageMetaData(EntityTookDamageMessage m)
	{ 
		//if the damage source is a player, update that players damage stats
		//if the damage recipient is a player, update that players damage taken stats
		Debug.Log("Entity took damage " + m.DamageTaken + " attacker: " + m.AttackingEntityType + " character " + m.AttackingCharacter +" defender: " + m.ReceivingEntityType + " character " + m.ReceivingCharacter);
		if (m.AttackingEntityType.Equals(EntityType.Player))
		{
			if (PlayerData.amIPlayer1(m.AttackingCharacter))
			{
				P1DamageDealt += m.DamageTaken;
			}
			else
			{
				P2DamageDealt += m.DamageTaken;
			}
		}
		if (m.ReceivingEntityType.Equals(EntityType.Player))
		{
			if (PlayerData.amIPlayer1(m.ReceivingCharacter))
			{
				P1DamageTaken += m.DamageTaken;
			}
			else
			{
				P2DamageTaken += m.DamageTaken;
			}		
		}
	}

	public void UpdateKillsMetaData(EntityDiedMessage m)
	{
		Debug.Log ("Kill Metadata grabbed for attacking entity type " + m.AttackingEntityType + " character " + m.AttackingCharacter);
		if (m.AttackingEntityType.Equals (EntityType.Player)) {
			if (m.ReceivingEntityType.Equals(EntityType.Player)) { return;}
			if (PlayerData.amIPlayer1 (m.AttackingCharacter)) {
				P1Kills++;
			} else {
				P2Kills++;
			}
		}
	}

	public void EvaluateMostKills()
	{
		MostKills = EvaluateAwardBasedOnHigherNumber(P1Kills, P2Kills);
	}

	/*public void EvaluateMostAssists()
	{
		MostAssists = WhichPlayer.Both; //TODO figure out how to calc this
	}*/

	public void EvaluateMostBonuses()
	{
		//loop through all bonuses and tally how much each player still has, then compare those values to see who has the most bonuses
		int numOfBonuses1 = 0;
		int numOfBonuses2 = 0;
		for(int op = 0; op < HasBonusArray.GetLength(0); op++)
		{
			//Debug.Log ("index value " + op);
			if (HasBonusArray[op,0])
			{
				numOfBonuses1++;
			}
			if (HasBonusArray[op,1])
			{
				numOfBonuses2++;
			}
		}
		MostBonuses = EvaluateAwardBasedOnHigherNumber(numOfBonuses1,numOfBonuses2);
	}

	public void EvaluateLeastDamageTaken()
	{
		//inverse the arguments on called method as a way of determining who took the least damage
		LeastDamageTaken = EvaluateAwardBasedOnHigherNumber ((int)P2DamageTaken, (int)P1DamageTaken);
	}

	public void EvaluateMostDamageDealt()
	{
		MostDamageDealt = EvaluateAwardBasedOnHigherNumber ((int)P1DamageDealt, (int)P2DamageDealt);
	}

	public void EvaluateMostOP()
	{
		if (P1MOPVotes > P2MOPVotes)
		{
			MostOPPlayer = WhichPlayer.Player1;
		}
		else if (P1MOPVotes > P2MOPVotes)
		{
			MostOPPlayer = WhichPlayer.Player2;
		}
		else
		{
			MostOPPlayer = WhichPlayer.Both;
		}
	}

	private WhichPlayer EvaluateAwardBasedOnHigherNumber(int p1Num, int p2Num)
	{
		if (p1Num > p2Num)
		{
			P1MOPVotes++;
			return WhichPlayer.Player1;
		}
		else if (p2Num > p1Num)
		{
			P2MOPVotes++;
			return WhichPlayer.Player2;
		}
		else
		{
			return WhichPlayer.Both;
		}
	}
}
