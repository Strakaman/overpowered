﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StageResultsScreen : OmonoPehaviour {

	public StageBonusScoring StageScoringTracking;
	bool StageOver = false;
		public Transform ResultsScreenGroup;
		protected int pageNum;
		protected int maxPageNum = 3;
		public Transform StandardInfoGroup;
		public Transform StandardBonusGroup;
		public Transform StandardAwardsGroup;

		//standard info section
		public Text P1KillsLabel;
		public Text P1DamageLabel;
		public Text P1DeathsLabel;
		public Text P1DamageTakenLabel;
		public Text P1Stat2Label;	
		public Text P2KillsLabel;
		public Text P2DamageLabel;
		public Text P2DeathsLabel;
		public Text P2DamageTakenLabel;
		public Text P2Stat2Label;

		//bonuses section
		public Transform p1NoJumpCheckMark;
		public Transform p1NoDashCheckMark;
		public Transform p1NoRangeCheckMark;
		public Transform p1NoMeleeCheckMark;
		public Transform p1NoDeathCheckMark;
		public Transform p1NoOPCheckMark;
		public Transform p2NoJumpCheckMark;
		public Transform p2NoDashCheckMark;
		public Transform p2NoRangeCheckMark;
		public Transform p2NoMeleeCheckMark;
		public Transform p2NoDeathCheckMark;
		public Transform p2NoOPCheckMark;

		//Awards Section
		public Text MostKillsAwardRecepient;
		public Text MostBonusesAwardRecepient;
		//public Text MostAssistsAwardRecepient;
		public Text MostDamageDealtAwardRecepient;
		public Text LeastDamageAwardRecepient;
		public Text MostOPPlayerAwardRecepient;

		void GrabStatsDataToDisplay()
		{
			Debug.Log ("Grabbing Stage Stats from Stage Bonus Scoring and setting display values accordingly");
			StageScoringTracking.CalcStatsAndAwards ();
			P1KillsLabel.text = "" + StageBonusScoring.P1Kills;
			P1DamageLabel.text = "" + (int)StageBonusScoring.P1DamageDealt;
			P1DamageTakenLabel.text ="" +  (int)StageBonusScoring.P1DamageTaken;
			P1DeathsLabel.text = "" + StageBonusScoring.P1Deaths;
			P2KillsLabel.text = "" + StageBonusScoring.P2Kills;
			P2DamageLabel.text = "" + (int)StageBonusScoring.P2DamageDealt;
			P2DamageTakenLabel.text ="" +  (int)StageBonusScoring.P2DamageTaken;
			P2DeathsLabel.text = "" + StageBonusScoring.P2Deaths;
			p1NoJumpCheckMark.gameObject.SetActive(StageBonusScoring.HasBonusArray[0,0]);
			p1NoDashCheckMark.gameObject.SetActive(StageBonusScoring.HasBonusArray[1,0]);
			p1NoRangeCheckMark.gameObject.SetActive(StageBonusScoring.HasBonusArray[2,0]);
			p1NoMeleeCheckMark.gameObject.SetActive(StageBonusScoring.HasBonusArray[3,0]);
			p1NoDeathCheckMark.gameObject.SetActive(StageBonusScoring.HasBonusArray[4,0]);
			p1NoOPCheckMark.gameObject.SetActive(StageBonusScoring.HasBonusArray[5,0]);
			p2NoJumpCheckMark.gameObject.SetActive(StageBonusScoring.HasBonusArray[0,1]);
			p2NoDashCheckMark.gameObject.SetActive(StageBonusScoring.HasBonusArray[1,1]);
			p2NoRangeCheckMark.gameObject.SetActive(StageBonusScoring.HasBonusArray[2,1]);
			p2NoMeleeCheckMark.gameObject.SetActive(StageBonusScoring.HasBonusArray[3,1]);
			p2NoDeathCheckMark.gameObject.SetActive(StageBonusScoring.HasBonusArray[4,1]);
			p2NoOPCheckMark.gameObject.SetActive(StageBonusScoring.HasBonusArray[5,1]);
			MostKillsAwardRecepient.text = TranslatePlayerToString(StageBonusScoring.MostKills);
			MostBonusesAwardRecepient.text = TranslatePlayerToString(StageBonusScoring.MostBonuses);
			LeastDamageAwardRecepient.text = TranslatePlayerToString(StageBonusScoring.LeastDamageTaken);
			//MostAssistsAwardRecepient.text = TranslatePlayerToString(StageBonusScoring.MostAssists);
			MostDamageDealtAwardRecepient.text = TranslatePlayerToString(StageBonusScoring.MostDamageDealt);
			MostOPPlayerAwardRecepient.text = TranslatePlayerToString(StageBonusScoring.MostOPPlayer);
		}

		string TranslatePlayerToString(WhichPlayer thePlaya)
		{
			if (thePlaya == WhichPlayer.Player1)
			{
				return "P1";
			}
			else if (thePlaya == WhichPlayer.Player2)
			{
				return "P2";
			}
			else
			{
				return "P1/P2";
			}
		}

		protected override void OnStart()
		{
			pageNum = 1;
			Utilities.RegisterListener(new Listener(OmonoPehaviour.ms_STAGEENDED,gameObject,"StageHasEnds"));

		}

		void Update()
		{/*
		if (Input.GetKeyDown(KeyCode.B))
		{
			for (int i=0; i < StageBonusScoring.HasBonusArray.GetLength(0); i++)
			{
				for (int j=0; j < 2; j++)
				{
					Debug.Log ("i : " + i + " j : " + j +" value: " + StageBonusScoring.HasBonusArray[i,j]);
				}
			}
		}*/
		if (!StageOver) {
			return;
		}
		if (Input.GetButtonDown (MenuControls.Confirm1))
			{
				pageNum++;
				ChangePageDisplay();
			}
		if (Input.GetButtonDown (MenuControls.Back1))
			{
				pageNum--;
				ChangePageDisplay();
			}
	
		}

	void StageHasEnds(Message m)
	{
		StageOver = true;
		GrabStatsDataToDisplay ();
		ResultsScreenGroup.gameObject.SetActive (true);
		ChangePageDisplay ();
	}
		void ChangePageDisplay()
		{
			StandardInfoGroup.gameObject.SetActive(false);
			StandardBonusGroup.gameObject.SetActive(false);
			StandardAwardsGroup.gameObject.SetActive(false);
			if (pageNum < 1)
			{
				pageNum = 3;
			}
			else if (pageNum > 7)
			{
				GoBackToStageSelect();
			}
			if (pageNum ==1)
			{
				StandardInfoGroup.gameObject.SetActive(true);
			}
			else if (pageNum == 2)
			{
				StandardBonusGroup.gameObject.SetActive (true);
			}
			else if (pageNum >= 3)
			{
				StandardAwardsGroup.gameObject.SetActive(true);
			}
		}

		void GoBackToStageSelect()
		{
			Utilities.LoadLevel("StageSelect");
		}
}
