﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StageSelector : OmonoPehaviour
{
	public DPadButtons dPadScript1;
	public DPadButtons dPadScript2;
	public Texture2D background; //background to draw behind everything
	public Texture2D selectionBoxIcon; //box hovers over current selection
	int selectionIndex; //represents current stage selection
	private bool drawBox; //toggle whether or not to draw the box for special effect
	private int stageListCount; //keep track of how many stages were loaded from the config file
	public GUIStyle numbersDisplay; //for font,size, and color
	public Text StageDataTextArea;

	// Use this for initialization
	protected override void OnStart () {
		drawBox = true;
		if (Utilities.inDebugMode ()) { //Stage data should already be loaded if coming from the main menu, but not necessarily when loaded from scene
			if (SessionData.listOfStages == null) {
				SessionData.LoadStageConfigData (); //can call this from any point to load the data again from file
			}
		}
		dPadScript1.SetAxisControls (MenuControls.LR1, MenuControls.UD1);
		dPadScript2.SetAxisControls (MenuControls.LR2, MenuControls.UD2);
        UpdateStageInfo();
        stageListCount = SessionData.listOfStages.Count; //cache value locally
	}
	
	
	
	// Update is called once per frame
	void Update ()
	{
		//if (Input.GetButtonDown ("Right1")) { //moves right if not at end of array
		if (dPadScript1.RightPressed() || dPadScript2.RightPressed()) {
			if (selectionIndex < stageListCount - 1) {
				selectionIndex++;
				UpdateStageInfo ();
			}
		}
		
		
		if (dPadScript1.LeftPressed() || dPadScript2.LeftPressed()) { //moves left if not at beginning of array
			if (selectionIndex > 0) {
				selectionIndex--;
				UpdateStageInfo ();
			}
		}
		
		
		if (Input.GetButtonDown(MenuControls.Confirm1) || Input.GetButtonDown(MenuControls.Confirm2)) //confirm choice if currently highlighting an unlocked stage
		{
			if (SessionData.listOfStages [selectionIndex].isUnlocked)
			{
				SessionData.currentStageIndex = selectionIndex; //current stage index will later be used by the Arena to figure out the paramaters for that stage
				GoToArena();
			}
		}
	}
	
	void UpdateStageInfo()
	{
		Stage currStageHighlighted = SessionData.listOfStages [selectionIndex]; //get the stage data for the currently highlighted stage
		StageDataTextArea.text = "Stage: " + currStageHighlighted.stageName + "\nUnlocked?: " + currStageHighlighted.isUnlocked + "\nNumber of Rooms: " + currStageHighlighted.listOfRooms.Length +"\nAverage Room Rating: " + currStageHighlighted.GetAverageRoomRating();
	}
	
	void OnGUI ()
	{
		//GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), background); //draw BG
		//GUI.Label (new Rect (Screen.width / 2, Screen.height / 40, Screen.width / 4, 50), "Stage Select"); //header text directing player
		/*for (int i = 0; i < stageListCount; i++)
			{
				GUI.Label(new Rect(i*150,Screen.height/5,100,100),""+i,numbersDisplay);
			}*/
		/*
				So the logic here is to have the current highlighted stage be centered on the screen,
				have the preceding stages be drawn to the left of the center
				and have the following stages be drawn to the right of the center
				To do this, we use two for loops
				Extra number added to stage number display #arraylogic
			*/
		for (int i = 0; i < selectionIndex; i++) {
			//for loop condition makes selectionIndex always greater than counter, so all textures here will be drawn left of the center
			GUI.Label (new Rect (Screen.width / 2 - ((selectionIndex - i) * 150) - 50, Screen.height / 3 - 37.5f, 100, 100), "" + (i+1), numbersDisplay);
		}
		
		
		for (int i = selectionIndex; i < stageListCount; i++) {
			//selectionIndex = counter, so texture is drawn exactly at the center
			//rest of for loop condition makes selectionIndex always less than counter, so rest of textures will be drawn right of the center
			GUI.Label (new Rect (Screen.width / 2 + ((i - selectionIndex) * 150) - 50, Screen.height / 3 - 37.5f, 100, 100), "" + (i+1), numbersDisplay);
		}
		
		
		if (drawBox) {
			GUI.DrawTexture (new Rect (Screen.width / 2 - 75, Screen.height / 3 - 75, 150, 150), selectionBoxIcon); //box should be cocentric with numbers
		}
		
		
		//Stage lol = SessionData.listOfStages [selectionIndex]; //get the stage data for the currently highlighted stage
		//GUI.Label (new Rect (Screen.width / 2, 9 * Screen.height / 10, 200, 50), lol.stageName + ": " + lol.isUnlocked); //display that stage data
	}
	
	
	
	public void GoToArena()
	{
		Utilities.LoadLevel("Arena");
	}
}