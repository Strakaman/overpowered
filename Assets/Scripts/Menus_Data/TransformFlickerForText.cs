﻿using UnityEngine;
using System.Collections;

public class TransformFlickerForText : MonoBehaviour {

	public	Transform[] TextsToFlicker;
	public float FlickerTimeInterval=1.5f;
	private bool TextActive = true;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnEnable()
	{
		StartCoroutine ("ChangeState", 5f);

	}

	void OnDisable()
	{
		StopAllCoroutines ();
	}
	IEnumerator ChangeState()
	{
		while (true) {
			TextActive = !TextActive;
			foreach (Transform t in TextsToFlicker) {
				t.gameObject.SetActive(TextActive);
			}
			yield return new WaitForSeconds (FlickerTimeInterval); //wait a tenth of a second between calls
		}
	}
}
