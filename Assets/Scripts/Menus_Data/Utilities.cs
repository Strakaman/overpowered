﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;

public static class Utilities
{
		private static bool DebugMode = true;
		public static GameObject EffectPlayer;
		public static MessageManager Messenger;
		public static int LastLoadedLevelB4DebugMenu = 9;
		private static GameObject oInstance;
		private static GameObject pInstance;

		public static float xjumpOverVec = Mathf.Cos(Mathf.Deg2Rad * 80);
		public static float yJumpOverVec = -Mathf.Sin(Mathf.Deg2Rad * 80);
		public static float HorMaxSpeed = 8;
		public static float Accel = 40;
		public static float JumpSpeed = 26;
		public static float DiveSpeed = -5;
		public static float gravityScale = 6;
		public static int ObstOnlyLMask = 1 << 16;
		public static int PlyrOnlyLMask = 1 << 9;
		public static int EnemOnlyLMask = 1 << 10;
		public static int EntiOnlyLMask = 11 << 9;
		public static int WallOnlyLMask = 1 << 17;
		public static int ObstWallMask = ((1 << 17) | (1 << 16));
		public static Vector3 DefaultTargetLocation = new Vector3 (0, -9, 0);
		public static Vector3 Player1StartPosition = new Vector3 (-6.75f, 9.05f, 0);
		public static Vector3 Player2StartPosition = new Vector3 (6.75f, 9.05f, 0);

		public static bool inDebugMode()
		{
				return DebugMode;
		}

		/*public static void saveGame ()
	{
		//Debug.Log (Application.dataPath);
		try {
			String savePath = Application.dataPath + "/test.blanksav";
			StreamWriter saver = new StreamWriter (savePath, false);
			saver.WriteLine ("Save data stores player's last scene, list of unlocked spells, and last position");
			saver.WriteLine (Application.loadedLevelName);
			String delimiter = "";
			foreach (Spell s in SpellBook.playerSpells) {
				saver.Write (delimiter + s.isSpellUnlocked ());
				delimiter = "|";
			} 	
			saver.Write ("\n");
			GameObject playaPlaya = GameObject.FindGameObjectWithTag ("Player");
			Vector3 playaPos = playaPlaya.transform.position;
			saver.WriteLine (playaPos.x + delimiter + playaPos.y + delimiter + playaPos.z);
			Debug.Log ("Game Saved (sorta) to " + savePath + ".");
			saver.Close ();
		} catch (Exception e) {
			//TODO: handle exception
			Debug.Log ("Error writing save file: " + e.Message);
		}
	}*/
		/*
		public static void loadGame ()
		{
				try {
						String savePath = Application.dataPath + "/test.blanksav";
						StreamReader loader = new StreamReader (savePath, false);
						loader.ReadLine ();
						String currLevelName = loader.ReadLine ();
						String[] unlockBools = loader.ReadLine ().Split ('|');
						int length = unlockBools.Length;
						int i;
						bool testBool;
						for (i = 0; i < length; i++) {
								Boolean.TryParse (unlockBools [i], out testBool);				
								if (testBool) {
										SpellBook.playerSpells [i].unlockSpell ();
								}
						}
						String[] coordinates = loader.ReadLine ().Split ('|');
						float x, y, z;
						float.TryParse (coordinates [0], out x);
						float.TryParse (coordinates [1], out y);
						float.TryParse (coordinates [2], out z);
						//Vector3 playaPos = new Vector3(x,y,z);
						loader.Close(); 
						Application.LoadLevel(currLevelName);
						//TODO: move plaver to position of loading
				} catch (Exception e) {
						//TODO: handle exception
						Debug.Log ("Error reading save file: " + e.Message);
				}
		}*/

		/**
		* Used to instantiate any object at the passed in position
		* Pass in a velocity vector if the object should move
				* Pass in a orientation vector so that the sprite can be rotated accordingly
				*/ 
				public static void cloneObject (GameObject o, Vector3 placetoCreate, Vector3 velocity, Quaternion orientation)
		{		
				//GameObject clonedesu = (GameObject)ScriptableObject.Instantiate (bulletToClone, placetoCreate, orientation);
				o.transform.position = placetoCreate;
				o.GetComponent<Rigidbody2D>().velocity = velocity;
				o.transform.rotation = orientation;
				//Debug.Log("poops: " + clonedesu);
				if (o.GetComponent<Rigidbody2D>()) {
						o.GetComponent<Rigidbody2D>().velocity = velocity;
						//Debug.Log ("poop da doops");
				}
				if(o.GetComponent<AudioSource>())
				{
						o.GetComponent<AudioSource>().Play();
				}
		}
				
		/*
	 * Checks passed in Game object to see if any of it's tags are of the requested value
	 */ 
		public static bool hasMatchingTag(string tagToCheckFor, GameObject objectToCheck)
		{
				MultiTagScript mult =  objectToCheck.GetComponent<MultiTagScript>();
				if (mult != null) { return mult.objectHasTag(tagToCheckFor);}
				return false;
		}

		public static void SetMessenger(MessageManager theMessenger)
		{
				Messenger = theMessenger;
		}

		public static void RegisterListener(Listener listenerToRegister)
		{
				if (Messenger == null) {
						//this should not be null unless the scene is changing and messenger has already been destroyed
						Messenger = GameObject.Find("World").GetComponent<MessageManager>();
				}
				if (Messenger != null) //null checking in case messenger has been destroyed due to change in scene
				{
						Messenger.RegisterListener(listenerToRegister);
				}
		}

		public static void SendToListeners(Message messageToSend)
		{
				if (Messenger == null) { 
						//this should not be null unless the scene is changing and messenger has already been destroyed
						GameObject messObj  = GameObject.Find("World");
						if (messObj != null)
						{
								Messenger = messObj.GetComponent<MessageManager>();
						}
				}
				if (Messenger != null) //null checking in case messenger has been destroyed due to change in scene
				{
						Messenger.SendToListeners(messageToSend);
				}
		}

		public static void playSound(SoundType typeofSoundToPlay)
		{
				if (EffectPlayer == null) { 
						EffectPlayer = GameObject.FindGameObjectWithTag("SoundEffectPlayer");
				}
				EffectPlayer.BroadcastMessage("playSoundEffect",typeofSoundToPlay);
		}

		public static void rotateObject(OPDirection direction, GameObject obj) {
				if (direction == OPDirection.up) {
						obj.transform.Rotate(new Vector3(0, 0, 90));
				}
				if (direction == OPDirection.down) {
						obj.transform.Rotate(new Vector3(0, 0, 270));
				}
				if (direction == OPDirection.right) {
						obj.transform.Rotate(new Vector3(0, 0, 0));
				}
				if (direction == OPDirection.left) {
						obj.transform.Rotate(new Vector3(0, 0, 180));
				}
		}

		public static void SetDirectCharacterInstance(GameObject charInstance, WhichCharacter wCeeDesu)
		{
				if (wCeeDesu.Equals(WhichCharacter.O))
						{
								oInstance = charInstance;
						}
				else if (wCeeDesu.Equals(WhichCharacter.P))
								{
										pInstance = charInstance;
								}
		}
				

		public static GameObject GetOInstance()
		{
				return oInstance;
		}

		public static GameObject GetPInstance()
		{
				return pInstance;
		}

	public static void LoadLevel(string SceneNameToChangeTo)
	{
		Utilities.SendToListeners(new Message(null, OmonoPehaviour.ms_CHANGINGSCENE));
		if (SceneNameToChangeTo.Equals("")) return; //thus get the broadcast but don't load the scene, yes i know this is bad
		Application.LoadLevel (SceneNameToChangeTo);

	}

	public static void LoadLevel(int SceneNumToChangeTo)
	{
		Utilities.SendToListeners(new Message(null, OmonoPehaviour.ms_CHANGINGSCENE));
		Application.LoadLevel (SceneNumToChangeTo);

	}
}

