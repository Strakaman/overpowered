﻿//enum mostly used for direction the player is facing probably can use same enum type for other objects as well
public enum OPDirection { down = 0, up = 1, left = 2, right = 3 };