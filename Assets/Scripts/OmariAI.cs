﻿using UnityEngine;
using System.Collections;
using Pathfinding;

[RequireComponent (typeof(Rigidbody2D))]
public class OmariAI : OmonoPehaviour
{
		EntityStateManager eManager;
		Animator animator;
		PlayerAction pAction;
		GroundChecker myGroundChecker;
		Dash dashScript;
		WallJump eWallJumper;
		int howManyJumps = 3;
		// What to chase?
		public Transform target;

		public bool HasJumpIntel;
		public bool HasGroundDashIntel;
		public bool HasAirDashIntel;
		public bool HasDropIntel;
		public bool HasJumpOverIntel;

		public AIAttack[] AIAttackOptions;
		private bool inNoAttackCooldown;
		private bool inNoMovementCooldown;

		//bool testonce = true;

		//for raycasting
		private Vector3 s;
		//box collider size to help with raycasting
		private Vector3 c;
		//box collider center to help with raycasting

		private float myLocalScale;
		//private float xjumpOverVec = Mathf.Cos(Mathf.Deg2Rad * 80);
		//private float yJumpOverVec = -Mathf.Sin(Mathf.Deg2Rad * 80);
		private float xDistance; //displacement from target x
		private float yDistance; //displacement from target y
		private Vector2 p; //curr position

		protected override void OnStart ()
		{
				myLocalScale = myLocalScale = Mathf.Abs (transform.localScale.x);
				eManager = GetComponent<EntityStateManager> ();
				animator = GetComponent<Animator> ();
				pAction = GetComponent<PlayerAction> ();
				dashScript = GetComponent<Dash> ();
				myGroundChecker = GetComponent<GroundChecker> ();
				eWallJumper = GetComponent<WallJump> ();
				BoxCollider2D zollider = GetComponent<BoxCollider2D> ();
				s = zollider.size;
				c = zollider.offset;
				if (target == null) {
						Debug.LogError ("No Player found? PANIC!");
						return;
				}
		}

		void Update ()
		{
				UpdateAnimation ();
		}

		void UpdateAnimation ()
		{
				if (GetComponent<Rigidbody2D>().velocity.x > 0) {
						transform.localScale = new Vector3 (myLocalScale, transform.localScale.y, transform.localScale.z);
						eManager.SetOPDirectionState (OPDirection.right);
						animator.SetBool ("walkRight", true);
				} else if (GetComponent<Rigidbody2D>().velocity.x < 0) {
						transform.localScale = new Vector3 (-myLocalScale, transform.localScale.y, transform.localScale.z);
						eManager.SetOPDirectionState (OPDirection.left);
						animator.SetBool ("walkRight", true);
				}
		}

		void UpdateDistancesAndLocations()
		{
				xDistance = target.transform.position.x - transform.position.x;
				yDistance = target.transform.position.y - transform.position.y;
				p = transform.position;
		}

		void FixedUpdate ()
		{
				if (!inNoAttackCooldown) {
						AttackChecks ();
				}
				if (!inNoMovementCooldown) {
						MovementChecks ();
				}
		}

		void AttackChecks()
		{
				foreach (AIAttack currAttack in AIAttackOptions)
				{
						if (currAttack.CanAttack(eManager.GetDirState()))
						{
								if (currAttack.noAttackPenaltyTime > 0)
								{
										inNoAttackCooldown = true;
										Invoke("NoAttackCooldownOver",currAttack.noAttackPenaltyTime);
								}
								if (currAttack.noMovementPenaltyTime > 0)
								{
										inNoMovementCooldown = true;
										Invoke("NoMovementCooldownOver",currAttack.noMovementPenaltyTime);
								}
								currAttack.DoAttack(eManager.GetDirState());
								break; //found the attack we wanna use so lets use it!
						}
				}
		}

		void MovementChecks()
		{
				/*if (testonce)
					{
						testonce = false;
						Invoke("WallJump",2);
					}*/
				//increaseHTransform ();
				//Debug.Log (FacingEndOFLedge());
				UpdateDistancesAndLocations ();
				if (eManager.Equals (EState.normal)) {
						if (xDistance > 0) {
								increaseHTransform ();
						} else if (xDistance < 0) {
								decreaseHTransform ();
						}
				}
				if (ShouldJump ()) {
						pAction.JumpInput ();
				}
				if (ShouldDropThru ()) {
						myGroundChecker.DropThrough ();
				}
				if (ShouldDash ()) {
						pAction.DashInput ();
				}
				if (FacingEndOFLedge () && yDistance > 0) {
						pAction.JumpInput ();
				}
		}

		private void NoAttackCooldownOver()
		{
				inNoAttackCooldown = false;
		}

		private void NoMovementCooldownOver()
		{
				inNoMovementCooldown = false;
		}

		bool ShouldJump ()
		{
				/* -If AIHasJmpIntel == true
				* -if O/P is above target and O/P is more than 1/5 of vertical space above target
				* -if upward raycast of jump length hits JPlat
				* -return true
				*/
				if (HasJumpIntel) {
						//float distance = target.transform.position.y - transform.position.y; //get distance between you and target
						//Debug.DrawRay(new Vector3(x,y,transform.position.z),new Vector3(0,1,0));
						if ((yDistance > 0) && (Mathf.Abs (yDistance) > 4)) {
								//Vector2 p = transform.position; //current position
								float x = p.x + c.x; //middle
								float y = (p.y + c.y + s.y / 2); // top
								//draw ray cast at head of object going upwards to see if there is a jump through platform above them
								RaycastHit2D hit = Physics2D.Raycast (new Vector2 (x, y), new Vector2 (0, 1), 4f, Utilities.ObstOnlyLMask);
								if ((hit.collider) && (hit.collider.gameObject) && Utilities.hasMatchingTag ("DropThru", hit.collider.gameObject)) {
										return true;
								}
						}
				}

				return false;
		}

		bool ShouldDash ()
		{
				if (HasGroundDashIntel || HasAirDashIntel) {
						//float distance = target.transform.position.x - transform.position.x;
						if ((xDistance < 0) && (eManager.Equals (OPDirection.right))) {
								return false; //return false if facing right and enemy is left of you
						}
						if ((xDistance > 0) && (eManager.Equals (OPDirection.left))) {
								return false;
						}
						if ((Mathf.Abs (xDistance) > 12)) {
								if (dashScript.CanDash ()) {
										if (myGroundChecker.isGrounded ()) {
												if (HasGroundDashIntel) {
														return true;
												}
										} else {
												if (HasAirDashIntel && GetComponent<Rigidbody2D>().velocity.y < -.5) {
														//should only air dash if you are falling not jumping, otherwise you could cut your jump short and miss the platform you were aiming for
														return true; 
												}
										}
								}
						}
				}
				return false;
		}

		bool ShouldDropThru ()
		{
				if (HasDropIntel) {
						if (myGroundChecker.isGrounded ()) {
								//float distance = target.transform.position.y - transform.position.y;
								if ((yDistance < 0) && (Mathf.Abs (yDistance) > 4)) {
										if (myGroundChecker.CanDropThrough ()) {
												return true;
										}
								}
						}
				}

				return false;
		}

		bool FacingEndOFLedge ()
		{
				if (HasJumpOverIntel) {
						if (!myGroundChecker.isGrounded ()) {
								return false;
						}

						int xDir;
						if (eManager.Equals (OPDirection.right)) {
								xDir = 1;
						} else {
								xDir = -1;
						}

						//Vector2 p = transform.position; //current position
						float x = p.x + c.x + (myLocalScale * s.x / 2); //right
						float y = (p.y + c.y + (myLocalScale * s.y / 2)); // top
						//Debug.DrawRay(new Vector2(x,y),new Vector2(xDir * Utilities.xjumpOverVec,Utilities.yJumpOverVec));
						RaycastHit2D hit = Physics2D.Raycast (new Vector2 (x, y), new Vector2 (xDir * Utilities.xjumpOverVec, Utilities.yJumpOverVec), 2f, Utilities.ObstOnlyLMask);
						if ((hit.collider) && (hit.collider.gameObject)) {
								return false;
						}
						return true;
				} else {
						return false;
				}
		}

		void decreaseHTransform ()
		{
				GetComponent<Rigidbody2D>().velocity = new Vector3 (-Utilities.HorMaxSpeed, GetComponent<Rigidbody2D>().velocity.y);
		}

		void increaseHTransform ()
		{
				GetComponent<Rigidbody2D>().velocity = new Vector3 (Utilities.HorMaxSpeed, GetComponent<Rigidbody2D>().velocity.y);
		}

		void WallJump ()
		{

				if (myGroundChecker.isGrounded ()) {
						pAction.JumpInput ();
						Invoke ("WallJump", .5f);
				} else {
						StartCoroutine ("WallJumpCycle", howManyJumps);
				}
		}

		IEnumerator WallJumpCycle (int numOfTimes)
		{
				while (numOfTimes > 0) { //basically how many wall jumps to perform in series
						numOfTimes--;
						int direction; //pAction wall jump code from here
						if (eWallJumper.CanWallJump (out direction)) {
								dashScript.InterruptDash ();
								dashScript.SetDashRestricted (false);
								eWallJumper.PerformWallJump (direction); //to here
								if (numOfTimes > 0) { //every jump other than the last jump, AI should go towards the wall
										float translationDuration = .3f; //delay in between wall jump attempts
										while (translationDuration > 0) { //loop to make sure AI keeps heading towards the wall
												if (direction < 0) { //less than 0 means u just wall jumped to the left so for multiple jumps you should head back right
														increaseHTransform ();
												} else { //should be heading left for next jump
														decreaseHTransform ();
												}
												yield return new WaitForSeconds (.1f); //wait a tenth of a second between calls
												translationDuration -= .1f;
										}
								}
						}
				}
		}

}
