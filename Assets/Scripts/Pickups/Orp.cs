﻿using UnityEngine;
using System.Collections;

public class Orp : Pickupable {

	int amountGiven = 5;
	// Use this for initialization
	protected override void OnStart () {
		Invoke("DestroySelf",10);
	}
	
	// Update is called once per frame
	void Update () {

	}

	void DestroySelf()
	{
		Destroy(gameObject);
	}

	public override void OnCollisionEnter2D(Collision2D whatICollidedWith)
	{
		if (Utilities.hasMatchingTag("Player",whatICollidedWith.gameObject))
		{
			whatICollidedWith.gameObject.BroadcastMessage("addOPP",amountGiven);
			Destroy(gameObject); 
		}
	}
}
