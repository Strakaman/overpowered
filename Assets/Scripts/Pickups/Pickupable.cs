﻿using UnityEngine;
using System.Collections;

public abstract class Pickupable : OmonoPehaviour {
	
	protected SoundType mySound = SoundType.pickup;
	public abstract void OnCollisionEnter2D(Collision2D whatICollidedWith);
}
