using UnityEngine;
using System.Collections;

public class WeaponPickup : Pickupable {

	public ProjType WeaponSupplied; //allows for all weapon pickups to use the same script
	protected override void OnStart () {
		Invoke("DestroySelf",10);
	}

	void Update () {
	
	}

	void DestroySelf()
	{
		Destroy(gameObject);
	}

	/*
	 * Whenever pickup collides with player, broadcast a message to that player
	 * telling them to change their equipment, using the Enum set in the inspector
	 */ 
	public override void OnCollisionEnter2D(Collision2D whatICollidedWith)
	{
		if (Utilities.hasMatchingTag("Player",whatICollidedWith.gameObject))
		{
			whatICollidedWith.gameObject.BroadcastMessage("ChangeEquip",WeaponSupplied);
			Destroy(gameObject); 
		}
	}

}
