﻿using UnityEngine;
using System.Collections;

public class PlayerAction : OmonoPehaviour {

	//public int movForceMag;
	private const int MAXSPEED=8;
	private const float ACCEL = 40;
	private const float JUMPSPEED = 26f;
	Animator animator;
	public GroundChecker pGroundChecker;
	public Dash dashScript;
	public WallJump pWallJumper;
	public EntityStateManager eManager;
	public EquipmentHandler equipManager;
	public GameObject pRangePrefab; //Projectile prefab
	public OverPower pOverPower;
	private GameObject instanceOfRangePrefab; //used to create an instance of the prefab in order to send message to the script. 
	private float currTime;
	public float cooldown = 0.5f;
	private float myLocalScale;
	private Rigidbody2D myRigidBody;
	public WhichCharacter whoAmI;

	bool BroadcastNoDashBonusLostMessage = true;
	bool BroadcastNoJumpBonusLostMessage = true;
	bool BroadcastNoRangeBonusLostMessage = true;
	bool BroadcastNoMeleeBonusLostMessage = true;
	bool BroadcastNoOPBonusLostMessage = true;


	// Use this for initialization
	protected override void OnStart () {
		myLocalScale = myLocalScale = Mathf.Abs (transform.localScale.x);
		animator = (Animator)GetComponent ("Animator");
		eManager.SetEntityState(EState.normal);
		if (transform.localScale.Equals (new Vector3(myLocalScale,transform.localScale.y,transform.localScale.z))) { //sets default direction of sprite
			eManager.SetOPDirectionState (OPDirection.right);
		}
		if (transform.localScale.Equals (new Vector3(-myLocalScale,transform.localScale.y,transform.localScale.z))) { //sets default direction of sprite
			eManager.SetOPDirectionState (OPDirection.left);
		}
		myRigidBody = GetComponent<Rigidbody2D> ();
	}

	// Update is called once per frame
	void Update () {
		//Debug.Log (eManager.GetAttackState ());
	}

	public void HorizontalInput(float h)
	{
		//float h = Input.GetAxis ("HorizontalO");
		//rigidbody2D.AddForce(new Vector2 (h*movForceMag, 0)); //add a horizontal force based on player's left/right input
		//rigidbody2D.velocity = new Vector2 (h*movForceMag, rigidbody2D.velocity.y);
		/*if (Mathf.Abs(rigidbody2D.velocity.x) > MAXSPEED && animator.GetBool("walkRight")) //top off based on max speed, will this mess up dash?
		{
			//set max speed based on current direction
			rigidbody2D.velocity = new Vector2(MAXSPEED * Mathf.Sign(rigidbody2D.velocity.x),rigidbody2D.velocity.y);
		}*/
		if (eManager.Equals(EState.dashing)) {
			if (h > 0 && eManager.Equals(OPDirection.left))
			{
				dashScript.InterruptDash();
			}
			else if (h < 0 && eManager.Equals(OPDirection.right))
			{
				dashScript.InterruptDash();
			}
			else //means player is dashing and didn't input for an opposite direction
			{
				return; //if player is dashing we should not calculate left and right movement
			}
		} 
		AState currAState = eManager.GetAttackState ();
		if (h != 0 && (currAState.Equals(AState.normal) || currAState.Equals(AState.airattack) || currAState.Equals(AState.shooting)
		               || currAState.Equals(AState.spintowin) || currAState.Equals(AState.debug))) {
			CalcHVeloctity(Mathf.Sign(h));
			if (h > 0)	{
				transform.localScale = new Vector3(myLocalScale,transform.localScale.y,transform.localScale.z);
				eManager.SetOPDirectionState(OPDirection.right);
			}
			else {
				transform.localScale = new Vector3(-myLocalScale,transform.localScale.y,transform.localScale.z);
				eManager.SetOPDirectionState(OPDirection.left);
			}
			animator.SetBool("walkRight",true);
		}
	}

	private void CalcHVeloctity (float dir)
	{ //dir variable passed in as indication of whether player pressed left or right
		if (myRigidBody.velocity.x != (MAXSPEED * dir)) { //only accelerate if player hasn't already hit it's max velocity in that direction
			float newV = myRigidBody.velocity.x + (ACCEL * Time.deltaTime * dir); //increase speed in whatever direction player pressed
			if (Mathf.Abs (newV) > MAXSPEED) {
				newV = MAXSPEED * dir; //account for if velocity change would send user over max horizontal speed
			}
			myRigidBody.velocity = new Vector2 (newV, myRigidBody.velocity.y); //set speed to value established by equations
		}
	}

	public void DashInput()
	{
		if (dashScript.DashInput ()) { 
			//let dash script return a boolean to tell you whether or not you are actually going to dash, useful for restricting multiple air dashes
			if (BroadcastNoDashBonusLostMessage) {
				BroadcastNoDashBonusLostMessage = false;
				Utilities.SendToListeners (new BonusLostMessage (gameObject, OmonoPehaviour.ms_BONUSLOST, whoAmI, WhichBonus.NoDash));
			}
			if (!pGroundChecker.isGrounded())
			{
				dashScript.SetDashRestricted(true);
			}
		}
	}

	public void JumpInput()
	{
			AState currAState = eManager.GetAttackState ();
			//Debug.Log(transform.name+ " is trying to jump");
			if (pGroundChecker.isGrounded ()) { 
						if (currAState.Equals (AState.manup)) { //allows for jump canceling launcher, sets astate back to normal so below if statement will allow jump
								eManager.SetAttackState (AState.normal);
								currAState = AState.normal;
						}
					if ((currAState.Equals (AState.normal) || currAState.Equals (AState.shooting) || currAState.Equals (AState.airattack))) {
							myRigidBody.velocity = new Vector2 (myRigidBody.velocity.x, JUMPSPEED);
							dashScript.InterruptDash ();
				if (BroadcastNoJumpBonusLostMessage) {
					BroadcastNoJumpBonusLostMessage = false;
					Utilities.SendToListeners (new BonusLostMessage (gameObject, OmonoPehaviour.ms_BONUSLOST, whoAmI, WhichBonus.NoJump));
				}
							//animator.SetBool("Melee1", false); //stop attacking if you jump
							//rigidbody2D.AddForce(new Vector2 (0, 3000));
					}
			}
		else
		{
			int direction;
			if (pWallJumper.CanWallJump(out direction)) {
				dashScript.InterruptDash();
				dashScript.SetDashRestricted (false);
				pWallJumper.PerformWallJump(direction);
				if (BroadcastNoJumpBonusLostMessage) {
					BroadcastNoJumpBonusLostMessage = false;
					Utilities.SendToListeners (new BonusLostMessage (gameObject, OmonoPehaviour.ms_BONUSLOST, whoAmI, WhichBonus.NoJump));
				}
			}
		}
	}


	public void DropThrough()
	{
		/*Collider2D underMe = pGroundChecker.getColliderUnderEntity();
		if (underMe) //null checking
		{
			if (Utilities.hasMatchingTag("DropThru",underMe.gameObject)) //if the platform under the player can be dropped through...
			{
				Physics2D.IgnoreCollision(collider2D,underMe,true); //remove the collision so that it can drop thru
			}*/
		if (pGroundChecker.CanDropThrough())
		{
			pGroundChecker.DropThrough();
		}
			else
			{
				JumpInput(); //if player is standing on a platform that can't be dropped through, should still be able to jump off that platform 
			}
		//Debug.Log(transform.name+ " is trying to drop through the platform below him");
		//rigidbody2D.velocity = new Vector2 (rigidbody2D.velocity.x, 5);
		//rigidbody2D.AddForce(new Vector2 (0, 3000));
	}

	public void MeleeInput() {
		AState currAState = eManager.GetAttackState ();
		if (eManager.GetEntityState().Equals(EState.normal)) {
			if (pGroundChecker.isGrounded()) {
				if (currAState.Equals(AState.attack1)) {
					eManager.SetAttackState(AState.attack2);
				} else if (currAState.Equals(AState.attack2)) {
					eManager.SetAttackState(AState.attack3);
				} else if (currAState.Equals(AState.normal)){
					eManager.SetAttackState (AState.attack1);
				}
			} else if (currAState.Equals(AState.normal) && !currAState.Equals(AState.spintowin)) {
				eManager.SetAttackState(AState.airattack);
			}
			if (BroadcastNoMeleeBonusLostMessage){
				BroadcastNoMeleeBonusLostMessage = false;
				Utilities.SendToListeners(new BonusLostMessage(gameObject,OmonoPehaviour.ms_BONUSLOST,whoAmI, WhichBonus.NoMelee));
				}
		}
	}

	public void LaunchMeleeInput()
	{
		if (pGroundChecker.isGrounded())
		{
			eManager.SetAttackState(AState.manup);
		}
		else
		{
			MeleeInput(); //if the player is in the air, input instead read as air melee attack 
		}
	}

	public void DiveMeleeInput()
	{
		if (eManager.Equals(AState.debug)) {return;}
		if (!pGroundChecker.isGrounded())
		{
			eManager.SetAttackState(AState.debug);
			myRigidBody.velocity = new Vector2(0,-5);
			dashScript.InterruptDash (); //immediately stop dashing if input has been entered
			dashScript.SetDashRestricted(true);
		}
		else
		{
			MeleeInput(); //if the player is on the ground, input instead read as ground melee attack
		}
	}

	public void MashMeleeAirInput()
	{
				if (!pGroundChecker.isGrounded() && !eManager.Equals(AState.debug))
		{
			//Debug.Log ("Mash Melee Air Input Recognized.");
			myRigidBody.velocity = new Vector2(0, 0);
			myRigidBody.gravityScale = 0;
			eManager.SetAttackState(AState.spintowin);
			Invoke("resetGravityScale", 1f);
		}
	}

	public void resetGravityScale() {
				myRigidBody.gravityScale = Utilities.gravityScale; //FOR YOU PETER!!!!
	}

	public void RangeInput() {
		AState currAState = eManager.GetAttackState ();
		if (currAState.Equals(AState.normal) || currAState.Equals(AState.shooting)) {
			equipManager.FireCurrentProjectile();
			eManager.SetAttackState (AState.shooting);
			if (BroadcastNoRangeBonusLostMessage) {
				Utilities.SendToListeners(new BonusLostMessage(gameObject,OmonoPehaviour.ms_BONUSLOST,whoAmI, WhichBonus.NoRange));
			}
		}
	}

	public void OverPowerInput()
	{
		if (BroadcastNoOPBonusLostMessage) {
			Utilities.SendToListeners (new BonusLostMessage (gameObject, OmonoPehaviour.ms_BONUSLOST, whoAmI, WhichBonus.NoOP));
		}
	}

	/*
	 * Broadcasted when player collides with platform object
	 * Used to reset certain states and variables
	 */
	public void GroundSelf()
	{
		if (eManager.Equals(AState.debug))
		{
			eManager.SetAttackState(AState.normal);
		}
		dashScript.SetDashRestricted (false);
	}
}
