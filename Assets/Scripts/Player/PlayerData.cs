﻿using UnityEngine;
using System.Collections;

public enum WhichPlayer { Player1 = 1, Player2 = 2, Neither = 3, Both = 4 };
public enum WhichCharacter { O = 1, P = 2, Neither = 3, Both = 4 };
public class PlayerData {
		WhichPlayer playerNum;
		WhichCharacter chosenChar;
		GameObject levelInstance;
		string horizontalInput;
		string verticalInput;
		//string leftInput;
		//string rightInput;
		//string upInput;
		//string downInput;
		string rangeInput;
		string meleeInput;
		string jumpInput;
		string dashInput;
		int numOfLives;
        int maxLives = 3;
        bool cpuControlled;
		//int level; //future implementation
		//int score; //future implementation
		
	/* static pointer to Player1, once the player1 object is created, set pointer to it
     * Allows all other code to reference these objects to get all data on that player
	 * and theoretically allow the data to persist across all stages/scenes
	 */
		static PlayerData p1Data=null;
		static PlayerData p2Data=null;
		
	/**
	* Constructor creates player metadata based on which character they picked
	*/
	public PlayerData(WhichCharacter wC, WhichPlayer wP, bool isCpuControlled)
	{
		chosenChar = wC;
		playerNum = wP;
		if (wC.Equals(WhichCharacter.O))
		{
			SetControls("DPadX1","DPadY1", "Range1", "Melee1", "Jump1", "Dash1");
		}
		else
		{
			SetControls("DPadX2","DPadY2", "Range2", "Melee2", "Jump2", "Dash2");
		}
		numOfLives = 3;
        cpuControlled = isCpuControlled;
	}

    public PlayerData(WhichCharacter wC, WhichPlayer wP) : this(wC, wP, false) { }


    /**
	* for debugging
	*/
    static void CreateDummyData()
	{
			Debug.Log("Should not be firing if loaded from main menu, like seriously.");
			p1Data = new PlayerData(WhichCharacter.O, WhichPlayer.Player1);
			p2Data = new PlayerData(WhichCharacter.P, WhichPlayer.Player2);
	}
		
	/**
	* Theoretically called when level is first loaded and O/P are first created
	*/
		public static void SetCharacterLevelInstance(GameObject OInstance, GameObject PInstance)
		{
			if(p1Data == null || p2Data == null)
			{
				CreateDummyData();
			}
			
			if (p1Data.getChosenChar().Equals(WhichCharacter.O))
			{
				p1Data.levelInstance = OInstance;
				p2Data.levelInstance = PInstance;
			}
			
			if (p1Data.getChosenChar().Equals(WhichCharacter.P))
			{
				p1Data.levelInstance = PInstance;
				p2Data.levelInstance = OInstance;
			}
		}
		
	/**
	* When O or P prefabs are instantiated, the game object must be linked to correct player metadata
	* Theoretically called when  scene is loaded or
	* player dies and a new instance has to be respawned
	*/
		public static void SetCharacterLevelInstance(GameObject playerInstance, PlayerData pDataToLink)
		{
			if (playerInstance == null) { return; } //should not be necessary but w/e
			if(pDataToLink == null) //should theoretically never happen
			{
				CreateDummyData();
			}
			
			pDataToLink.levelInstance = playerInstance;
		}
		
	/**
	* When O or P prefabs are instantiated, the game object must be linked to correct player metadata
	* Theoretically called when  scene is loaded or
	* player dies and a new instance has to be respawned
    * July 10 2017 - Modified to return player data corresponding to the instance
	*/
	public static PlayerData SetCharacterLevelInstance(GameObject playerInstance, WhichCharacter wC)
	{
		if (playerInstance == null) { return null; } //should not be necessary but w/e
		if(p1Data == null || p2Data == null)
		{
			CreateDummyData();
		}
		if (p1Data.getChosenChar().Equals(wC))
		{
			p1Data.levelInstance = playerInstance;
            return p1Data;
		}
		else
		{
			p2Data.levelInstance = playerInstance;
            return p2Data;
		}
	}
		/**
	* Assign controls to player data so that we know which player is tied to what buttons
	*
	*/
		public void SetControls(string horizontalMovement, string verticalMovement, string range, string melee, string jump, string dash)
		{
			horizontalInput = horizontalMovement;
			verticalInput = verticalMovement;
			/*leftInput = left;
			rightInput = right;
			upInput = up;
			downInput = down;*/
			rangeInput = range;
			meleeInput = melee;
			jumpInput = jump;
			dashInput = dash;
		}
		
		/**
	* Called when O/P just were created /respawned and don't know what their controls are
	*/
		public void getControls(out string h, out string v, out string ra, out string m, out string j, out string da)
		{
			/*l = leftInput;
			r = rightInput;
			u = upInput;
			d = downInput;*/
			h = horizontalInput;
			v = verticalInput;
			ra = rangeInput;
			m = meleeInput;
			j = jumpInput;
			da = dashInput;
		}
		
	/**
	* Called when O/P just were created /respawned and don't know what their controls are
	*/
	public static void GetControls(WhichCharacter wC, out string hor, out string vert, out string range, out string melee, out string jump, out string dash)
		{
		if(p1Data == null || p2Data == null)
		{
			CreateDummyData();
		}
			if (p1Data.getChosenChar().Equals(wC))
			{
				p1Data.getControls(out hor, out vert, out range, out melee, out jump, out dash);
			}
			else
			{
				p2Data.getControls(out hor, out vert, out range, out melee, out jump, out dash);
			}
		}
		
		//Get character associated with this player data
		public WhichCharacter getChosenChar()
		{
			return chosenChar;
		}
		
	//Get character associated with this player data
	public GameObject getStageInstance()
	{
		return levelInstance;
	}
		/**
	* Called by HUD to figure out whether or not to draw player data on the left or right
	*/
		public static bool amIPlayer1(WhichCharacter myCharacter)
		{
			if(p1Data ==null || p2Data==null)
			{
				CreateDummyData();
			}
			
			if (p1Data.getChosenChar().Equals(myCharacter))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		/*
	* Since the two static variables are public, this will probably never get set. Like maybe if
	* I didn't violate proper data structure rules, but I totally did like a bum.
	*/
		public static void setTheSessionPlayers(PlayerData firstPlayer, PlayerData secondPlayer)
		{
			p1Data = firstPlayer;
			p2Data = secondPlayer;
		}

	public static PlayerData GetPlayer1()
	{
		if (p1Data == null) {CreateDummyData();}
		return p1Data;
	}
	public static PlayerData GetPlayer2()
	{
		if (p2Data == null) {CreateDummyData();}
		return p2Data;
	}

	public int GetNumOfLives()
	{
		return numOfLives;
	}
	public void SetNumOfLives(int num)
	{
		numOfLives = num;
	}

    public void ResetToMaxLives()
    {
        numOfLives = maxLives;
    }

    public bool IsCpuControlled()
    {
        return cpuControlled;
    }
	}