﻿//using UnityEngine;
//using System.Collections;
//
//public class PlayerHUD : OmonoPehaviour {
//	//if left is true, draw HUD stuff on left and reference p1Data to do it
//	//if left is false, draw HUD stuff on right and reference p2Data to do it
//	
//	// O/P specific settings
//	public Texture2D OHUDIcon;
//	public Texture2D PHUDIcon;
//	public WhichPlayer whosHUD;
//	
//	public GameObject OrpPrefab;
//
//	//Variables to be set based on which player/character the HUD corresponds to
//	private bool left;
//	private Texture2D charface;
//	private PlayerData playerRef;
//	
//	//GUI controls
//	public GUIStyle numDisplay;
//
//	private bool drawPlayerHUD;
//	
//	//reference scripts on associated character prefab
//	private Entity eScript;
//	private OverPower opScript;
//	private EquipmentHandler eHandler; //so we can get the ammo and the player's current weapon
//	
//	/* Local variables to be updated when changed instead 
//    of having constant method calls every ONGUI */
//	private int currAmmo;
//	private float currHealth;
//	private float maxHealth;
//	private int currOP;
//	private int maxOP;
//	private PlayerWeapon currWeapon;
//	private Texture2D weaponIcon;
//	private Vector3 orpSpawnLocation; //precalculated location for opr drop on hud attack
//
//	protected void callDamage() {
//		//Debug.Log ("Calling damage");
//		if (OrpPrefab)
//		{
//			GameObject spawnedOrp = (GameObject)Instantiate(OrpPrefab,orpSpawnLocation,Quaternion.Euler(0,0,0));
//			spawnedOrp.GetComponent<Rigidbody2D>().velocity = new Vector2(Random.Range(-16f,16f),-9);
//		}
//	}
//
//	protected override void OnStart () {
//		drawPlayerHUD = false; //protect against null reference exceptions by not drawing immediately
//		orpSpawnLocation = new Vector3(transform.position.x,transform.position.y-.25f,0);
//		if (whosHUD.Equals(WhichPlayer.Player1)) //player 1 HUD drawn on the left
//		{
//			left = true;
//			playerRef = PlayerData.GetPlayer1();
//		}
//		else //player2 HUD drawn on the right
//		{
//			left = false;
//			playerRef = PlayerData.GetPlayer2();
//		}
//		
//		if (playerRef.getChosenChar().Equals(WhichCharacter.O)) //figure out which HUD head icon to use
//		{
//			charface = OHUDIcon;
//		}
//		else
//		{
//			charface = PHUDIcon;
//		}
//		//register listeners so that when messages are broadcast, updates are triggered
//		Utilities.RegisterListener(new Listener(OmonoPehaviour.ms_CHARSPAWNED,gameObject,"GrabInstanceData"));
//		Utilities.RegisterListener(new Listener(OmonoPehaviour.ms_CHARDIED,gameObject,"CharDied"));
//		Utilities.RegisterListener(new Listener(OmonoPehaviour.ms_EQUIPCHANGE,gameObject,"UpdateWeapon"));
//		Utilities.RegisterListener(new Listener(OmonoPehaviour.ms_HEALTHCHANGE,gameObject,"UpdateHealth"));
//		Utilities.RegisterListener(new Listener(OmonoPehaviour.ms_AMMOCHANGE,gameObject,"UpdateAmmo"));
//		Utilities.RegisterListener(new Listener(OmonoPehaviour.ms_OPPCHANGE,gameObject,"UpdateOP"));
//
//		//check if level instance is null, if it is not, then grab components of level instance
//		GrabInstanceData(null);
//		/* b/c of the order of Start methods, instance may not actually exist, so HUD will not draw to screen
//        *  if this happens, it will be up to the registered listener to be alerted when the instance exists and has registered
//        *  itself to the player metadata
//        */
//	}
//	
//	public void setHUDStatus(bool isActive)
//	{
//		drawPlayerHUD = isActive;
//	}
//	
//	void OnGUI()
//	{
//		if (drawPlayerHUD)
//		{
//			if (left)
//			{
//				GUI.DrawTexture(new Rect(Screen.width/20, Screen.height / 30, 50, 50), charface);
//				GUI.Label(new Rect(Screen.width/20, Screen.height / 30, 30, 30), ""+playerRef.GetNumOfLives());
//				GUI.DrawTexture(new Rect(2* Screen.width/20, Screen.height / 30, 50, 50), weaponIcon);
//				if (!currWeapon.pType.Equals(ProjType.straight))
//				{
//					GUI.Label(new Rect(2* Screen.width/20, Screen.height / 30, 50, 50), ""+currAmmo, numDisplay);
//				}
//				GUI.Label(new Rect(3* Screen.width/20, Screen.height / 30, 50, 50), "Health: "+currHealth);
//				GUI.Label(new Rect(4* Screen.width/20, Screen.height / 30, 50, 50), "OP: "+currOP);
//			}
//			else
//			{
//				GUI.DrawTexture(new Rect(Screen.width - Screen.width/20, Screen.height / 30, 50, 50), charface);
//				GUI.Label(new Rect(Screen.width - Screen.width/20, Screen.height / 30, 30, 30), ""+playerRef.GetNumOfLives());
//				GUI.DrawTexture(new Rect(Screen.width - (2* Screen.width/20), Screen.height /30, 50, 50), weaponIcon);
//				if (!currWeapon.pType.Equals(ProjType.straight))
//				{
//					GUI.Label(new Rect(Screen.width - (2* Screen.width/20), Screen.height / 30, 50, 50), ""+currAmmo, numDisplay);
//				}
//				GUI.Label(new Rect(Screen.width - (3* Screen.width/20), Screen.height / 30, 50, 50), "Health: "+currHealth);
//				GUI.Label(new Rect(Screen.width - (4* Screen.width/20), Screen.height / 30, 50, 50), "OP: "+ currOP);
//			}
//		}
//	}
//	
//	void GrabInstanceData(Message m)
//	{
//		if (playerRef.getStageInstance() != null)
//		{
//			GameObject instance = playerRef.getStageInstance();
//			eScript = instance.GetComponent<Entity>();
//			opScript = instance.GetComponent<OverPower>();
//			eHandler = instance.GetComponent<EquipmentHandler>();
//			currHealth = eScript.GetHealth();
//			maxHealth = eScript.GetMaxHealth();
//			currOP = opScript.GetOPP();
//			maxOP = opScript.GetMaxOPP();
//			currWeapon = eHandler.GetCurrWeapon();
//			weaponIcon = eHandler.GetCurrWeapon().GetHUDTexture();
//			currAmmo = eHandler.GetCurrAmmo();
//			drawPlayerHUD = true; //now HUD is read to be drawn
//		}
//	}
//
//	void CharDied(Message m)
//	{
//
//	}
//
//	//used to determine if script reference is null (most likely b/c attached object was destroyed)
//	bool ScriptIsAlive(OmonoPehaviour elScript)
//	{
//		return (elScript != null);
//	}
//	
//	/* Local variables to be updated when changed instead 
//     * of having constant method calls every ONGUI
//     */
//	void UpdateWeapon(Message m)
//	{
//		if (ScriptIsAlive(eHandler))
//		{
//			currWeapon = eHandler.GetCurrWeapon();
//			weaponIcon = eHandler.GetCurrWeapon().GetHUDTexture();
//			currAmmo = eHandler.GetCurrAmmo();
//		}
//	}
//	
//	void UpdateAmmo(Message m)
//	{
//		if (ScriptIsAlive(eHandler))
//		{
//			currAmmo = eHandler.GetCurrAmmo();
//		}
//	}
//	
//	void UpdateHealth(Message m)
//	{
//		if (ScriptIsAlive(eScript))
//		{
//			currHealth = eScript.GetHealth();
//		}
//	}
//
//	void UpdateOP(Message m)
//	{
//		if (ScriptIsAlive(opScript))
//		{
//			currOP = opScript.GetOPP();
//		}
//	}
//
//	void EnableHUDAttack()
//	{
//		GetComponent<Collider2D>().enabled = true;
//	}
//
//	
//	void DisableHUDAttack()
//	{
//		GetComponent<Collider2D>().enabled = false;
//	}
//}
