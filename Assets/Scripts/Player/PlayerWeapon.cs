﻿using UnityEngine;
using System.Collections;
//using UnityEngine.UI;

[System.Serializable]
public class PlayerWeapon  {

	public GameObject WeaponPrefab;
	public int ammo;
	public ProjType pType;
	public Texture2D HUDTexture;
	public Sprite HUDImage;

	public Texture2D GetHUDTexture()
	{
		return HUDTexture;
	}

	public Sprite GetHUDImage()
	{
		return HUDImage;
	}
}
