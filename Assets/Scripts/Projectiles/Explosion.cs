﻿using UnityEngine;
using System.Collections;

public class Explosion : OmonoPehaviour {
	
	public float lifeSpan;
	public int damageValue; //how much damage they do
	public string TargetType; //Set to Player or Enemy in Inspector
	//public int knockBackVelocity; //for knockback
	public float hKnockBack;
	public float vKnockBack;
	public float hitDelay; //for a delay between when another hit would register

	protected GameObject entity;
	protected EntityType attackerEntityType = EntityType.NothingThatMatters;
	protected WhichCharacter attackerCharacterIfPlayer = WhichCharacter.Neither;
	protected override void OnStart () {
		Destroy(gameObject,lifeSpan);
	}
	
	void OnTriggerStay2D (Collider2D collInfo) {
		//if meant to hit player, will hit player, if meant to hit enemy, will hit enemy
		if (Utilities.hasMatchingTag (TargetType, collInfo.gameObject)) {
			//DamageStruct thisisntastructanymore = new DamageStruct (damageValue, collider2D.gameObject, knockBackVelocity, hitDelay);
			//struct used to pass more than one parameter through send message, which only lets you pass one object as a parameter
			//collInfo.gameObject.SendMessage("callDamage",thisisntastructanymore);
			//collInfo.gameObject.SendMessage ("SlowYourself"); //call method in object that should get slowed and they will slow themselves
			collInfo.gameObject.SendMessage("callDamage",SendMessageOptions.DontRequireReceiver);
			collInfo.gameObject.GetComponent<Entity>().damageProperties(gameObject, damageValue, hKnockBack, vKnockBack, hitDelay,attackerEntityType,attackerCharacterIfPlayer,null);
		}
	}
	
}