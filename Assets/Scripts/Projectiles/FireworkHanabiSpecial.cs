﻿using UnityEngine;
using System.Collections;

public class FireworkHanabiSpecial : FireworkProjectile {
		private float explLocMod = 2f;
		protected override void Detonate()
		{
				Instantiate(ExplosionPrefab,transform.position,Quaternion.Euler(0,0,0));
				Instantiate(ExplosionPrefab,transform.position + new Vector3(explLocMod,explLocMod,0),Quaternion.Euler(0,0,0));
				Instantiate(ExplosionPrefab,transform.position + new Vector3(-explLocMod,explLocMod,0),Quaternion.Euler(0,0,0));
				Instantiate(ExplosionPrefab,transform.position + new Vector3(explLocMod,-explLocMod,0),Quaternion.Euler(0,0,0));
				Instantiate(ExplosionPrefab,transform.position + new Vector3(-explLocMod,-explLocMod,0),Quaternion.Euler(0,0,0));
		}
}
