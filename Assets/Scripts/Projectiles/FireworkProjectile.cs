﻿using UnityEngine;
using System.Collections;

public class FireworkProjectile : MissileProjectile {

		protected override void OnStart() {
				Invoke ("Detonate", projectileDuration-.0001f);
				base.OnStart ();

		}
}
