﻿using UnityEngine;
using System.Collections;

public class MissileProjectile : StraightProjectile {
	public GameObject ExplosionPrefab;
	
	new void OnCollisionEnter2D(Collision2D collInfo) {
			Detonate ();
			base.OnCollisionEnter2D(collInfo);
	}
	
		protected virtual void Detonate()
		{
				Instantiate(ExplosionPrefab,transform.position,Quaternion.Euler(0,0,0));
		}
	
}