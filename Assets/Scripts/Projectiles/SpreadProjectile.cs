﻿using UnityEngine;
using System.Collections;

public class SpreadProjectile : BulletDesu {
	public int projectileSpeed=0; //for spell that shoot something out
	private Vector3 clonePosition;
	private Vector3 cloneVelocity;
	private Quaternion cloneOrientation;
	private OPDirection direction;
	private GameObject bulletToClone;


	public override void execute(GameObject g) {
		entity = g;
		direction = entityStates.GetDirState ();
		bulletToClone = gameObject;
		if (entity == null){entity = GameObject.FindGameObjectWithTag("Entity");}
		createProjectile (0.5f, 0, 0, 0);
		createProjectile (0.5f, 0.15f, 0, 3);
		createProjectile (0.5f, -0.15f, 0, -3);
	}
	
	public void createProjectile(float xPos, float yPos, float rotation, float angle) {
		if (direction == OPDirection.left) {
			clonePosition = entity.transform.position + new Vector3(-xPos, yPos, 0);
			cloneVelocity = new Vector3 (-projectileSpeed, angle, 0);
			cloneOrientation = Quaternion.Euler(0, 0, rotation + 180);
		}
		else if (direction == OPDirection.right) {
			clonePosition = entity.transform.position + new Vector3(xPos,yPos,0);
			cloneVelocity = new Vector3 (projectileSpeed, angle, 0);
			cloneOrientation = Quaternion.Euler(0, 0, rotation);
		}
		Utilities.cloneObject(bulletToClone, clonePosition, cloneVelocity, cloneOrientation);
		Physics2D.IgnoreCollision (bulletToClone.GetComponent<Collider2D>(), entity.GetComponent<Collider2D>());
		Destroy (bulletToClone,projectileDuration);
	}
}
