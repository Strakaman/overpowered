﻿using UnityEngine;
using System.Collections;

public class ScreenFader : OmonoPehaviour {

	SpriteRenderer mySpriteRenderer;

	//adjust the alpha of an all black sprite to simulate that you are fading in and out

	protected override void OnStart () {
		mySpriteRenderer = GetComponent<SpriteRenderer> ();
		mySpriteRenderer.enabled = true;
		float alpha = 0;
		Color newA = new Color (mySpriteRenderer.material.color.r, mySpriteRenderer.material.color.g, mySpriteRenderer.material.color.b, alpha);
		mySpriteRenderer.material.color = newA;
		//Invoke ("StartFade", 2); //for testing
		//Invoke ("EndFade", 2); //for testing
		Utilities.RegisterListener(new Listener(OmonoPehaviour.ms_ROOMENDED,gameObject,"RoomFadeTransition"));
	}
	
	// Update is called once per frame
	void RoomFadeTransition () {
		Invoke ("StartFade", 3); //fade in and then back out
		Invoke ("EndFade", 5); 
	}

	void StartFade()
	{
		StartCoroutine ("FadeIn");
	}

	IEnumerator FadeIn()
	{
		float fadeDuration = 0;
		while (fadeDuration < 1.1f) {
			fadeDuration += .1f;
			float alpha = fadeDuration;
			Color newA = new Color (mySpriteRenderer.material.color.r, mySpriteRenderer.material.color.g, mySpriteRenderer.material.color.b, alpha);
			mySpriteRenderer.material.color = newA;
			yield return new WaitForSeconds (.1f);
			//Debug.LogWarning ("Fading...Current alpha at: " + alpha);
		}
	}

	void EndFade()
	{
		StartCoroutine ("FadeOut");

	}

	IEnumerator FadeOut()
	{
		float fadeDuration = 1.1f;
		while (fadeDuration > 0) {
			fadeDuration -= .1f;
			float alpha = fadeDuration;
			Color newA = new Color (mySpriteRenderer.material.color.r, mySpriteRenderer.material.color.g, mySpriteRenderer.material.color.b, alpha);
			mySpriteRenderer.material.color = newA;
			yield return new WaitForSeconds (.1f);
			//Debug.LogWarning ("Fading out...Current alpha at: " + alpha);
		}
	}

}
