﻿using UnityEngine;
using System.Collections;

public enum SoundType
{
	pickup = 1
};
public class SoundEffectPlayer : OmonoPehaviour {
	
	public AudioClip pickupSound;
	// Use this for initialization
	protected override void OnStart () {
	
		}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void playSoundEffect(SoundType typeofSound)
	{
		if (typeofSound.Equals(SoundType.pickup))
		{
			GetComponent<AudioSource>().clip = pickupSound;
			GetComponent<AudioSource>().Play();
		}
	}
	
}
