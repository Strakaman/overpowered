﻿using System;
using UnityEngine;
using System.Collections;

public enum Talker { None = 0 , O = 1, P = 2, Announcer = 3};
public enum SpeechBubbleSide {Left = 0, Right = 1, Both = 2, Neither = 3};

[System.Serializable]	
public class TalkLine
{
	public Talker leftTalker;
	public Talker rightTalker;
	public SpeechBubbleSide ArrowDir;
	public string Sentence;
}


