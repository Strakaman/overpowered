﻿using UnityEngine;
using System.Collections;
using System;

public class TextCutScene : OmonoPehaviour 
{
	//public string[] talkLines;
	public string NextSceneName;
	public Texture2D BackGroundImage;
	private string currText;
	public Texture2D dialogTexture;
	public int textScrollSpeed;
	private bool talking;
	//private bool toggleGUI; //used for animating mouths which was removed
	private bool textIsScrolling;
	private int currentLine;
	public Texture2D npcImage;
	public TalkLine[] dialogueLines;
	public Texture2D[] npcImages;
	//int randomImageIndex;
	string displayText;
	public GUIStyle oppaFontStyle = new GUIStyle();
	public Texture2D oImage;
	public Texture2D pImage;
	public Texture2D blank;
	public Texture2D aImage;
	private Texture2D textureToDraw;
	private Texture2D leftImage;
	private Texture2D rightImage;
	private Rect drawTangle;
	private Rect leftRect;
	private Rect rightRect;
	
	protected override void OnStart () {
		StartCutScene(gameObject);
	}
	
	// Update is called once per frame
	void Update ()
	{
		/* switched order of statements so that check to see if the line is finished comes before 
		 * check to see if text is scrolling, makes code work so oh well
		 */
		if (talking) {
			if (Input.GetKeyDown ("f")) {
				if (currText.Equals (dialogueLines[currentLine].Sentence)) {
					//display next line
					textIsScrolling = false;
					if (currentLine < dialogueLines.Length - 1) { //player not on last line yet
						currentLine++; 
						updateDrawImage(); //update what pic to draw in case npc changed
						StartCoroutine (startScrolling ());
					} else {  //player just finished reading the last line
						currentLine = 0;
						currText = "";
						updateNPC (false); //sets all the booleans to display
						FollowUps (); //trigger follow ups in any subclasses, maybe replenish health or spawn an item or something
						Invoke("NextLevel",2);
					}
				} else if (textIsScrolling) {
					//display full line
					currText = dialogueLines [currentLine].Sentence;
					textIsScrolling = false;
				}
			}
			//auto skip to the last line if start is pressed
			if(Input.GetKeyDown("p")) {
				currentLine = dialogueLines.Length - 1;
				updateDrawImage(); //update what pic to draw in case npc changed
				currText = dialogueLines[currentLine].Sentence;
				textIsScrolling = false;
				updateNPC(false);
				Invoke("NextLevel",.5f);
			}
		}
	}
	
	IEnumerator startScrolling ()
	{
		textIsScrolling = true;
		int startLine = currentLine;
		displayText = "";
		for (int i = 0; i < dialogueLines[currentLine].Sentence.Length; i++) {
			if (textIsScrolling && currentLine == startLine) {
				displayText += dialogueLines [currentLine].Sentence [i];
				currText = displayText;
				yield return new WaitForSeconds (1 / textScrollSpeed);
			} else {
				yield return true;
			}
		}
	}
	
	public void updateNPC (bool isNPCactive)
	{
		talking = isNPCactive;
		}
	
	public void StartCutScene (GameObject player)
	{
		//ensures text and pic elements are in the right position
		if (dialogueLines.Length == 0) {return;}
		//talkTextGUI.transform.position = new Vector3 (0, -.12f, talkTextGUI.transform.position.z);
		//textBoxTexture.transform.position = new Vector3 (0.3198967f, 0.07225594f, textBoxTexture.transform.position.z);
		//transform.parent.transform.position = new Vector3 (0, 0, -10);
		updateNPC (true);
		currentLine = 0;
		//randomImageIndex = Random.Range(0,npcImages.Length);
		updateDrawImage(); //update what pic to draw in case npc changed
		StartCoroutine (startScrolling ());
	}
	
	void OnGUI()
	{
		GUI.DrawTexture(new Rect(0,0,Screen.width,Screen.height),BackGroundImage);
		if (talking) //scale image to screen size
		{
			//GUI.DrawTexture(new Rect(0, Screen.height/2.5f, Screen.width/3.5f, Screen.height/2),leftImage);
			//GUI.DrawTexture(new Rect(Screen.width - Screen.width/3.5f - 5, Screen.height/2.5f, Screen.width/3.5f, Screen.height/2),rightImage);
			GUI.DrawTexture(new Rect(280, Screen.height/2.5f, -254, 350),leftImage);
			GUI.DrawTexture(new Rect(Screen.width - Screen.width/3.5f - 5, Screen.height/2.5f, 254, 350),rightImage);
			//if (leftRect != null) {GUI.DrawTexture(leftRect,arrow);}
			//if (rightRect != null) {GUI.DrawTexture(rightRect,arrow);}
			GUI.DrawTexture(new Rect(-Screen.width/2.5f,Screen.height/1.15f,Screen.width*1.5f,(Screen.height/12)+40),dialogTexture);
			GUI.Label(new Rect(30,Screen.height/1.11f,Screen.width*.85f,(Screen.height/12)+40),currText,oppaFontStyle);
		}
	}
	
	
	
	public virtual void FollowUps()
	{
		
	}
	
	void updateDrawImage()
	{
		leftImage = RealUpdateDrawImage(dialogueLines[currentLine].leftTalker);
		rightImage = RealUpdateDrawImage(dialogueLines[currentLine].rightTalker);
		GetArrowPosition(dialogueLines[currentLine].ArrowDir,out leftRect,out rightRect);
	}
		
	void NextLevel()
	{
        Debug.LogWarning("next " + NextSceneName);
		if (!NextSceneName.Equals(""))
		{
			Utilities.LoadLevel(NextSceneName);
		}
	}
	
	Texture2D RealUpdateDrawImage(Talker daTalker)
	{
		if (daTalker.Equals(Talker.None))
		{
			return blank;
		}
		else if (daTalker.Equals(Talker.O))
		{
			return oImage;
		}
		else if (daTalker.Equals(Talker.P))
		{
			return pImage;
		}
		else if (daTalker.Equals(Talker.Announcer))
		{
			return aImage;
		}
		else
		{
			return null;
		}
	}
	
	void GetArrowPosition(SpeechBubbleSide curOPDirection, out Rect leftRect, out Rect rightRect)
	{

		if (curOPDirection.Equals(SpeechBubbleSide.Left))
		{
			leftRect =  new Rect(25,30,100,50);
			rightRect = new Rect(-1000,-1000,0,0);
		}
		else if (curOPDirection.Equals(SpeechBubbleSide.Right))
		{
			leftRect = new Rect(-1000,-1000,0,0);
			rightRect = new Rect(Screen.width-25,30,100,50);
		}
		else if (curOPDirection.Equals(SpeechBubbleSide.Both))
		{
			leftRect =  new Rect(25,30,100,50);
			rightRect = new Rect(Screen.width-25,30,100,50);
		}
		else
		{
			leftRect = new Rect(-1000,-1000,0,0);
			rightRect = new Rect(-1000,-1000,0,0);
		}
	}
	
}
