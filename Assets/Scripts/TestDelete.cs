﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Collections.Generic;

public class TestDelete : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	if (Input.GetKeyDown("space"))
		{
			try
			{
				Debug.Log("Test File Written To: " + Application.persistentDataPath);
				StreamWriter sw = new StreamWriter(Application.persistentDataPath + "/DebugResult.txt");
				Debug.Log("File stream opened.");
				sw.WriteLine("Testing");
				sw.WriteLine(gameObject.transform.position.x);
				sw.WriteLine(gameObject.transform.position.y);
				sw.WriteLine(gameObject.transform.position.z);
				sw.WriteLine("End of File");
				sw.Close();
				Debug.Log("File stream closed.");
			}
			catch (Exception e)
			{
				Debug.Log(e.Message);
			}
		}
	}
}
