using System;
using UnityEngine;
using System.Collections;

public class TimeBomb : Enemy
	{
		public int initialTime;
		private float timeLeft;
		public GameObject explosionToCreate;
		Animator animator ;
		
		protected override void OnStart () {
			timeLeft = (float)initialTime;
		}
		
		void Update()
		{
			timeLeft -= Time.deltaTime;
			UpdateAnimation();
			if (timeLeft <=0)
			{
				Instantiate(explosionToCreate,transform.position,Quaternion.Euler(0,0,0));
				Destroy(this);
			}
		}
		
		void UpdateAnimation()
		{
			int integerLeft = (int)timeLeft;
			if (integerLeft < 1)
			{
				//animator.SetInteger(0);
			}/*
			if (integerLeft < 2)
			{
				animator.SetInteger(1);
			}
			if (integerLeft < 3)
			{
				animator.SetInteger(2);
			}
			if (integerLeft < 4)
			{
				animator.SetInteger(3);
			}
			if (integerLeft < 5)
			{
				animator.SetInteger(4);
			}*/
		}
	
	
	}