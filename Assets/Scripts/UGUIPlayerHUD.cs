﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UGUIPlayerHUD : OmonoPehaviour {
	//if left is true, draw HUD stuff on left and reference p1Data to do it
	//if left is false, draw HUD stuff on right and reference p2Data to do it

	// O/P specific settings
	public Sprite OHUDIcon;
	public Sprite PHUDIcon;
	public WhichPlayer whosHUD;

	public GameObject OrpPrefab;

	//Variables to be set based on which player/character the HUD corresponds to
	//private bool left; //won't need as HUD is now a prefab with Unity GUi 4.6
	private Sprite charface;
	private PlayerData playerRef;

	//GUI controls
	public GUIStyle numDisplay;

	//child objects to main HUD Script
	public Image CharacterIconDisplay;
	public Image CurrentWeaponDisplay;
	public Text AmmoDisplay;
	public Text NumOfLivesDisplay;
	public Text HealthDisplay;
	public Text OPDisplay;
	public Transform OutOfLivesDisplay;

	private bool drawPlayerHUD;
	private int shotDir;
	//reference scripts on associated character prefab
	private Entity eScript;
	private OverPower opScript;
	private EquipmentHandler eHandler; //so we can get the ammo and the player's current weapon

	/* Local variables to be updated when changed instead
    of having constant method calls every ONGUI */
	private int currAmmo;
	private float currHealth;
	private float maxHealth;
	private int currOP;
	private int maxOP;
	private PlayerWeapon currWeapon;
//	private Texture2D weaponIcon;
	private Vector3 orpSpawnLocation; //precalculated location for opr drop on hud attack
	private Collider2D myCollider;

	protected void callDamage() {
		//Debug.Log ("Calling damage");
		if (OrpPrefab)
		{
			GameObject spawnedOrp = (GameObject)Instantiate(OrpPrefab,orpSpawnLocation,Quaternion.Euler(0,0,0));
			spawnedOrp.GetComponent<Rigidbody2D>().velocity = new Vector2(Random.Range(5,40f)*shotDir,-20);
		}
	}

	protected override void OnStart () {
		drawPlayerHUD = false; //protect against null reference exceptions by not drawing immediately
		myCollider = GetComponent<Collider2D>();
		orpSpawnLocation = new Vector3(transform.position.x,transform.position.y-.25f,0);
		//orpSpawnLocation = new Vector3(8,3,0);

		if (whosHUD.Equals(WhichPlayer.Player1)) //player 1 HUD drawn on the left
		{
			//left = true;
			playerRef = PlayerData.GetPlayer1();
			shotDir = 1;
		}
		else //player2 HUD drawn on the right
		{
			//left = false;
			playerRef = PlayerData.GetPlayer2();
			shotDir = -1;
		}

		if (playerRef.getChosenChar().Equals(WhichCharacter.O)) //figure out which HUD head icon to use
		{
			charface = OHUDIcon;
		}
		else
		{
			charface = PHUDIcon;
		}
		CharacterIconDisplay.sprite = charface;
		//register listeners so that when messages are broadcast, updates are triggered
		Utilities.RegisterListener(new Listener(OmonoPehaviour.ms_CHARSPAWNED,gameObject,"GrabInstanceData"));
		Utilities.RegisterListener(new Listener(OmonoPehaviour.ms_CHARDIED,gameObject,"CharDied"));
		Utilities.RegisterListener(new Listener(OmonoPehaviour.ms_EQUIPCHANGE,gameObject,"UpdateWeapon"));
		Utilities.RegisterListener(new Listener(OmonoPehaviour.ms_ENTITYTOOKDAMAGE,gameObject,"UpdateHealth"));
		Utilities.RegisterListener(new Listener(OmonoPehaviour.ms_AMMOCHANGE,gameObject,"UpdateAmmo"));
		Utilities.RegisterListener(new Listener(OmonoPehaviour.ms_OPPCHANGE,gameObject,"UpdateOP"));
		Utilities.RegisterListener(new Listener(OmonoPehaviour.ms_ROOMSTARTED,gameObject,"DisableHUDAttack"));
		Utilities.RegisterListener(new Listener(OmonoPehaviour.ms_ROOMENDED,gameObject,"EnableHUDAttack"));

		//check if level instance is null, if it is not, then grab components of level instance
		GrabInstanceData(null);
		/* b/c of the order of Start methods, instance may not actually exist, so HUD will not draw to screen
        *  if this happens, it will be up to the registered listener to be alerted when the instance exists and has registered
        *  itself to the player metadata
        */
	}

	public void setHUDStatus(bool isActive)
	{
		drawPlayerHUD = isActive;
	}

	void OnGUI()
	{
		/*if (drawPlayerHUD)
        {
            if (left)
            {
                GUI.DrawTexture(new Rect(Screen.width/20, Screen.height / 30, 50, 50), charface);
                GUI.Label(new Rect(Screen.width/20, Screen.height / 30, 30, 30), ""+playerRef.GetNumOfLives());
                GUI.DrawTexture(new Rect(2* Screen.width/20, Screen.height / 30, 50, 50), weaponIcon);
                if (!currWeapon.pType.Equals(ProjType.straight))
                {
                    GUI.Label(new Rect(2* Screen.width/20, Screen.height / 30, 50, 50), ""+currAmmo, numDisplay);
                }
                GUI.Label(new Rect(3* Screen.width/20, Screen.height / 30, 50, 50), "Health: "+currHealth);
                GUI.Label(new Rect(4* Screen.width/20, Screen.height / 30, 50, 50), "OP: "+currOP);
            }
            else
            {
                GUI.DrawTexture(new Rect(Screen.width - Screen.width/20, Screen.height / 30, 50, 50), charface);
                GUI.Label(new Rect(Screen.width - Screen.width/20, Screen.height / 30, 30, 30), ""+playerRef.GetNumOfLives());
                GUI.DrawTexture(new Rect(Screen.width - (2* Screen.width/20), Screen.height /30, 50, 50), weaponIcon);
                if (!currWeapon.pType.Equals(ProjType.straight))
                {
                    GUI.Label(new Rect(Screen.width - (2* Screen.width/20), Screen.height / 30, 50, 50), ""+currAmmo, numDisplay);
                }
                GUI.Label(new Rect(Screen.width - (3* Screen.width/20), Screen.height / 30, 50, 50), "Health: "+currHealth);
                GUI.Label(new Rect(Screen.width - (4* Screen.width/20), Screen.height / 30, 50, 50), "OP: "+ currOP);
            }
        }*/
	}

	void GrabInstanceData(Message m)
	{
		/*if (playerRef.getStageInstance() != null)
        {
            GameObject instance = playerRef.getStageInstance();
            eScript = instance.GetComponent<Entity>();
            opScript = instance.GetComponent<OverPower>();
            eHandler = instance.GetComponent<EquipmentHandler>();
            currHealth = eScript.GetHealth();
            maxHealth = eScript.GetMaxHealth();
            currOP = opScript.GetOPP();
            maxOP = opScript.GetMaxOPP();
            currWeapon = eHandler.GetCurrWeapon();
            weaponIcon = eHandler.GetCurrWeapon().GetHUDTexture();
            currAmmo = eHandler.GetCurrAmmo();
            drawPlayerHUD = true; //now HUD is read to be drawn
        }*/
		if (playerRef.getStageInstance() != null)
		{
			Debug.Log ("Player HUD Info Grabbed for " + playerRef.getChosenChar ().ToString());
			GameObject instance = playerRef.getStageInstance();
			eScript = instance.GetComponent<Entity>();
			opScript = instance.GetComponent<OverPower>();
			eHandler = instance.GetComponent<EquipmentHandler>();
			maxHealth = eScript.GetMaxHealth();
			HealthDisplay.text = "HP: " + eScript.GetHealth() + " / " + maxHealth;
			maxOP = opScript.GetMaxOPP();
			OPDisplay.text = "OP: " + opScript.GetOPP() + " / " + maxOP;
			currWeapon = eHandler.GetCurrWeapon();
			//Debug.LogWarning ("Current Weapon name: " + eHandler.name);

			//Debug.LogWarning ("Current Weapon name: " + eHandler.GetCurrWeapon().pType.ToString());
//			if (eHandler.GetCurrWeapon() == null) {
//				Debug.LogWarning ("get curr weapon == null");
//				Debug.Break();
//				Debug.LogWarning ("should not be hit");
//			}
//			if (eHandler.GetCurrWeapon().GetHUDImage() == null) {
//				Debug.LogWarning ("get hud image == null");
//				Debug.Break();
//			}
			if (eHandler.GetCurrWeapon () != null) { //for some reason on respawn this value is null
				
				CurrentWeaponDisplay.sprite = eHandler.GetCurrWeapon ().GetHUDImage ();
			}
			AmmoDisplay.text = eHandler.GetCurrAmmo()+"";
			drawPlayerHUD = true; //now HUD is read to be drawn           
			NumOfLivesDisplay.text = playerRef.GetNumOfLives()+"";
			OutOfLivesDisplay.gameObject.SetActive (false);
		}
	}

	void CharDied(Message m)
	{
		OutOfLivesDisplay.gameObject.SetActive (true);
	}

	//used to determine if script reference is null (most likely b/c attached object was destroyed)
	bool ScriptIsAlive(OmonoPehaviour elScript)
	{
		return (elScript != null);
	}

	/* Local variables to be updated when changed instead
     * of having constant method calls every ONGUI
     */
	void UpdateWeapon(Message m)
	{
		if (ScriptIsAlive(eHandler))
		{
			Debug.Log ("Player HUD Weapon Info Grabbed for " + playerRef.getChosenChar ().ToString());

			currWeapon = eHandler.GetCurrWeapon();
			CurrentWeaponDisplay.sprite = eHandler.GetCurrWeapon().GetHUDImage();
			AmmoDisplay.gameObject.SetActive(false);
			if (!currWeapon.pType.Equals(ProjType.straight))
			{
				AmmoDisplay.gameObject.SetActive(true);
				AmmoDisplay.text = eHandler.GetCurrAmmo()+"";   
			}           
		}
	}

	void UpdateAmmo(Message m)
	{
		if (ScriptIsAlive(eHandler))
		{
			Debug.Log ("Player HUD Ammo Info Grabbed for " + playerRef.getChosenChar ().ToString());

			AmmoDisplay.text = eHandler.GetCurrAmmo()+"";
		}
	}

	void Update()
	{
		/*
		if (Input.GetKeyDown ("y")) {
			GameObject spawnedOrp = (GameObject)Instantiate(OrpPrefab,orpSpawnLocation,Quaternion.Euler(0,0,0));
			Rigidbody2D dammit = spawnedOrp.GetComponent<Rigidbody2D> ();
			Utilities.cloneObject(spawnedOrp, orpSpawnLocation, new Vector2(Random.Range(-30f,30f),-20f),Quaternion.Euler(0,0,0));
			//spawnedOrp.GetComponent<Rigidbody2D>().velocity = new Vector2(Random.Range(-30f,30f),-20);
			//dammit.velocity =  new Vector2(3,3);

		}*/
	}
	void UpdateHealth(Message m)
	{
		if (ScriptIsAlive(eScript))
		{
			Debug.Log ("Player HUD Health Info Grabbed for " + playerRef.getChosenChar ().ToString());
			HealthDisplay.text = "HP: " + eScript.GetHealth() + " / " + maxHealth;
		}
	}

	void UpdateOP(Message m)
	{
		if (ScriptIsAlive(opScript))
		{
			OPDisplay.text = "OP: " + opScript.GetOPP() + " / " + maxOP;
		}
	}

	void EnableHUDAttack()
	{
		myCollider.enabled = true;
	}

	void DisableHUDAttack()
	{
		myCollider.enabled = false;
	}
}